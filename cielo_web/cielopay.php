<?php
	require_once("../../lib/common.php");
	require_once(DIR_WS_PAYMENT."/payment.php");
	require_once(DIR_WS_LIB.'cart_class.php');
	require_once(DIR_WS_PAYMENT."cielo_web/includes/include.php");
	
	$checkout_details        	= new Shopping_Cart_Data(true,true,true,true,false);
	$Payment_Checkout 			= $checkout_details->ship_pay_details;
	
    $paymentMethodId = $RRequest->request('payment_method_id');
    $payment_method_id = (isset($paymentMethodId) && $paymentMethodId!="")?$paymentMethodId:basename(__DIR__);
	$PaymentDetails = getPaymentMethodSettings($payment_method_id);
	
	$count_pageload =0;
    if($RRequest->post() != ''){
		$count_pageload =$count_pageload+1;
	}

	$return_url     = show_page_link("payment/cielo_web/response.php",true);

    $orderid = $RRequest->request('order_id');

	$Pedido = new Pedido();

//		$merchant_ec_number = $PaymentDetails[merchant_ec_number];
//	    $merchant_key		= $PaymentDetails[merchant_key];
    $cielo_ec_number	= $PaymentDetails[cielo_ec_number];
    $cielo_key			= $PaymentDetails[cielo_key];
    
//		define("LOJA", "$merchant_ec_number");
//		define("LOJA_CHAVE", "$merchant_key");
	define("CIELO", "$cielo_ec_number");
	define("CIELO_CHAVE", "$cielo_key");
	
	// L? dados do $_POST
	$Pedido->formaPagamentoBandeira = $Payment_Checkout["codigoBandeira"]; 
	
	//$formaPagamentoProduto = explode(":",$Payment_Checkout["formaPagamento"]);

//		if($Payment_Checkout["formaPagamento"] != "A" && $Payment_Checkout["formaPagamento"] != "1")
//		{
//			$Pedido->formaPagamentoProduto 	= $PaymentDetails["tipoParcelamento"];
//			$Pedido->formaPagamentoParcelas = $formaPagamentoProduto[0];
//		} 
//		else 
//		{
		$Pedido->formaPagamentoProduto 	= 1;
		$Pedido->formaPagamentoParcelas = 1;
//		}

	$Pedido->dadosEcNumero 	= CIELO;
	$Pedido->dadosEcChave 	= CIELO_CHAVE;

	$Pedido->capturar 	= $PaymentDetails["capturarAutomaticamente"];	
	$Pedido->autorizar 	= $PaymentDetails["indicadorAutorizacao"];

	$Pedido->dadosPedidoNumero 	= $orderid;

	$ori_price = $RRequest->request('total');

	if($formaPagamentoProduto[1] > 0){
		$tax = $ori_price * $formaPagamentoProduto[1] / 100;
		$ori_price  = $ori_price + $tax;
		$ori_price = number_format((float)$ori_price, 2, '.', '');
	}

	if(strpos($ori_price,'.') > 0){
		 $price = str_replace('.','',$ori_price);
	}else{
		$price = $ori_price.'00';
	}
    
	$currency_code	= explode("-",$PaymentDetails['txt_currency_code']);
	$dadosPedidoMoeda = $currency_code[1];
	$Pedido->dadosPedidoValor 	= $price;
	$Pedido->dadosPedidoMoeda	= $dadosPedidoMoeda;
	$Pedido->urlRetorno = $return_url;
	/*echo "<pre>";
	print_r($Pedido);
	echo "</pre>";
	exit;*/
	// ENVIA REQUISI??O SITE CIELO
	
	$objResposta = $Pedido->RequisicaoTransacao(false);

	$Pedido->tid 		= $objResposta->tid;
	$Pedido->pan 		= $objResposta->pan;
	$Pedido->status 	= $objResposta->status;

	$urlAutenticacao	= "url-autenticacao";
	$Pedido->urlAutenticacao = $objResposta->$urlAutenticacao;
	$post_url 			= $objResposta->$urlAutenticacao;
	
	// Serializa Pedido e guarda na SESSION
	$StrPedido = $Pedido->ToString();
	$_SESSION["pedidos"]->append($StrPedido);
	
	$item_name		= $PaymentDetails['txt_item_name'];
	$currency_code	= $PaymentDetails['txt_currency_code'];
	/*$currency_code 	= $_REQUEST['ccode'];*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></title>
	<link href="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_CSS;?>theme.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript">
var loopTime=setInterval("currentTime()",500);
</script>
<style type="text/css" >
#contentContainer {
	background:none;
}
.div_img_loader {
	/*color:#df6c1b;*/
	float:left;
	font-size:72px;
	text-align:left;
	width:25%;
	margin:-12px 0px 0px 0px;
}
.btn_pay_style{
	/*background:#df6c1b;*/
	color:#FFFFFF;
	font-size:16px;
	height:48px;
	padding:3px 5px 3px 5px;
}
.wait_img_loading {
	height:300px;
}
.wait_img_loading .img_load {
	float:none;
	padding:72px 0 10px 0;
	text-align:center;
	line-height:28px;
	font-size:15px;	
	/*font-weight:bold;*/
}
.wait_img_loading .img_load .load_message {
	text-align:right;
	line-height:28px;
	font-size:34px;
	font-weight:bold;
	/*color:#df6c1b;*/
	width:66%;
	text-align:center;
	float:left;
	padding-left:90px;
}
.wait_img_loading .img_load .click_message {
	text-align:center;
	line-height:28px;
	font-size:16px;
}
</style>
<script type="text/javascript">
<!-- Begin
/* This script and many more are available free online at
The JavaScript Source!! http://javascript.internet.com
Created by: Abraham Joffe :: http://www.abrahamjoffe.com.au/ */

var startTime=new Date();
var strDisplay = '.';
var repeat = 5;
var start = 0;
function currentTime(){
  var a=Math.floor((new Date()-startTime)/100)/10;
  if(start != repeat ) {
  	document.getElementById("div_repeat_info").innerHTML = document.getElementById("div_repeat_info").innerHTML + strDisplay;
  	start = start + 1;
  } else {
  	document.getElementById("div_repeat_info").innerHTML = strDisplay;
  	start = 0;
  }
  /*if (a%1==0) a+=".0";
  document.getElementById("div_img_loader").innerHTML=a;*/
}

window.onload=function(){
  clearTimeout(loopTime);
  strDisplay = document.getElementById("div_repeat_info").innerHTML
}

// End -->
</script>
</head>
<body>
<form action="<?php echo $post_url; ?>" method="post" name="payment">
<div id="container">
	<div id="header">
		<div id="headerleft">
		<!-- Header Left -->
			<div class="logo">
				<!--<span><a href="<?php echo show_page_link(FILE_INDEX);?>"><img src="<? echo DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES; ?>logo.gif" /></a></span>-->
				<span><a href="<?php echo show_page_link(FILE_INDEX);?>"><img src="<? echo SITE_LOGO_PATH; ?>" /></a></span>
			</div>
		</div>
	</div>
	<!-- main contian End -->
	<div id="contentContainer" class="wait_img_loading">
		<div class="img_load">
			<div class="page-section-header load_message"><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></div><div id="div_repeat_info" class="page-section-header div_img_loader">.</div>
			<div style="float:left;padding:20px 0 7px 7px;text-align:left;width:100%;text-align:center;"><?php echo LABEL_PAGE_LOAD_PROBLEM ;?></div>
			<div style="float:left;padding:8px 0 7px 7px;width:100%;"><?php if(file_exists(DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES.'img_click_to_pay.gif')) { ?>
				<label class="btn btn-primary btn_left_co"><input type="submit" class="btn btn-primary btn_pay_style" name="btnpaysubmit" src="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES; ?>img_click_to_pay.gif" title="Click Here To Pay" value="<?php echo LEBEL_CLICK_HERE_TO_PAY;?>" alt="Click Here To Pay"></label>
			<?php } else { ?>
				<label class="btn btn-primary btn_left_co"><input type="submit" class="btn btn-primary btn_pay_style" name="btnpaysubmit" title="Click Here To Pay" value="<?php echo LEBEL_CLICK_HERE_TO_PAY;?>" ></label>
			<?php } ?></div>
		</div>	
	</div>
	</div>
</div>
</form>
<!-- Main End -->
</body>
</html>
<?php if($count_pageload <= 1){ ?>
<script language="javascript">
document.payment.submit();
</script>
<?php } ?>