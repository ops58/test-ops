<?php
	require_once(DIR_WS_PAYMENT."/payment.php");
	require_once(DIR_WS_LIB.'cart_class.php');
	require_once(DIR_WS_PAYMENT."cielo_web/includes/include.php");
	
	$ObjSitePaymentMethodMaster = new PaymentMethodMaster();
	$checkout_details        	= new Shopping_Cart_Data(true,true,true,true,false);
	$Payment_Checkout 			= $checkout_details->ship_pay_details;
	
	$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));
	$installment_conditions = "1,2,3";//$PaymentDetails['installment_conditions'];
	$installment_conditions = explode(",",$installment_conditions);
	$sitepaymentid=$data['sitepaymentid'];
?>
<div class="row">
	<div class="col-12 pay_sub">
		<div class="form-row my-3">
			<label for="codigoBandeira_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo PAYMENT;?></label>							
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<select class="select_width custom-select" name="codigoBandeira" id="codigoBandeira_<?php echo $sitepaymentid; ?>" data-rule-required="true">
						<option value="visa">Visa</option>
					
					<?php if($PaymentDetails['offer_mastercard'] == 1) { ?>
						<option value="mastercard">Mastercard</option>
					<?php } ?>
					
					<?php if($PaymentDetails['offer_diners'] == 1) { ?>
						<option value="diners">Diners</option>
					<?php } ?>
					
					<?php if($PaymentDetails['offer_discover'] == 1) { ?>
						<option value="discover">Discover</option>
					<?php } ?>
					
					<?php if($PaymentDetails['offer_elo'] == 1) { ?>
						<option value="elo">Elo</option>
					<?php } ?>
					
					<?php if($PaymentDetails['offer_amex'] == 1) { ?>
						<option value="amex">Amex</option>
					<?php } ?>
					</select>										        	        	
					<span class="input-group-require text-danger pl-1"></span>
				</div>
			</div>
		</div>
		
		
		<div class="form-row mb-3 d-none" id="tr_installment" >
			<label for="formaPagamento_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo INSTALLMENT;?></label>							
			<div class="col-12 col-md-8">
				<div class="position-relative">
    				<?php foreach ($installment_conditions as $installament_val){
        		       $val = explode(":",$installament_val);
        		       if($checkout_details->payment_method_amount/$val[0] > $PaymentDetails['minimum_value_per_plot']){
        		           if($val[0] == 1){
        		               $checked = 'checked';
        		           }else{
        		               $checked = '';
        		           }
                    ?>
					<div class="<?php echo $val[0];?> custom-control custom-radio pr-3 m-1">
    					<input type="radio" name="formaPagamento" id="formaPagamento_<?php echo $val[0]; ?>" class="<?php echo $val[0];?> custom-control-input" <?php echo $checked; ?> value="<?php echo $installament_val; ?>"  style="margin-left:20px;"/>
    					<label for="formaPagamento_<?php echo $val[0]; ?>" class="custom-control-label"><?php echo $val[0]."x";?></label>
    				</div>
    				<?php } } ?>							        	        	
					<span class="input-group-require text-danger pl-1"></span>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
<?php //Start Payment Function ?>
function paymentform_<?php echo $sitepaymentid; ?>(){
	return true;
}

<?php /* End Payment Function */ ?>
</script>