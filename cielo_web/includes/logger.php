<?php
	class Logger
	{	
		private $path = DIR_WS_PAYMENT;
		private $log_file = "cielo_web/logs/xml.log";
		private $fp = null;
		
		public function logOpen()
		{	
			$logfile_path = $this->path.$this->log_file;
			$this->fp = fopen($logfile_path, 'a');
		}
		 
		public function logWrite($strMessage, $transacao)
		{
			if(!$this->fp)
				$this->logOpen();
			
			$path = $_SERVER["REQUEST_URI"];
			$data = date("Y-m-d H:i:s:u (T)");
			
			$log = "***********************************************" . "\n";
			$log .= $data . "\n";
			$log .= "DO ARQUIVO: " . $path . "\n"; 
			$log .= "OPERA��O: " . $transacao . "\n";
			$log .= $strMessage . "\n\n"; 

			fwrite($this->fp, $log);
		}
	}
?>
