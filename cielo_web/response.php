<?php
/**
 * This file is check the paypal response.
 *
 * @author     Radixweb <team.radixweb@gmail.com>
 * @copyright  Copyright (c) 2008, Radixweb
 * @version    1.0
 * @since      1.0
 */
require_once("../../lib/common.php");
require_once(DIR_WS_MODEL."PaymentMethodMaster.php");
require_once(DIR_WS_MODEL."PaymentDetailsMaster.php");
require_once(DIR_WS_MODEL."OrderMaster.php");	
require_once(DIR_WS_MODEL."orderHistoryMaster.php");
require_once(DIR_WS_PAYMENT."payment.php");
require_once(DIR_WS_PAYMENT."cielo_web/includes/include.php");


$error_email = "";
$from_error_email = "";

$ObjPaymentDetailsMaster= new PaymentDetailsMaster();
$PaymentDetailsDataObj	= new PaymentDetailsData();
$ObjPaymentMethodMaster = new PaymentMethodMaster();
$OrderMasterObj         = new OrderMaster();
$OrderDataObj	        = new OrderData();
$ObjOrderHistoryMaster	= new OrderHistoryMaster();
$OrderHistoryData		= new OrderHistoryData();


$ultimoPedido = $_SESSION["pedidos"]->	count();
	
$ultimoPedido -= 1;

$Pedido = new Pedido();
$Pedido->FromString($_SESSION["pedidos"]->offsetGet($ultimoPedido));

// Consulta situa??o da transa??o
$objResposta = $Pedido->RequisicaoConsulta();
$response_array = json_decode(json_encode($objResposta), true);

$dadospedido = "dados-pedido";
$order_number = $response_array[$dadospedido][numero];
$payment_status_details = $response_array[status];
$Pedido->status = $objResposta->status;
$staus_remark = $Pedido->getStatus();

$transaction_id = $response_array[tid];

if($payment_status_details == '4' || $payment_status_details == '6' || $payment_status_details == '3')
	$payment_status_result = true;
else
	$payment_status_result = false;

// Atualiza Pedido da SESSION
$StrPedido = $Pedido->ToString();
$_SESSION["pedidos"]->offsetSet($ultimoPedido, $StrPedido);

$OptionPay = array();
$ObjPaymentDetailsMaster->setWhere("AND Orders_Id = :Orders_Id", $order_number, 'int');
$payment_details = $ObjPaymentDetailsMaster->getPaymentDetails();
$OrderPayment = $payment_details[0];

$PaymentDetails = getPaymentMethodSettings($OrderPayment['payment_method']);

if($transaction_id != '') {
	$ObjPaymentDetailsMaster->setWhere("AND transactionid = :transactionid", $transaction_id, 'string');
	$paymentDetailsTrans = $ObjPaymentDetailsMaster->getPaymentDetails();
}

$paypal_details = serialize($response_array);

	if($payment_status_result===true) {
		$OrderDataObj->orders_id 		= $order_number;
		$OrderDataObj->orders_status_id	= $PaymentDetails[ELEMENT_ORDER_STATUS];
		$OrderDataObj->order_last_modified 	= date('Y-m-d H:i:s');
		$OrderMasterObj->editOrder($OrderDataObj, "yes");
		
		// Order History Data
		$OrderHistoryData->orders_status_id 	= $PaymentDetails[ELEMENT_ORDER_STATUS];
		$OrderHistoryData->comments 		= '';
		$OrderHistoryData->customer_notified 	= '0';
		$OrderHistoryData->orders_id 		= $order_number;
		$OrderHistoryData->date_added		= date('Y-m-d H:i:s');
		$OrderHistoryData->createdby = "A";
		// Added In Order History table
		$ObjOrderHistoryMaster->addOrderHistory($OrderHistoryData);
		
		$PaymentDetailsDataObj->orders_id = $order_number;
		$PaymentDetailsDataObj->remark = $staus_remark;
		$PaymentDetailsDataObj->transactionid = $transaction_id;
		
		if($response_array['forma-pagamento']['parcelas']){
			$installment_charge = array();
			$installment = explode(",",$PaymentDetails['installment_conditions']);
			foreach ($installment as $inst){
				$installment_charge = explode(":",$inst);
				if($response_array['forma-pagamento']['parcelas'] == $installment_charge[0]){
					$installment_rate = $installment_charge[1];
				}
			}
			
			$tax = $OrderPayment->total_amount * $installment_rate/100;
			$ori_price  = $OrderPayment->total_amount + $tax;
			$OrderPayment->total_amount = number_format((float)$ori_price, 2, '.', '');
			
		}
		
		$PaymentDetailsDataObj->total_amount = $OrderPayment->total_amount;
		$PaymentDetailsDataObj->payment_response_data = $paypal_details;
		$ObjPaymentDetailsMaster->editPaymentDetails($PaymentDetailsDataObj,true);
		
		
		/** To Send Mail to customer **/
		require_once(DIR_WS_MODEL . "OrderProductMaster.php");
		$OrderProductMasterObj = new OrderProductMaster();
		
		$OrderMasterObj->setWhere("AND orders_id = :orders_id", $order_number, 'int');
		$Cust_OrderDetails = $OrderMasterObj->getOrder(null, $sOrder);
		$Cust_OrderDetails = $Cust_OrderDetails[0];
		$OrderProductMasterObj->setWhere("AND orders_id = :orders_id", $order_number, 'int');
		$DataOrderProduct = $OrderProductMasterObj->getOrderProduct(null, $sOrder);
		
		$Shipping_Address = array(
			'name' => htmlspecialchars(stripslashes($Cust_OrderDetails->delivery_name),ENT_QUOTES),
			'street_address' => htmlspecialchars(stripslashes($Cust_OrderDetails->delivery_street_address),ENT_QUOTES),
			'superb' => htmlspecialchars(stripslashes($Cust_OrderDetails->delivery_suburb),ENT_QUOTES),
			'postcode' => $Cust_OrderDetails->delivery_postcode,
			'city' => htmlspecialchars(stripslashes($Cust_OrderDetails->delivery_city),ENT_QUOTES),
			'state' => $Delivery_stateData->delivery_state,
			'country' => $DelCountryData->delivery_country,
		);
		$Billing_Address = array(
			'name' => htmlspecialchars(stripslashes($Cust_OrderDetails->billing_name),ENT_QUOTES),
			'street_address' => htmlspecialchars(stripslashes($Cust_OrderDetails->billing_street_address),ENT_QUOTES),
			'superb' => htmlspecialchars(stripslashes($Cust_OrderDetails->billing_suburb),ENT_QUOTES),
			'postcode' => $Cust_OrderDetails->billing_postcode,
			'city' => htmlspecialchars(stripslashes($Cust_OrderDetails->billing_city),ENT_QUOTES),
			'state' => $billing_stateData->billing_state,
			'country' => $DelCountryData->billing_country,
		);
		
		Order_Completion_Front($order_number);
		$param = '?add=success&OrderNo='.$order_number;
	} else {
		$PaymentDetailsDataObj->orders_id = $order_number;
		$PaymentDetailsDataObj->remark = $staus_remark;
		$PaymentDetailsDataObj->payment_response_data = $paypal_details;
		$ObjPaymentDetailsMaster->editPaymentDetails($PaymentDetailsDataObj,true);
		$param = '?cancleMsg=true&OrderNo='.$order_number;
	}
	show_page_header(FILE_THANKS.$param);
