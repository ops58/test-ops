<?php
/** Cielo Payment - Staprint */
define_const('SUBDIVISION', 'Subdivision');
define_const('SHOP', 'Shop');
define_const('ADMINISTRATOR', 'Administrator');
define_const('AUTOMATICALLY_CAPTURE', 'Automatically capture?');
define_const('NOT', 'Not');
define_const('YES', 'Yes');
define_const('AUTOMATIC_AUTHORIZATION', 'Automatic Authorization');
define_const('AUTHORIZE_DIRECT', 'Authorize Direct');
define_const('AUTHORIZE_TRANSACTION_AUTHENTICATED_AND_NONAUTHENTICATED', 'Authorize Transaction Authenticated And Nonauthenticated');
define_const('ONLY_AUTHENTICATE_THE_TRANSACTION', 'Only Authenticate The Transaction');
define_const('AUTHORIZE_TRANSACTION_ONLY_IF_AUTHENTICATED', 'Aauthorize Transaction Only If Authenticated');
define_const('MERCHANT_EC_NUMBER', 'Merchant EC Number');
define_const('MERCHANT_KEY', 'Merchant Key');
define_const('CIELO_EC_NUMBER', 'Cielo EC Number');
define_const('CIELO_KEY', 'Cielo Key');

define_const('MINIMUM_VALUE_PER_PLOT', 'Minimum Value Per Plot');
define_const('MAXIMUM_VALUE_TO_PLOT','Maximum Value To Plot');
define_const('OFFER_MASTERCARD', 'Offer Mastercard');
define_const('OFFER_DINERS', 'Offer Diners');
define_const('OFFER_DISCOVER', 'Offer Discover');
define_const('OFFER_ELO', 'Offer Elo');
define_const('OFFER_AMEX', 'Offer Amex');

define_const('SHARE_TRADING_INTEREST','Share Trading Interest');
define_const('DISPLAY_ORDER','Display Order');
define_const('INSTALLMENT_CONDITIONS','Installment Conditions');

/** END Cielo Payment - Staprint */

/*Help messages */
	$help_messages1= array (
	'HELP_DISCOUNT' => 'Allow Discount On Order Amount',
	'HELP_DISCOUNT_AMOUNT' => '__% Discount On Order Amount',
	'HELP_MINIMUM_ORDER' => 'Minimum Order Amount',
	'HELP_MINIMUM_VALUE_PER_PLOT' => 'Enter the minimum value for each plot. use  (dot) as the decimal separator, example: 30.00',
	'HELP_MAXIMUM_VALUE_TO_PLOT' => 'Enter the order amount which can be paid via CIELO. use. (dot) as the decimal separator, <br>example: 3000.00 Although not use this resource should leave a very high value configured as per example set 9999999999999999',
	'HELP_OFFER_MASTERCARD' => 'If not wanted to use the card by <b>Mastercard</b> Cielo choose <b>Not</b>',
	'HELP_OFFER_DINERS' => 'If not wanted to use the card by <b>Diners</b> Cielo choose <b>Not</b>',
	'HELP_OFFER_DISCOVER' => 'If not wanted to use the card by <b>Discover</b> Cielo choose <b>Not</b>',
	'HELP_OFFER_ELO' => 'If not wanted to use the card by <b>Elo</b> Cielo choose <b>Not</b>',
	'HELP_OFFER_AMEX' => 'If not wanted to use the card by <b>Amex</b> Cielo choose <b>Not</b>',
	
	'HELP_SHARE_TRADING_INTEREST'  => 'Enter the number in the field above the plot he ought to have the interest or exchanged for store manager.<br> Use only digit interio for example set to enter the parcel seven (7) and only seven. To share ten type 10 and only ten',
	
	'HELP_DISPLAY_ORDER' => 'Display Order',
	'AUTHENTICATION_TYPE' => 'Authentication Type',
	'HELP_INSTALLMENT_CONDITIONS' => 'Set the type of action performed for each form of installments.<BR>
									- Discount: Sera percentage discount applied to the previously defined total order value and then divided by the respective number of installments.<BR>
									- Without interest: the value will be divided by the respective total order number of plots without accretion or discount.<BR>
									- With interest: Will be applied the percentage of accretion defined anteriorly to the total order value and then divided by the respective number of installments.<BR>
										1x standard and value for cash payment.<BR>
										In installment sales if selected in the plot "with interest." and the field index Xx is left blank the system will automatically consider that the installments and the manager ignoring the configuration of the field type of installment',
	);
	$help_messages =array_merge($help_messages,$help_messages1);
?>
<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">   
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_ENVIRONMENT; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['sel_payment_env'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['sel_payment_env'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   
		   ?>
		   <span><input type="radio" id="sel_payment_env" name="sel_payment_env" value="1" <?php echo $lchecked;?>><?php echo PAYMENT_LBL_LIVE_ENV; ?></span>
		   <span class="radio_distance"><input type="radio" id="sel_payment_env" name="sel_payment_env" value="0" <?php echo $tchecked;?>><?php echo PAYMENT_LBL_TESTING_ENV; ?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_ENVIRONMENT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	<tr>
		<td class="form_caption" align="left"><?php echo SUBDIVISION; ?> : </td>
		<td align="left">
			<select class="select_small" name="tipoParcelamento" id="tipoParcelamento">
				<option value="2" <?php if($PaymentDetails['tipoParcelamento'] == 2) { echo "selected"; }?>><?php echo SHOP; ?></option>
				<option value="3" <?php if($PaymentDetails['tipoParcelamento'] == 3) { echo "selected"; }?>><?php echo ADMINISTRATOR; ?></option>
			</select>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_SUBDIVISION'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	
	<tr>
		<td class="form_caption" align="left"><?php echo AUTOMATICALLY_CAPTURE; ?> : </td>
		<td align="left">
			<select class="select_small" name="capturarAutomaticamente" id="capturarAutomaticamente">
				<option value="false" <?php if($PaymentDetails['capturarAutomaticamente'] == 'false') { echo "selected"; }?>><?php echo NOT; ?></option>
				<option value="true"  <?php if($PaymentDetails['capturarAutomaticamente'] == 'true') { echo "selected"; }?>><?php echo YES; ?></option>
			</select>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_AUTOMATICALLY_CAPTURE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	<tr>
		<td class="form_caption" align="left"><?php echo AUTOMATIC_AUTHORIZATION; ?> : </td>
		<td align="left">
			<select class="select_std" name="indicadorAutorizacao" id="indicadorAutorizacao">
				<option value="3" <?php if($PaymentDetails['indicadorAutorizacao'] == 3) { echo "selected"; }?>><?php echo AUTHORIZE_DIRECT; ?></option>
				<option value="2" <?php if($PaymentDetails['indicadorAutorizacao'] == 2) { echo "selected"; }?>><?php echo AUTHORIZE_TRANSACTION_AUTHENTICATED_AND_NONAUTHENTICATED; ?></option>
				<option value="0" <?php if($PaymentDetails['indicadorAutorizacao'] == 0) { echo "selected"; }?>><?php echo ONLY_AUTHENTICATE_THE_TRANSACTION; ?></option>
				<option value="1" <?php if($PaymentDetails['indicadorAutorizacao'] == 1) { echo "selected"; }?>><?php echo AUTHORIZE_TRANSACTION_ONLY_IF_AUTHENTICATED; ?></option>
			</select>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_AUTOMATIC_AUTHORIZATION'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	

	<!--<tr>
		<td class="form_caption" align="left"><?php echo MERCHANT_EC_NUMBER; ?> : </td>
		<td align="left"><input type="text" name="merchant_ec_number" id="merchant_ec_number" value="<?php echo $PaymentDetails['merchant_ec_number'];?>" class="text_box_std"></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_MERCHANT_EC_NUMBER'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>

	<tr>
		<td class="form_caption" align="left"><?php echo MERCHANT_KEY; ?> : </td>
		<td align="left"><input type="text" name="merchant_key" id="merchant_key" value="<?php echo $PaymentDetails['merchant_key'];?>" class="text_box_std"></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_MERCHANT_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>-->

	<tr>
		<td class="form_caption" align="left"><?php echo CIELO_EC_NUMBER; ?> : </td>
		<td align="left"><input type="text" name="cielo_ec_number" id="cielo_ec_number" value="<?php echo $PaymentDetails['cielo_ec_number'];?>" class="text_box_std"></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_CIELO_EC_NUMBER'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>

	<tr>
		<td class="form_caption" align="left"><?php echo CIELO_KEY; ?> : </td>
		<td align="left"><input type="text" name="cielo_key" id="cielo_key" value="<?php echo $PaymentDetails['cielo_key'];?>" class="text_box_std"></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_CIELO_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	
	
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo OFFER_MASTERCARD; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['offer_mastercard'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['offer_mastercard'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   		if($PaymentDetails['offer_mastercard'] != "0" && $PaymentDetails['offer_mastercard'] != "1"){
		   			$lchecked = "checked";
		   		}
		   ?>
		   <span><input type="radio" id="offer_mastercard" name="offer_mastercard" value="1" <?php echo $lchecked;?>><?php echo YES; ?></span>
		   <span class="radio_distance"><input type="radio" id="offer_mastercard" name="offer_mastercard" value="0" <?php echo $tchecked;?>><?php echo NOT; ?></span>
		</td>
	</tr>
	<tr>
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_OFFER_MASTERCARD'];?></td>
	</tr>
	<tr>
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo OFFER_DINERS; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['offer_diners'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['offer_diners'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   		if($PaymentDetails['offer_diners'] != "0" && $PaymentDetails['offer_diners'] != "1"){
		   			$lchecked = "checked";
		   		}
		   ?>
		   <span><input type="radio" id="offer_diners" name="offer_diners" value="1" <?php echo $lchecked;?>><?php echo YES; ?></span>
		   <span class="radio_distance"><input type="radio" id="offer_diners" name="offer_diners" value="0" <?php echo $tchecked;?>><?php echo NOT; ?></span>
		</td>
	</tr>
	<tr>
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_OFFER_DINERS'];?></td>
	</tr>
	<tr>
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo OFFER_DISCOVER; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['offer_discover'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['offer_discover'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   		if($PaymentDetails['offer_discover'] != "0" && $PaymentDetails['offer_discover'] != "1"){
		   			$lchecked = "checked";
		   		}
		   ?>
		   <span><input type="radio" id="offer_discover" name="offer_discover" value="1" <?php echo $lchecked;?>><?php echo YES; ?></span>
		   <span class="radio_distance"><input type="radio" id="offer_discover" name="offer_discover" value="0" <?php echo $tchecked;?>><?php echo NOT; ?></span>
		</td>
	</tr>
	<tr>
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_OFFER_DISCOVER'];?></td>
	</tr>
	<tr>
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo OFFER_ELO; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['offer_elo'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['offer_elo'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   		if($PaymentDetails['offer_elo'] != "0" && $PaymentDetails['offer_elo'] != "1"){
		   			$lchecked = "checked";
		   		}
		   ?>
		   <span><input type="radio" id="offer_elo" name="offer_elo" value="1" <?php echo $lchecked;?>><?php echo YES; ?></span>
		   <span class="radio_distance"><input type="radio" id="offer_elo" name="offer_elo" value="0" <?php echo $tchecked;?>><?php echo NOT; ?></span>
		</td>
	</tr>
	<tr>
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_OFFER_ELO'];?></td>
	</tr>
	<tr>
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo OFFER_AMEX; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['offer_amex'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['offer_amex'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   		if($PaymentDetails['offer_amex'] != "0" && $PaymentDetails['offer_amex'] != "1"){
		   			$lchecked = "checked";
		   		}
		   ?>
		   <span><input type="radio" id="offer_amex" name="offer_amex" value="1" <?php echo $lchecked;?>><?php echo YES; ?></span>
		   <span class="radio_distance"><input type="radio" id="offer_amex" name="offer_amex" value="0" <?php echo $tchecked;?>><?php echo NOT; ?></span>
		</td>
	</tr>
	<tr>
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_OFFER_AMEX'];?></td>
	</tr>
	<tr>
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>

	
	<input type="hidden" name="installment_conditions" id="installment_conditions" value="1" />
	
	<tr>
		<td class="form_caption" align="left"><?php echo MINIMUM_VALUE_PER_PLOT; ?> : </td>
		<td align="left"><input type="text" name="minimum_value_per_plot" id="minimum_value_per_plot" value="<?php echo $PaymentDetails['minimum_value_per_plot'];?>" class="text_box_small"></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_MINIMUM_VALUE_PER_PLOT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
		
	<tr>
		<td align="left" class="form_caption"><?php echo PAYPAL_STD_LBL_CURRENCY_CODE; ?> : </td>
		<td align="left">
			<select name="txt_currency_code" id="txt_currency_code" class="select_small">
				<option value="<?php echo 'AUD-036'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'AUD-036') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_AUSTRALIAN_DOLLAR.' => AUD'; ?></option>
				<option value="<?php echo 'BRL-986'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'BRL-986') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_BRAZILIAN_REAL.' => BRL'; ?></option>
				<option value="<?php echo 'CAD-124'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CAD-124') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_CANADIAN_DOLLAR.' => CAD'; ?></option>
				<option value="<?php echo 'CZK-203'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CZK-203') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_CZECH_KORUNA.' => CZK'; ?></option>
				<option value="<?php echo 'DKK-208'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'DKK-208') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_DANISH_KRONE.' => DKK'; ?></option>
				<option value="<?php echo 'EUR-978'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'EUR-978') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_EURO.' => EUR'; ?></option>
				<option value="<?php echo 'HKD-344'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'HKD-344') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_HONGKONG_DOLLAR.' => HKD'; ?></option>
				<option value="<?php echo 'HUF-348'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'HUF-348') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_HUNGARIAN_FORINT.' => HUF'; ?></option>
				<option value="<?php echo 'ILS-376'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'ILS-376') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_ISRAELI_NEW_SHEQEL.' => ILS'; ?></option>
				<option value="<?php echo 'JPY-392'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'JPY-392') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_JAPANESE_YEN.' => JPY'; ?></option>
				<option value="<?php echo 'MYR-456'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'MYR-456') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_MALAYSIAN_RINGGIT.' => MYR'; ?></option>
				<option value="<?php echo 'MXN-484'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'MXN-484') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_MEXICAN_PESO.' => MXN'; ?></option>
				<option value="<?php echo 'NOK-578'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'NOK-578') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_NORWEGIAN_KRONE.' => NOK'; ?></option>
				<option value="<?php echo 'NZD-578'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'NZD-578') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_NEW_ZEALAND_DOLLAR.' =>NZD'; ?></option>
				<option value="<?php echo 'PHP-608'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'PHP-608') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_PHILIPPINE_PESO.' => PHP'; ?></option>
				<option value="<?php echo 'PLN-985'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'PLN-985') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_POLISH_ZLOTY.' => PLN'; ?></option>
				<option value="<?php echo 'GBP-826'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'GBP-826') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_POUND_STERLING.' => GBP'; ?></option>
				<option value="<?php echo 'SGD-702'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'SGD-702') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SINGAPORE_DOLLAR.' => SGD'; ?></option>
				<option value="<?php echo 'SEK-752'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'SEK-752') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SWEDISH_KRONA.' => SEK'; ?></option>
				<option value="<?php echo 'CHF-756'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CHF-756') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SWISS_FRANC.' => CHF'; ?></option>
				<option value="<?php echo 'TWD-901'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'TWD-901') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_TAIWAN_NEW_DOLLAR.' => TWD'; ?></option>
				<option value="<?php echo 'THB-764'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'THB-764') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_THAI_BAHT.' => THB'; ?></option>
				<option value="<?php echo 'USD-840'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'USD-840') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_US_DOLLAR.' => USD'; ?></option>
				<option value="<?php echo 'TRY-949'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'TRY-949') { echo 'selected'; } ?>><?php echo 'Turkish lira'.' => TRY'; ?></option>
			</select>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_CURRENCY_CODE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>	
</table>