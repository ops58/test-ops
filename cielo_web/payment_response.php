<?php

// Return Array which is required into order confirmation page.
$response_details = array();

// 0=>failure, 1=>success, 2=>pending(This will be used to redirect to third party payment website i.e. Paypal Standard Method, CCAvenue )
$response_details['payment_ack_status'] = 2;

// Order Progress status id, set from admin side. if this is blank, it will take the default order status
$response_details["order_status"]	= '';

// Selected currency code (3 digit) from admin side
$response_details['currency_code'] = $PaymentDetails['txt_currency_code'];

// Transaction Id return by gateway
$response_details['transaction_id'] = '';

// Comment sent from payment gateways
$response_details["payment_comment"] = '';

// Serialized data. if other details need to add into database, received from payment gateway
$response_details["payment_response_details"] = '';

// 1 => send Confirmation mail, 0 => do not send Confirmation mail
$response_details["mail_status"] = 0;

// This will be used to redirect to third party payment website i.e. Paypal Standard Method, CCAvenue
$response_details["redirect_url"] = 'payment/cielo_web/cielopay.php';
