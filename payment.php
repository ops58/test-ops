<?php
/* Payment Configuration Constants */
define('ELEMENT_PAYMENT_ID', 'txt_pay_method_id');
define('ELEMENT_ENABLE', 'rbd_enable');
define('ELEMENT_ORDER_STATUS', 'select_order_status');
define('ELEMENT_BUTTON_SAVE', 'btnsubmitship');	
define('ELEMENT_PAYMENT_REQUEST', 'rbd_payment_request');
/* Payment Configuration Constants End Here */
function getpaymentMethod ($payment_method_id=null, $enable_payon_account=false,$payment_request=false,$shipment_request =false,$offline_order =false)
{	
	global $__Session, $_SITE_VAR_CORPORATE_APPROVAL_CHEQUE;
	require_once(DIR_WS_MODEL . "PaymentMethodMaster.php");
	$ObjPaymentMethodMaster = new PaymentMethodMaster();
	$PaymentMethods = array();
    $storeID = getAccessObj()->getStoreID();
    $userTypeID = getAccessObj()->getUserType();
    $isStore = getAccessObj()->isStore();
	if ($payment_method_id != "") {
		if($__Session->HasValue('_session_checkout_details')) {
			$checkout_details_arr = $__Session->GetValue('_session_checkout_details');
			$CustomerPaymentDetails = $checkout_details_arr['post_data'];
		}
		$key = "";
		if ($payment_method_id != "") {
			$ObjPaymentMethodMaster->setWhere("AND  payment_method_id = :payment_method_id", $payment_method_id, 'int');
			$key = "payment_method_id";
		}

		// Get Payment Method Details
		$ObjPaymentMethodMaster->setOrderBy("sort_order ASC");
		$DataDomainPayment = $ObjPaymentMethodMaster->getPaymentMethod();
		if(!empty($DataDomainPayment)) {
			$PaymentDetails = unserialize($DataDomainPayment[0]['payment_details']);
			$PaymentMethods['payment_method_id'] = $payment_method_id;
			$PaymentMethods['payment_title'] = (!empty($PaymentDetails['payment_title'][SITE_LANGUAGE_ID])?$PaymentDetails['payment_title'][SITE_LANGUAGE_ID]:ucfirst($DataDomainPayment[0]['payment_method_name']));
			$dirName = $DataDomainPayment[0]['directory'];
			$PaymentMethods['template_file'] = DIR_WS_PAYMENT . $dirName . "/payment_template.php";
			$PaymentMethods['payment_note'] = $PaymentDetails['payment_note'][SITE_LANGUAGE_ID];
			$PaymentMethods['invoice_message'] = $PaymentDetails['invoice_message'][SITE_LANGUAGE_ID];
			$syncfilename  = DIR_WS_PAYMENT.'/' . $dirName . '/synchronize_data.php';
            if(opsIsFile($syncfilename, false)) {
				require_once ($syncfilename);
				$PaymentMethods['payer_details'] = $Customer_Payment_Details;
			}
		}
		/*if($payment_method_id == 20 && $_GET['action'] == 'express') {
			array_push($PaymentMethods, $express_checkout_details_arr);
		}*/
        if($isStore) {
			if (defined('CORPORATE_SETTING_PAYMENT_OPTIONS_METHOD') && CORPORATE_SETTING_PAYMENT_OPTIONS == 'set_payment_options') {
				$shipping_methods_aval = explode(';',CORPORATE_SETTING_PAYMENT_OPTIONS_METHOD);				
				if($PaymentMethods['payment_method_id'] != 1 && !in_array($PaymentMethods['payment_method_id'],$shipping_methods_aval)){
					unset($PaymentMethods);
				}
			}
		}
					
	} else {
		// Get Payment Method Details
		$ObjPaymentMethodMaster->setOrderBy("sort_order");
		/* corporate customer hide price */ 
		// For normal order & Shipment request if price is hide by corporate then only payon should available.
		// Normal payment request ( Direct Payment) wil display all online methods.
		if (!is_price_visible() && ( ($payment_request === true && $shipment_request === true) || ($payment_request === false && $shipment_request === false) )) {
			$ObjPaymentMethodMaster->setWhere("AND  directory = :directory", 'payon_account', 'string');
		}
		if($payment_request === true)
		{
// 			if (!is_price_visible()) {$SearchSiteDomainPayment =array();}
		    $ObjPaymentMethodMaster->setWhere(" AND status = :status" ,'1','string');
            if(!empty($storeID) && $storeID > 0) {
		    	$ObjPaymentMethodMaster->setWhere(" AND FIND_IN_SET(:user_type_2,user_type) ", '2', 'string');
		    } else {
		    	$ObjPaymentMethodMaster->setWhere(" AND FIND_IN_SET(:user_type_1,user_type) ", '1', 'string');
		    }
			if($shipment_request === false ){
			    // for normal payment request load payment methods which available for direct payment.
				$ObjPaymentMethodMaster->setWhere("AND  payment_request = :payment_request", 2, 'string');
			}
			else {
			    // For shipment request allowed payon including other online payment methods.
				$ObjPaymentMethodMaster->setWhere("AND ( payment_request = :payment_request OR  directory = :directory_payon )", 
						array(':payment_request'=>2, ':directory_payon' => 'payon_account'),
						array(':payment_request'=>'string', ':directory_payon' => 'string'));
			}
		}
		/**
		 * POS payment method should only visible to branch admin & superadmin.
		 */
		if(/* defined('DISPLAY_BRANCH_MODULE') && strtolower(DISPLAY_BRANCH_MODULE) == 'yes' && */ $__Session->HasValue("_Sess_Admin_Login") !== false) {
		    // admin login
		    $admin_details = getAccessObj()->getAdminDetails();
		    $load_POS = $load_moneris = false;
		    if($admin_details->is_branch || $admin_details->superadmin || ($admin_details->group_id > 0 && empty($admin_details->corporate_id))) {
		        $load_POS = $load_moneris = true;
		    }
		    if($load_POS == false){
		        $ObjPaymentMethodMaster->setWhere(" AND directory != :directory_pos" ,'pos','string');
		    }
		    if($load_moneris == false) {
		        $ObjPaymentMethodMaster->setWhere(" AND directory != :directory_moneris" ,'moneris','string');
		    }
		}
		else {
		    $ObjPaymentMethodMaster->setWhere(" AND directory != :directory_pos" ,'pos','string');
		    $ObjPaymentMethodMaster->setWhere(" AND directory != :directory_moneris" ,'moneris','string');
		}		
		
		if($offline_order === true){
		 // $ObjPaymentMethodMaster->setWhere(" AND ( (status = :status" ,'1','string');    // Check online methods status
		 // $ObjPaymentMethodMaster->setWhere(" AND  online = :online_1)  " ,'1','string'); // Online = 1 means all online methods
		//	$ObjPaymentMethodMaster->setWhere(" OR online = :online_2 " ,'0','string'); //All offline methods should come in offline order
		    
		    $ObjPaymentMethodMaster->setWhere(" AND status = :status" ,'1','string'); // Check status of payment method
		    if(!empty($storeID) && $storeID > 0){
		        $ObjPaymentMethodMaster->setWhere(" AND (FIND_IN_SET(:user_type_2,user_type)  ", '2', 'string');
		    } else if(empty($storeID)){
		        $ObjPaymentMethodMaster->setWhere(" AND (FIND_IN_SET(:user_type_1,user_type)  ", '1', 'string');
		    }
		    $ObjPaymentMethodMaster->setWhere(" OR FIND_IN_SET(:user_type_3,user_type))  ", '3', 'string');
		}
		else if($payment_request === false){
			$ObjPaymentMethodMaster->setWhere(" AND ( (status = :status" ,'1','string');
			
			if(!empty($storeID) && $storeID > 0){
				$ObjPaymentMethodMaster->setWhere(" AND FIND_IN_SET(:user_type_2,user_type) ) ", '2', 'string');
			} else {
				$ObjPaymentMethodMaster->setWhere(" AND FIND_IN_SET(:user_type_1,user_type) )  ", '1', 'string');
			}
			$ObjPaymentMethodMaster->setWhere(" OR (directory = :directory_cheque " ,'cheque','string');
			$ObjPaymentMethodMaster->setWhere(" AND payment_method_id= :cheque_payment_method_id )) " ,'1','string');
		}
		// Added for offline payment method for retailer
        if(!empty($userTypeID) && $userTypeID == 1 && $payment_request === false && defined('RETAILER_OFFLINE_PAYMENT_METHOD') && !empty(RETAILER_OFFLINE_PAYMENT_METHOD)) {
		    $ObjPaymentMethodMaster->setWhere("OR (payment_method.payment_method_id IN @payment_method_payment_method_id", explode(',', RETAILER_OFFLINE_PAYMENT_METHOD), 'string');
		    $ObjPaymentMethodMaster->setWhere("AND FIND_IN_SET(:payment_method_user_type,payment_method.user_type))", "1", "string");
		}
				
		$DataDomainPayment = $ObjPaymentMethodMaster->getPaymentMethod();
	
		if ($DataDomainPayment) {
			foreach ($DataDomainPayment as $key => $Value){
				//if ($Value['status'] == 1 || $Value['payment_method_id'] == 1) { // OR condition Added by jiten bhavsar because Cheque payment method always fetched regardless of it's status. 
					
                    if((((getAccessObj()->isStoreEnabled()) && (defined('ENABLE_CORPORATE_PAYON_ACCOUNT') && ENABLE_CORPORATE_PAYON_ACCOUNT === true) && 
							(!empty($storeID) && $storeID > 0)) || ((!empty($userTypeID) && $userTypeID == 1) && (defined('ENABLE_RETAILER_PAYON_ACCOUNT') && ENABLE_RETAILER_PAYON_ACCOUNT))) && ($Value['directory'] == 'payon_account' && $enable_payon_account === true)) {
						$PaymentDetails = unserialize($Value['payment_details']);
						$dirName = $Value['directory'];
						foreach ($PaymentMethods as $paymentkey => $paymentDataKey) {
							$PaymentMethods[$paymentkey]['default_method'] = '0';
						}
						$PayonPaymentMethods = array();
						$PayonPaymentMethods['payment_method_id'] = $Value['payment_method_id'];
						$PayonPaymentMethods['name'] = $Value['payment_method_name'];
						$PayonPaymentMethods['default_method'] = '1';
						$PayonPaymentMethods['directory'] = $dirName;
						$PayonPaymentMethods['template_file'] = DIR_WS_PAYMENT.$dirName."/payment_template.php";
						$PayonPaymentMethods['payment_title'] = $PaymentDetails['payment_title'][SITE_LANGUAGE_ID];
						$PayonPaymentMethods['payment_note'] = $PaymentDetails['payment_note'][SITE_LANGUAGE_ID];
						$PayonPaymentMethods['payment_status'] = $Value['status'];
						$PayonPaymentMethods['disable_rule_condition'] = $Value['disable_rule_condition'];
						$PayonPaymentMethods['user_type'] = explode(',',$Value['user_type']);
						if(!empty($storeID) && $storeID > 0)
								array_unshift($PaymentMethods, $PayonPaymentMethods);
						else 
								$PaymentMethods[] = $PayonPaymentMethods;
						
					} elseif ($Value['directory'] != 'payon_account') {
						$PaymentDetails = unserialize($Value['payment_details']);
	
						$dirName = $Value['directory'];
						$PaymentMethods[$key]['payment_method_id'] = $Value['payment_method_id'];
						$PaymentMethods[$key]['name'] = $Value['payment_method_name'];
						$PaymentMethods[$key]['default_method'] = $Value['default_method'];
						$PaymentMethods[$key]['directory'] = $dirName;
						$PaymentMethods[$key]['template_file'] = DIR_WS_PAYMENT.$dirName."/payment_template.php";
						$PaymentMethods[$key]['disable_rule_condition'] = $Value['disable_rule_condition'];
						$PaymentMethods[$key]['payment_title'] = $PaymentDetails['payment_title'][SITE_LANGUAGE_ID];
						$PaymentMethods[$key]['payment_note'] = $PaymentDetails['payment_note'][SITE_LANGUAGE_ID];
						$PaymentMethods[$key]['payment_status'] = $Value['status'];	
						$PaymentMethods[$key]['user_type'] = explode(',',$Value['user_type']);
					}
				//}
			}
		}
        if($isStore) {
			if (defined('CORPORATE_SETTING_PAYMENT_OPTIONS_METHOD') && CORPORATE_SETTING_PAYMENT_OPTIONS == 'set_payment_options') {
				$shipping_methods_aval = explode(';',CORPORATE_SETTING_PAYMENT_OPTIONS_METHOD);
				foreach ($PaymentMethods as $key=>$payment_method){
					if(!in_array($payment_method['payment_method_id'],$shipping_methods_aval)){
						// Check if cheque method is exclude from corporate specific payment options.Need to add cheque method in inactive status. later on might be getting enable if paid amount getting zero after coupon redemption  
						if($payment_method['payment_method_id'] == 1 && !in_array($payment_method['payment_method_id'],$shipping_methods_aval)){ 
							$PaymentMethods[$key]['payment_status']=0;
						}
						else
							unset($PaymentMethods[$key]);
					}
				}
			}
		}
		if($payment_request === false && $offline_order == false){
			foreach ($PaymentMethods as $key=>$payment_method){		       
				// Check if cheque method is inactive OR not available for current logged in user type. Need to add cheque method in inactive status. later on might be getting enable if paid amount getting zero after coupon redemption
				if($payment_method['payment_method_id'] == 1 && $payment_method['payment_status'] == 1 && !in_array($userTypeID, $payment_method['user_type']) ){
					$PaymentMethods[$key]['payment_status']=0;
				}
			}
		}
	}
	
	//in case of corporate order approval is on & order amount 0 at that cheque also visible , which DZD-703-84749 do not wants to display.Add new variable
	if($_SITE_VAR_CORPORATE_APPROVAL_CHEQUE == '1' && $isStore) {
		$unset_cheque = false;
		foreach ($PaymentMethods as $key=>$payment_method){
			if($payment_method['payment_method_id'] == 1 && $payment_method['payment_status'] == 0){
				$cheque_key = $key;
				$unset_cheque = true;
			}
		}
		if($unset_cheque) {
			unset($PaymentMethods[$cheque_key]);
		}
	}
	
	return $PaymentMethods;
}

function dopayment($checkout_details) {
	require_once(DIR_WS_MODEL . "PaymentMethodMaster.php");
	$ObjSitePaymentMethodMaster = new PaymentMethodMaster();
	$ObjSitePaymentMethodMaster->setWhere("AND payment_method_id = :payment_method_id", $checkout_details->PaymentMethod, 'int');
	//$ObjSitePaymentMethodMaster->setWhere("AND payment_method_id = :payment_method_id2", 20, 'int');

	// Get Payment Method Details
	$DataDomainPayment = $ObjSitePaymentMethodMaster->getPaymentMethod();
	if(!empty($DataDomainPayment)) {
		$DataDomainPayment = $DataDomainPayment[0];
		$PaymentDetails = unserialize($DataDomainPayment['payment_details']);
		$_POST['payment_details'] = $PaymentDetails;
		
		$arr_admin_cur_data = array();
		$arr_admin_cur_data = create_currency_session('', $arr_currency_data);
		// Start : Code added by Sandip
		$sel_curr_decimalpoint = $arr_admin_cur_data['currency_decimalpoint'];
		$sel_curr_value = $arr_admin_cur_data['currency_value'];
		$sel_curr_code = $arr_admin_cur_data['currency_code'];
		if(!empty($PaymentDetails['txt_currency_code']) && $PaymentDetails['txt_currency_code']!=$sel_curr_code) {
			$sel_curr_code = $PaymentDetails['txt_currency_code'];
			$ObjSiteCurrencyMaster = new CurrencyMaster();				
			$ObjSiteCurrencyMaster->setWhere("AND currency_code = :currency_code", $PaymentDetails['txt_currency_code'], 'string');
			$DataSiteCurrency = $ObjSiteCurrencyMaster->getCurrency();
			$DataSiteCurrency = $DataSiteCurrency[0];
			if(!empty($DataSiteCurrency)) {
				$sel_curr_decimalpoint = $DataSiteCurrency['decimal_point'];
				$sel_curr_value = $DataSiteCurrency['currency_value'];
			}
		}// End : Code added by Sandip
		$checkout_details->payment_method_curreny_code = $sel_curr_code;
		$checkout_details->payment_method_amount = currency_format($checkout_details->payment_method_amount,'.',null,null,null,null,$sel_curr_value,true);
	
		require_once(DIR_WS_PAYMENT.$DataDomainPayment['directory'].'/payment_response.php');
		
		// Serialized data. Site Payment details set from admin panel
		$response_details["site_payment_details"] = $DataDomainPayment;
	
		// Payment Method id from payment_method table
		$response_details["payment_method_id"] = $DataDomainPayment['payment_method_id'];
	}

	//$resArray["site_payment_method"] = $DataDomainPayment;
	return $response_details;
}

function createPaymentConfigForm($PaymentMethodData=null, $SiteLanguageData = null) {

	global $processstatus; 		// Process Status after Order Comfirmation	
	global  $help_messages;
	
	$sep_width=1;
	$sep_height=25;
	require_once(DIR_WS_MODEL . "ProcessStatusMaster.php");
	$ObjProcessStatusMaster = new ProcessStatusMaster();
	
	if(!empty($PaymentMethodData)) {
		
		$payment_method_id = $PaymentMethodData['payment_method_id'];
		$payment_method_name = $PaymentMethodData['payment_method_name'];
		$DirectoryPayment = $PaymentMethodData['directory'];
		$paymentRequest = $PaymentMethodData['payment_request'];
		$defaultMethod = $PaymentMethodData['default_method'];
		$statusEnable = $PaymentMethodData['status'];
		$sort_order   = $PaymentMethodData['sort_order'];
		

		$PaymentDetails = unserialize($PaymentMethodData['payment_details']);
		if(!empty($_POST)) {
			$_POST = array_merge($PaymentDetails, $_POST);
		} else {
			$_POST = $PaymentDetails;
		}
		$_POST['payment_details'] = $PaymentDetails;
		/*if(!empty($_POST)) {
			$_POST = array_merge($PaymentDetails, $_POST);
		} else {
			$_POST = $PaymentDetails;
		}*/
		//unset($PaymentDetails);
		// Configuration Form File
		$formFileName = DIR_WS_PAYMENT . '/' . $DirectoryPayment . '/payment_config.php';
		
	// Use $hiddenVariables, in the form of config_form.php in all shipping directory
	
?>		
		<form name="frmpayment" method="POST" >
		<input type="hidden" id="<?php echo ELEMENT_PAYMENT_ID; ?>" name="<?php echo ELEMENT_PAYMENT_ID; ?>" value="<?php echo $payment_method_id; ?>" />
		<table width="100%" cellpadding="0" cellspacing="5" border="0" align="left" >
			<tr>
				<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_SELECT_STATUS; ?> : </td>
				<td align="left">
					<span><input type="radio" class="radiochk" id="<?php echo ELEMENT_ENABLE; ?>" name="<?php echo ELEMENT_ENABLE; ?>" value="1"  <?php if($statusEnable == '1' ){ ?>checked="checked"<?php } ?> /><?php echo PAYMENT_LBL_ENABLE; ?></span>
					<?php if($defaultMethod !='1') {?>
					<span class="radio_distance"><input type="radio" class="radiochk" id="<?php echo ELEMENT_ENABLE; ?>" name="<?php echo ELEMENT_ENABLE; ?>" value="0" <?php if($statusEnable == '0' || empty($statusEnable) ){ ?>checked="checked"<?php } ?> /><?php echo PAYMENT_LBL_DISABLE; ?></span>
					<?php } ?>
				</td>
			</tr>
			<tr>											
				<td><?php echo draw_separator($sep_width,$sep_height);?></td>
				<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_STATUS'];?></td>
			</tr>
			<tr>											
				<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
			</tr>
			<tr>
				<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_SORT_ORDER; ?> : </td>
				<td align="left">
					<input type="text" id="payment_sort_order" name="payment_sort_order" value="<?php echo $sort_order;  ?>" class="text_box_small" />
				</td>
			</tr>
			<tr>											
				<td><?php echo draw_separator($sep_width,$sep_height);?></td>
				<td class="help_msg"><?php echo $help_messages['HELP_SORT_ORDER'];?></td>
			</tr>	
			<tr>											
				<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
			</tr>					
		<?php 
		
		$ObjProcessStatusMaster->setWhere("AND order_process_status.status_type = :ops_status_type", '0','string');
		//Hide pending status from external payment gateway
		if(!in_array($DirectoryPayment, array('cheque', 'payon_account')))
			$ObjProcessStatusMaster->setWhere("AND order_process_status.process_status_id != :ops_process_status_id", '1','string');
		$processData = $ObjProcessStatusMaster->getProcessStatusDesc(DEFAULT_LANGUAGE_ID,null,'1');
		$process_stat = $_POST['payment_details'][ELEMENT_ORDER_STATUS];
		if(isset($processData) && $processData != '') { ?>
			<tr>
				<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_COMMON_LBL_STATUS; ?> : </td>
				<td align="left">
				<select id="<?php echo ELEMENT_ORDER_STATUS; ?>" name="<?php echo ELEMENT_ORDER_STATUS; ?>" class="select_small" >
			<?php 
				foreach ($processData as $pdata) {
					if(empty($process_stat) && $pdata['process_status_default'] == 1) {
						$process_stat = $pdata['process_status_id'];
					}
					?>
					<option value="<?php echo $pdata['process_status_id']; ?>" <?php if($process_stat == $pdata['process_status_id']) { echo 'selected'; } ?> ><?php echo $pdata['process_status_title']; ?></option>
			<?php	}
			?>
				</select>
				</td>
			</tr>
			<tr>											
				<td><?php echo draw_separator($sep_width,$sep_height);?></td>
				<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_ORDER_STATUS'];?></td>
			</tr>	
										
	<?php	
		} ?>
		
		<?php 
	 // Check Configuration file ('config_form.php') is exists or not in all seperate shipping modules directory
        if(opsIsFile($formFileName, false)) { ?>
			<tr>
				<td align="left"  colspan="2">
					<?php require_once($formFileName); 		// include Configuration file ?>
				</td>
			</tr>
			<!-- $paymentRequest=0 means this payment method is not configure for payment request -->
			<?php if($paymentRequest!=0){ ?> 
			<tr>
				<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_AVAILABLE_REQUEST_PAYMENT; ?> : </td>
				<td align="left">
					<span><input type="radio" class="radiochk" id="<?php echo ELEMENT_PAYMENT_REQUEST; ?>" name="<?php echo ELEMENT_PAYMENT_REQUEST; ?>" value="2"  <?php if($paymentRequest == '2' ){ ?>checked="checked"<?php } ?> /><?php echo PAYMENT_LBL_MAKE_DEFAULT_YES; ?></span>
					
					<span class="radio_distance"><input type="radio" class="radiochk" id="<?php echo ELEMENT_PAYMENT_REQUEST; ?>" name="<?php echo ELEMENT_PAYMENT_REQUEST; ?>" value="1" <?php if($paymentRequest == '1' || empty($paymentRequest) ){ ?>checked="checked"<?php } ?> /><?php echo PAYMENT_LBL_MAKE_DEFAULT_NO; ?></span>
					
				</td>
			</tr>
			<tr>											
				<td><?php echo draw_separator($sep_width,$sep_height);?></td>
				<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_REQUESTPAYMENT'];?></td>
			</tr>
			<tr>											
				<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
			</tr>
			<?php } else { ?>
				<input type="hidden" name="<?php echo ELEMENT_PAYMENT_REQUEST; ?>" id="<?php echo ELEMENT_PAYMENT_REQUEST; ?>" value="0" >
				<tr>											
					<td></td>
					<td class="help_msg message_mendatory"><?php echo $help_messages['HELP_PAYMENT_REQUESTPAYMENT_NOT_AVAILABLE'];?></td>
				</tr>
			<?php } ?>
			<tr>
				<td align="left" class="helpmsg" colspan="2"><?php echo ADMIN_LANGUAGE_PAYMENT_NOTE;?></td>
				
			</tr>
	<?php	} 
	
		/** When Languagewise data **/ 
			if($SiteLanguageData != '') { ?>
			<tr>
				<td colspan="2" align="left">
				<div id="TabbedPanels1" class="TabbedPanels">
					<ul class="TabbedPanelsTabGroup">
				<?php foreach($SiteLanguageData as $arrid => $Languages) { ?>
						<li class="TabbedPanelsTab" tabindex="<?php echo $arrid; ?>" ><span><?php echo $Languages['site_language_name']; ?></span></li>
				<?php } ?>
					</ul>
				
			<div class="TabbedPanelsContentGroup">	
			<?php foreach ($SiteLanguageData as $Languages) { 
				$PaymentMethodName = $_POST['payment_details']['payment_title'][$Languages['site_language_id']];
				$invoice_message = $_POST['payment_details']['invoice_message'][$Languages['site_language_id']];
				
				if(empty($PaymentMethodName)) {
					$PaymentMethodName = $payment_method_name;
				}
			   ?>
					<div class="TabbedPanelsContent">
						<div id="container">
                           <div id="jquery_table"  class="jquery_pagination" >
						<table width="100%" cellpadding="2" cellspacing="5" border="0" align="center" >
							
						    <tr>
								<td align="left" valign="top" class="form_caption" width="26%"><?php echo PAYMENT_COMMON_LBL_TITLE; ?> : </td>
								<td><input type="text" id="payment_title" name="payment_title[<?php echo $Languages['site_language_id']; ?>]" value="<?php echo $PaymentMethodName;  ?>" class="text_box_std" /></td>
							</tr>
							
							<?php if($Languages['site_language_id'] == DEFAULT_LANGUAGE_ID) { ?>								
							<tr>											
								<td><?php echo draw_separator($sep_width,$sep_height);?></td>
								<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_TITLE'];?></td>
							</tr>
							<tr>											
								<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
							</tr>		
							<?php } else {?>
							<tr>
								<td><?php echo draw_separator($sep_width,$sep_height);?></td>																	
							</tr>
							<tr>
								<td><?php echo draw_separator($sep_width,$sep_height);?></td>
							</tr>
							<?php } ?>
							<tr>
								<td><?php echo draw_separator($sep_width,$sep_height);?></td>																	
							</tr>
							<tr>
								<td align="left" valign="top" class="form_caption" width="26%"><?php echo PAYMENT_COMMON_INVOICE_MESSAGE; ?> : </td>
								<td><?php CreateCKeditor('invoice_message['.$Languages['site_language_id'].']',$invoice_message,'530px','200','RadixToollbarMedium');?></textarea>
								</td>
							</tr>
							<tr>											
								<td><?php echo draw_separator($sep_width,$sep_height);?></td>
								<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_INVOICE_MESSAGE'];?></td>
							</tr>
							<tr>
								<td><?php echo draw_separator($sep_width,$sep_height);?></td>
							</tr>
							<tr>
								<td align="left" valign="top" class="form_caption" width="26%"><?php echo PAYMENT_COMMON_LBL_NOTE; ?> : </td>
								<td><?php CreateCKeditor('payment_note['.$Languages['site_language_id'].']',$_POST['payment_details']['payment_note'][$Languages['site_language_id']],'100%','200','TemplatesToolbar'); ?>
									<!--<textarea name="payment_note[<?php echo $Languages['site_language_id']; ?>]" id="payment_note" rows="5" cols="40"><?php echo $_POST['payment_details']['payment_note'][$Languages['site_language_id']];  ?></textarea>-->
								</td>
							</tr>
							<?php if($Languages['site_language_id'] == DEFAULT_LANGUAGE_ID) { ?>
							<tr>											
								<td><?php echo draw_separator($sep_width,$sep_height);?></td>
								<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_NOTE'];?></td>
							</tr>
							<?php } else {?>
							<tr>
								<td><?php echo draw_separator($sep_width,$sep_height);?></td>																	
							</tr>	
							<?php } ?>
						</table>
					      </div>
					    </div>
					</div>      
			<?php } ?>
				</div>
				</div>
				</td>
			</tr>
		<?php } else { ?>
			<tr>
				<td align="left" valign="top" class="form_caption" width="26%"><?php echo PAYMENT_COMMON_LBL_TITLE; ?> : </td>
				<td><input class="text_box_std" type="text" id="payment_title" name="payment_title" value="<?php echo $_POST['payment_title'];  ?>" /></td>
			</tr>
			<tr>
				<td align="left" valign="top" class="form_caption" width="26%"><?php echo PAYMENT_COMMON_LBL_NOTE; ?> : </td>
				<td><?php CreateCKeditor('payment_note',$_POST['payment_note'],'100%','200','TemplatesToolbar'); ?>
				<!--<textarea name="payment_note" id="payment_note" rows="5" cols="40"><?php echo $_POST['payment_note'];  ?></textarea>-->
				</td>
			</tr>
		<?php } ?>
			<tr>
				<td align="left"><?php echo draw_separator($sep_width,$sep_height);?></td>
				<td align="left"><input type="submit"  class="action_button" name="<?php echo ELEMENT_BUTTON_SAVE; ?>" id="<?php echo ELEMENT_BUTTON_SAVE; ?>" value="<?php echo FORM_LABEL_SAVE_SHIPPING_CONFIG; ?>" /></td>
			</tr>
		</table>
		</form>
<?php		
	}
}

/**
 * It is used for the Third website redirection to make payment. For eg. Paypal Standard, Google Checkout, Authorize_sim
 * Once Complete the payment process on website and back to our site with transaction details.
 * 
 * @param array $payment_transaction_details
 * 	  i.e.
 *       $payment_transaction_details['order_id'] = integer.
 *       $payment_transaction_details['transaction_id'] = mixed(Alphanumeric)
 *       $payment_transaction_details['payment_remarks'] = Alphabeticals
 *       $payment_transaction_details['extra_info']  = Array / String Other information.
 */
function update_order($payment_transaction_details) {
	
	if(!empty($payment_transaction_details['order_id'])) {
		
	/**
	 * Variable Declaration
	 */
		$order_number = $payment_transaction_details['order_id'];
		$transaction_id = $payment_transaction_details['transaction_id'];
		$payment_status_details = $payment_transaction_details['payment_remarks'];
		$extra_info = $payment_transaction_details['extra_info'];
		if(is_array($extra_info)) {
			$extra_info = serialize($extra_info);
		}

	/**
	 * File Inlcusion
	 */
		require_once(DIR_WS_MODEL."PaymentMethodMaster.php");	// Payment Method Master file
		require_once(DIR_WS_MODEL."PaymentDetailsMaster.php");
		require_once(DIR_WS_MODEL."OrderMaster.php");
		require_once(DIR_WS_MODEL."orderHistoryMaster.php");
		require_once(DIR_WS_PAYMENT."payment.php");
		require_once(DIR_WS_MODEL . "ProcessStatusMaster.php");

	/**
	 * Object Declaration
	 */
		$ObjPaymentDetailsMaster= new PaymentDetailsMaster();
		$PaymentDetailsDataObj	= new PaymentDetailsData();
		$OrderMasterObj         = new OrderMaster();
		$OrderDataObj	        = new OrderData();
		$ObjOrderHistoryMaster	= new OrderHistoryMaster();
		$OrderHistoryData		= new OrderHistoryData();
		$ObjPaymentMethodMaster = new PaymentMethodMaster();	// Payment Method Master Object
		$ObjProcessStatusMaster  = new ProcessStatusMaster();
		
	/**
	 * Get Order Payment Details 
	 */
		$ObjPaymentDetailsMaster->setWhere("AND  Orders_Id = :Orders_Id", $order_number, 'int');
		$payment_details = $ObjPaymentDetailsMaster->getPaymentDetails();
		$OrderPayment = $payment_details[0];
		
		$OrderMasterObj->setSelect(array('orders.orders_id','orders.user_id'));
		$OrderMasterObj->setWhere("AND orders.orders_id = :orders_orders_id", $order_number, 'int');
		$OrderDetails = $OrderMasterObj->getOrder();
		$OrderDetails = $OrderDetails[0];
		$OrderDetails['payment_method'] = $OrderPayment['payment_method'];
		$OrderDetails['total_amount'] = $OrderPayment['total_amount'];
		
	/**
	 * Get Payment Method Details 
	 */
		$ObjPaymentMethodMaster->setWhere("AND  payment_method_id = :payment_method_id", $OrderPayment['payment_method'], 'int');
		$DataDomainPayment = $ObjPaymentMethodMaster->getPaymentMethod();
		$PaymentDetails = unserialize($DataDomainPayment[0]['payment_details']);
		
	/**
	 * Update the Order Process Status Details in Order Table
	 */
		$OrderDataObj->orders_id 		= $order_number;
		$OrderDataObj->orders_status_id	= $PaymentDetails[ELEMENT_ORDER_STATUS];
		$OrderDataObj->order_last_modified 	= date('Y-m-d H:i:s');
		$OrderMasterObj->editOrder($OrderDataObj, "yes");
		
	/**
	 * Add comments for such transaction into history table
	 */
		$ObjProcessStatusMaster->setSelect("order_process_status.process_status_id");
		$ObjProcessStatusMaster->setWhere("AND process_status_complete = :process_status_complete","1","string");
		$processData = $ObjProcessStatusMaster->getProcessStatus();
		foreach($processData as $value) {
		    $completed_process_status_id[] =  $value->process_status_id;
		}
		
        if(getAccessObj()->getUserType() == '1' && in_array($PaymentDetails[ELEMENT_ORDER_STATUS],$completed_process_status_id)){
		    cashback_calculation_for_customer($OrderDetails,$completed_process_status_id);
		}
		$OrderHistoryData->orders_status_id 	= $PaymentDetails[ELEMENT_ORDER_STATUS];
		$OrderHistoryData->comments 		= '';
		$OrderHistoryData->customer_notified 	= '0';
		$OrderHistoryData->orders_id 		= $order_number;
		$OrderHistoryData->date_added		= date('Y-m-d H:i:s');
		$OrderHistoryData->createdby = "A";
		// Added In Order History table
		$ObjOrderHistoryMaster->addOrderHistory($OrderHistoryData);
		
	/**
	 * Update the Payment Details
	 */
		$PaymentDetailsDataObj->orders_id = $order_number;
		$PaymentDetailsDataObj->remark = $payment_status_details;
		$PaymentDetailsDataObj->transactionid = $transaction_id;
		$PaymentDetailsDataObj->payment_response_data = $extra_info;
		$PaymentDetailsDataObj->payment_id = $OrderPayment->payment_id;
		$PaymentDetailsDataObj->payment_method = $OrderPayment->payment_method;
		$PaymentDetailsDataObj->order_amount = $OrderPayment->order_amount;
		$PaymentDetailsDataObj->shipping_amount = $OrderPayment->shipping_amount;
		$PaymentDetailsDataObj->total_amount = $OrderPayment->total_amount;
		$PaymentDetailsDataObj->payment_date = $OrderPayment->payment_date;
		$PaymentDetailsDataObj->city_tax_amount = $OrderPayment->city_tax_amount;
		$PaymentDetailsDataObj->message = $OrderPayment->message;
		$PaymentDetailsDataObj->coupon_amount = $OrderPayment->coupon_amount;
		// Update the payment Details
		$ObjPaymentDetailsMaster->editPaymentDetails($PaymentDetailsDataObj, true);

	// Sent mail to customer and also admin 	
		Order_Completion_Front($order_number);
	}
}

/**
 * Update Payment details after payment is processed.
 *
 * @param int $order_id Order ID
 * @param bool $success Payment is successful or not
 * @param string $transaction_id Transaction ID for payment
 * @param string $response Serialized Response received from payment gateway
 * @param Array $PaymentDetails Payment Gateway settings
 * @param string $remark Remarks for the updation
 * @param bool $ipn Whether request is from ipn or redirection method
 */
function updatepaymentdetails($order_id=null,$success=false,$transaction_id=null,$response=null,$PaymentDetails=null,$remark=null,$ipn=false,$sendmail=true){
	if(empty($order_id)) {
		throw new Exception("Order ID is missing !!");
	}
    global $_SITE_VAR_INVOICE_NUMBER_BASED_ON_ORDER_STATUS;
	
	require_once(DIR_WS_MODEL."PaymentMethodMaster.php");
	require_once(DIR_WS_MODEL."PaymentDetailsMaster.php");
	require_once(DIR_WS_MODEL . "OrderProductMaster.php");
	require_once(DIR_WS_MODEL."OrderMaster.php");
	require_once(DIR_WS_MODEL."orderHistoryMaster.php");
	require_once(DIR_WS_MODEL."ProcessStatusMaster.php");
	
	$PaymentDetailsMasterObj= new PaymentDetailsMaster();
	$PaymentDetailsDataObj	= new PaymentDetailsData();
	
	$ObjPaymentMethodMaster = new PaymentMethodMaster();
	$OrderMasterObj         = new OrderMaster();
	$OrderDataObj	        = new OrderData();
	$ObjOrderHistoryMaster	= new OrderHistoryMaster();
	$OrderHistoryData		= new OrderHistoryData();
	$ObjProcessStatusMaster = new ProcessStatusMaster();
	
	//Fetch order payment details
	$PaymentDetailsMasterObj->setWhere("AND orders_id = :orders_id", $order_id, 'int');
	$data_payment_details  = $PaymentDetailsMasterObj->getPaymentDetails();
	$currentPaymentDetails = $data_payment_details[0];
	$PaymentDetails = getPaymentMethodSettings($currentPaymentDetails['payment_method']);
	if(!is_string($response)){
		$response = serialize($response);
	}
	
	//If response is success
	if($success === true && !empty($currentPaymentDetails)) {
	    
        // Find order if transaction id not set. Result will become not empty if transaction id already set.
	    $PaymentDetailsMasterObj->setWhere("AND orders_id = :orders_id AND transactionid !=  ''", $order_id, 'string');
	    $paymentDetailsTrans = $PaymentDetailsMasterObj->getPaymentDetails(null,$searchPayment);
	    
	    //Empty result means still transaction id not set :	    	  
	    if($paymentDetailsTrans == "") {
	        
	        $OrderMasterObj->setSelect(array('orders.orders_id','orders.user_id'));
	        $OrderMasterObj->setWhere("AND orders.orders_id = :orders_orders_id", $order_id, 'int');
	        $OrderDetails = $OrderMasterObj->getOrder();
	        $OrderDetails = $OrderDetails[0];
	        $OrderDetails['payment_method'] = $currentPaymentDetails['payment_method'];
	        $OrderDetails['total_amount'] = $currentPaymentDetails['total_amount'];
	        
	        //Assign order to printer
	        PrinterOrderAssignment($OrderDetails);
	        
    	    $ObjProcessStatusMaster->setSelect("order_process_status.process_status_id");
            $ObjProcessStatusMaster->setWhere("AND process_status_complete = :process_status_complete","1","string");
            $processData = $ObjProcessStatusMaster->getProcessStatus();
            foreach($processData as $value) {
                $completed_process_status_id[] =  $value->process_status_id;
            }
	        
            if(getAccessObj()->getUserType() == '1' && in_array($PaymentDetails[ELEMENT_ORDER_STATUS],$completed_process_status_id)){
	            cashback_calculation_for_customer($OrderDetails,$completed_process_status_id);
	        }
    		//Update Order Status
    		$OrderDataObj->orders_id 		= $order_id;
    		$OrderDataObj->orders_status_id	= $PaymentDetails[ELEMENT_ORDER_STATUS];
    		$OrderDataObj->order_last_modified 	= date('Y-m-d H:i:s');
    		$OrderMasterObj->editOrder($OrderDataObj);
    		
            // Allow invoice number generation based on orde status
            $allowInvoiceNumber = true;
            if($_SITE_VAR_INVOICE_NUMBER_BASED_ON_ORDER_STATUS == '1'){
                $allowInvoiceNumber = false;
                $ObjProcessStatusMaster->setWhere("AND process_status_id = :process_status_id", $PaymentDetails[ELEMENT_ORDER_STATUS], 'int');
                $statusData = $ObjProcessStatusMaster->getProcessStatus();
                if($statusData[0]['allow_invoice_download'] == '1'){
                    $allowInvoiceNumber = true;
                }
            }
            //Update Order invoice number
            if($allowInvoiceNumber == true){
                update_invoice_number($order_id);
            }
    		
    		//Add Order History Data
    		$OrderHistoryData->orders_status_id 	= $PaymentDetails[ELEMENT_ORDER_STATUS];
    		$OrderHistoryData->comments 			= '';	// @TODO - Add some comments
    		$OrderHistoryData->customer_notified 	= '0';
    		$OrderHistoryData->orders_id 			= $order_id;
    		$OrderHistoryData->date_added			= date('Y-m-d H:i:s');
    		$OrderHistoryData->createdby 			= "A";
    		$ObjOrderHistoryMaster->AddOrderHistory($OrderHistoryData);
    		
    		//Update Order Payment Details with transaction id and response
    		$PaymentDetailsDataObj->orders_id 		= $order_id;
    		$PaymentDetailsDataObj->remark		 	= $remark;
    		$PaymentDetailsDataObj->transactionid 	= $transaction_id;
    		$PaymentDetailsDataObj->payment_id 		= $currentPaymentDetails->payment_id;
    		$PaymentDetailsDataObj->payment_date 	= date('Y-m-d H:i:s');
    		$PaymentDetailsDataObj->payment_response_data = $response;
    		$PaymentDetailsDataObj->payment_status = (empty($PaymentDetails['select_payment_status']))?1:$PaymentDetails['select_payment_status']; // Set success payment status , 2 = Unpaid (Default Added)
    		$PaymentDetailsMasterObj->editPaymentDetails($PaymentDetailsDataObj);
    		
    		//Send email notification
    		if($sendmail === true) {
    			Order_Completion_Front($order_id);
    		}
	    }
		//Redirect to success page if redirection method
		if($ipn === false) {
			show_page_header(FILE_THANKS.'?add=success&OrderNo='.$order_id,true);
			exit;
		}
	} 
	//If response is not successful
	else {
		//Update Order Payment Details with response
		$PaymentDetailsDataObj->payment_id 				= $currentPaymentDetails->payment_id;
		$PaymentDetailsDataObj->payment_response_data 	= $response;
		$PaymentDetailsMasterObj->editPaymentDetails($PaymentDetailsDataObj);
		// Notify admin for unpaid order
        if(!empty($currentPaymentDetails)){
            Unpaid_Order_Notify_to_Admin($order_id);
        }
		paymentwriteLog($response,$PaymentDetails,"order_payment");
		//Redirect to success page with failed message
		if($ipn === false) {
			show_page_header(FILE_THANKS.'?cancleMsg=true&OrderNo='.$order_id,true);
			exit;
		}
	}
}

/**
 * Get Payment Method Settings by directory  name
 *
 * @param string $payment_directory_or_id Payment Method Directory Name or Method ID
 * @return Array All Settings
 */
function getPaymentMethodSettings($payment_directory_or_id) {
	require_once(DIR_WS_MODEL . "PaymentMethodMaster.php");
	$ObjPaymentMethodMaster = new PaymentMethodMaster();
	if(is_numeric($payment_directory_or_id)){
        $ObjPaymentMethodMaster->setWhere("AND payment_method_id = :payment_method_id ", $payment_directory_or_id, 'int');
	}
	else{
	    $ObjPaymentMethodMaster->setWhere("AND directory = :directory", $payment_directory_or_id, 'string');
	}
	$DataDomainPayment = $ObjPaymentMethodMaster->getPaymentMethod();
	if($DataDomainPayment)
		$PaymentMethodDetails = unserialize($DataDomainPayment[0]['payment_details']);
	else
		$PaymentMethodDetails = array();
	return $PaymentMethodDetails;
}

function process_payment_request($sucess=false,$fail=false,$request_id=null,$transaction_id=null,$payment_response=null,$redirect=true)
{
	require_once(DIR_WS_MODEL."PaymentRequestMaster.php");
	require_once(DIR_WS_MODEL."PaymentMethodMaster.php");
	require_once(DIR_WS_MODEL."BasketMaster.php");
	require_once(DIR_WS_MODEL."OrderMaster.php");	
	require_once(DIR_WS_MODEL."PaymentDetailsMaster.php");
	require_once(DIR_WS_MODEL . "UserMaster.php");
	require_once(DIR_WS_MODEL."orderHistoryMaster.php");
	require_once(DIR_WS_MODEL."ProcessStatusMaster.php");
	require_once(DIR_WS_LIB . 'cart_class.php');
	
	$ObjPaymentRequestData = new PaymentRequestData();
	$ObjPaymentRequest		= new PaymentRequestMaster();
	$ObjPaymentMethodMaster = new PaymentMethodMaster();
	$BasketMasterObj 	= new BasketMaster();
	$OrderDataObj	        = new OrderData();
	$OrderMasterObj         = new OrderMaster();
	$PaymentDetailsDataObj	= new PaymentDetailsData();
	$ObjPaymentDetailsMaster= new PaymentDetailsMaster();
	$UserMasterObj    = new UserMaster();
	$ObjOrderHistoryMaster	= new OrderHistoryMaster();
	$OrderHistoryData		= new OrderHistoryData();
	$ObjProcessStatusMaster = new ProcessStatusMaster();
	
	global $__Session;
	
	$request_session =$__Session->GetValue('_session_payment_request');
	$type=$request_session['type'];
	$payment_method_id=$request_session['payment_method_id'];
	
	$ObjPaymentRequest->setSelect(array("payment_request_details.*","orders.customers_email_address"));
	$ObjPaymentRequest->setWhere("AND  payment_request_id = :payment_request_id", $request_id, 'int');
 	$chk_order_request = $ObjPaymentRequest->getPaymentRequest();
 	$payment_processing_fees = number_format($chk_order_request[0]['payment_processing_fee'], COUNT_AFTER_DECIMAL, '.', '');
	$existing_response = $chk_order_request[0]->payment_response;
	$payment_request_type = $chk_order_request[0]->type;
	$order_id = $chk_order_request[0]->orders_id;
	$auto_credit = $chk_order_request[0]->auto_credit;
	$user_id = $chk_order_request[0]->user_id;
	$amount = $chk_order_request[0]->amount;
	$payment_method_id = $chk_order_request[0]->payment_method_id;
	$paymentreq_paid_status = $chk_order_request[0]->paid;
    $userID = getAccessObj()->getUserID();

	// Get Payment Method Details
	$ObjPaymentMethodMaster->setWhere("AND  payment_method_id = :payment_method_id", $payment_method_id, 'int');
	$DataDomainPayment = $ObjPaymentMethodMaster->getPaymentMethod();
	$PaymentDetails = unserialize($DataDomainPayment[0]['payment_details']);
	
	$additional_amount = 0;
	if($payment_request_type == '5'){
	    $payment_response = unserialize($payment_response);
	    $additional_amount = get_additional_payonbalance($amount);
	    $payment_response['additional_amount'] = $additional_amount;	    
	    $payment_response = serialize($payment_response);
	}
	
	if($sucess === true) // If response success
	{		
	    //If payment request  staus not set to 1 means still request is unpaid.
	    if($paymentreq_paid_status != '1'){
	    	if($payment_request_type == '5'){
	    		$ObjPaymentRequestData->amount = $amount + $additional_amount;
	    	}
    		$ObjPaymentRequestData->transaction_id = $transaction_id;
    	 	$ObjPaymentRequestData->payment_response = (!empty($existing_response)?$existing_response."|".$payment_response:$payment_response);
    		$ObjPaymentRequestData->paid_date =date("Y-m-d H:i:s");		
    		$ObjPaymentRequestData->paid =1;
    		$ObjPaymentRequestData->payment_request_id =$request_id;	
    		$ObjPaymentRequestData->payment_method_id =$payment_method_id;
    		$ObjPaymentRequestData->payment_processing_fee = $payment_processing_fees;
    		$ObjPaymentRequestData->updated_by ="U";
    		$ObjPaymentRequestData->modified_date =date("Y-m-d H:i:s");
    		$ObjPaymentRequest->editPaymentRequest($ObjPaymentRequestData, "sucess_response");
    		
    		if($payment_request_type =='1' || $payment_request_type =='2') //  1 = for Pending Order  2 = FOr Offline Order - > Need to update order status once payment comeplete sucessfully.
            {	
                $OrderMasterObj->setSelect(array('orders.orders_id','orders.user_id','order_production_days','order_shipping_days','orders.orders_status_id','orders.corporate_id','orders.customer_type','orders.user_type_id','orders.customers_name','orders.customers_email_address','payment_details.coupon_code','payment_details.coupon_amount'));
                $OrderMasterObj->setJoin("LEFT JOIN payment_details ON payment_details.orders_id=orders.orders_id");
    		    $OrderMasterObj->setWhere("AND orders.orders_id = :orders_orders_id", $order_id, 'int');
    		    $OrderDetails = $OrderMasterObj->getOrder();
    		    $OrderDetails = $OrderDetails[0];
                $OrderDetails['payment_method'] = $payment_method_id;
    			$OrderDetails['total_amount'] = $amount;
    			
    			$OrderDataObj->orders_id 		= $order_id;
    			$OrderDataObj->orders_status_id	= $PaymentDetails[ELEMENT_ORDER_STATUS];
                $OrderDataObj->order_last_modified = date("Y-m-d H:i:s");
    			
    			//Assign order to printer
    			PrinterOrderAssignment($OrderDetails);
    			
    			// Re-calculate order due date
    			$production_days = $OrderDetails['order_production_days'];
    			$shipping_days = $OrderDetails['order_shipping_days'];
    			if ($production_days != "" &&  $shipping_days != ""){
    			    $delivery_info = Shopping_Cart_Data::get_delivery_info($production_days, $shipping_days, false);
    			    $OrderDataObj->orders_due_date = $delivery_info['date'];
    			}
    			// Re-calculate payment due date
    			$is_offline_method = ($PaymentDetails['of_method'] == '0')?true:false;
    			$order_payment_due_date = ($is_offline_method)?(defined("SES_PAYMENT_DUE_DAYS") && SES_PAYMENT_DUE_DAYS != "") ? SES_PAYMENT_DUE_DAYS : $PaymentDetails['payment_due_days']:$PaymentDetails['payment_due_days'];
    			if(!empty($order_payment_due_date)) {
    			    $OrderDataObj->payment_due_date = date('Y-m-d', strtotime("+".$order_payment_due_date." days"));
    			}else {
    			    $OrderDataObj->payment_due_date = date('Y-m-d');
    			}
    			$OrderMasterObj->editOrder($OrderDataObj, "yes");
    			
                //To add history
                historyLogForOrder($OrderDetails, $OrderDataObj, null, null, '', null, '', 'A');
    			
    			$ObjPaymentDetailsMaster->setWhere("AND  orders_id = :Orders_Id ", $order_id, 'int');
    			$payment_details = $ObjPaymentDetailsMaster->getPaymentDetails();
    			$OrderPayment 	 = $payment_details[0];
    	
    			// UPdate Payment detail for pending & Offline orders 
    			$PaymentDetailsDataObj->orders_id 	= $order_id;
    			$PaymentDetailsDataObj->payment_method 	= $payment_method_id;
    			$PaymentDetailsDataObj->total_amount 	= $OrderPayment->total_amount - $OrderPayment->payment_processing_fees + $payment_processing_fees;
    			$PaymentDetailsDataObj->payment_processing_fees	= $payment_processing_fees;
    			$PaymentDetailsDataObj->payment_response_data = $payment_response;
    			$PaymentDetailsDataObj->payment_id 		= $OrderPayment->payment_id;
    			$PaymentDetailsDataObj->transactionid 		= $transaction_id;
    			$PaymentDetailsDataObj->payment_date=date('Y-m-d H:i:s');
    			$PaymentDetailsDataObj->payment_status = (empty($PaymentDetails['select_payment_status']))?1:$PaymentDetails['select_payment_status'];
    			$ObjPaymentDetailsMaster->editPaymentDetails($PaymentDetailsDataObj, false);
    			
    			if($payment_request_type == '1'){
    		      Order_Completion_Front($order_id);
    		    }
    		}    		
    		
            // To change order status and payment status of order that are selected by admin at the time when request is raised
            if($payment_request_type == '0') {
                changeOrderAndPaymentStatus($order_id, $chk_order_request[0], 'user');
            }
    		if($auto_credit !='' && $auto_credit == '1'  || $payment_request_type == '5'){
    			$fees = "";
				if(!empty($payment_processing_fees) && $payment_processing_fees != " "){
					$fees = '<br>'.PAYON_PROCESSING_FEES.' : '.(float) $payment_processing_fees;
				}
    				 
                if(getAccessObj()->isStore() && (defined('ENABLE_CORPORATE_PAYON_ACCOUNT_LIMIT') && ENABLE_CORPORATE_PAYON_ACCOUNT_LIMIT) && (defined('ENABLE_CORPORATE_PAYON') && ENABLE_CORPORATE_PAYON)) {
    				require_once(DIR_WS_MODEL . "CorporateUserMaster.php");
    				require_once(DIR_WS_MODEL . "CorporateAccountMaster.php");
    				require_once(DIR_WS_MODEL."CorporateDepartmentMaster.php");
    				$objCorporateDepartment 	= new CorporateDepartmentMaster();
    				$ObjCorporateAccountData	= new CorporateAccountData();
    				$ObjCorporateAccountMaster = new CorporateAccountMaster();
    				$ObjCorporateData = new CorporateUserData();
    				$ObjCorporateMaster = new CorporateUserMaster();
    		
    				$ses_corp_id = getAccessObj()->getStoreID();
                    $ObjCorporateMaster->setWhere(getAccessObj()->getStoreAccess('corporate_user', array('corporate_id' => $ses_corp_id)));
    				$get_corporate_detail = $ObjCorporateMaster->getCorporateUser();
    				$CorporateDetails = $get_corporate_detail[0];
    				
    				if(!empty($user_id)) {
    					$seldepartment_fieldarr =array("orders.orders_id,orders.user_id,orders.department_id");
    					$OrderMasterObj->setSelect($seldepartment_fieldarr)
    					->setWhere(" AND orders_id=:orders_id",$order_id,'int');
    					$userdept_details = $OrderMasterObj->getOrder();
    					$userdept_details = $userdept_details[0];
    					$sel_dept = $userdept_details['department_id'];
    				}
    		
    				$currentAmmount = $CorporateDetails->balance_amount;
    				if(empty($currentAmmount)) {
    					$currentAmmount = 0;
    				}
    		
    				$newAmmount = $currentAmmount + (float) $amount;
    				$credittype = 'C';
    				if($newAmmount >= 0 || $credittype == 'C') {
    					$changeStatus = array('balance_amount');
    					$ObjCorporateData->balance_amount = $newAmmount;
    					$ObjCorporateData->corporate_id = $ses_corp_id;
    					$editcurrentAmount = $ObjCorporateMaster->editCorporateUser($ObjCorporateData);
    						
    					if($editcurrentAmount == true) {
    						$ObjCorporateAccountData->corporate_id = $ses_corp_id;
    						$ObjCorporateAccountData->amount 	   = $amount;
    						$ObjCorporateAccountData->type 		   = $credittype;
    						$ObjCorporateAccountData->comments     = '<a href="order_action.php?Action=edit_order&OrderId='.$order_id.'" target="_blank">#'.$order_id.'</a>'.' - '.PAYMENT_REQUEST_AUTO_CREDIT_HELP;
    						$ObjCorporateAccountData->date_added   = date('Y-m-d H:i:s');
    						$ObjCorporateAccountData->department_id = $sel_dept;
    						$addaccountHistory 					   = $ObjCorporateAccountMaster->addCorporateAccount($ObjCorporateAccountData);
    					}
    				}
    			} else {
    				require_once(DIR_WS_MODEL."UserMaster.php");
    				require_once(DIR_WS_MODEL."UserAccountMaster.php");
    				$ObjUserAccountData	  = new UserAccountData();
    				$ObjUserAccountMaster = new UserAccountMaster();
    				$ObjUserMaster 		  = new UserMaster();
    				$ObjUserData 		  = new UserData();
    				$ses_corp_id = getAccessObj()->getStoreID();
    				if(!empty($user_id)) {
    					$ObjUserMaster->setWhere(" AND userid = :userid",$user_id,"int");
    					$user_details = $ObjUserMaster->getUser();
    				}
    				if(!empty($user_details))
    					$UserDetails = $user_details[0];
    		
    				$balance 		 = $UserDetails->balance_amount;
    				$remaining_limit = $UserDetails->pay_limit + $balance;
    		
    				$currentAmmount 		 = $balance;
    				if(empty($currentAmmount)) {
    					$currentAmmount = 0;
    				}
    
    				$newAmmount = $currentAmmount + (float) $amount  + (float) $additional_amount;
    				$credittype = 'C';
    				if($newAmmount >= 0 || $credittype == 'C') {
    					$changeStatus 				 = array('balance_amount');
    					$ObjUserData->balance_amount = $newAmmount;
    					$ObjUserData->userid 		 = $user_id;
    					$editcurrentAmount = $ObjUserMaster->editUser($ObjUserData, $changeStatus);
    		
    					if($editcurrentAmount == true) {
    						$ObjUserAccountData->user_id 	  = $user_id;
    						$ObjUserAccountData->corporate_id = $ses_corp_id;
    						$ObjUserAccountData->amount 	  = $amount + $additional_amount;
    						$ObjUserAccountData->type 		  = $credittype;
    						$ObjUserAccountData->is_useradmin = '0';
                            if($payment_request_type == '5') {
                                $ObjUserAccountData->comments = "";
                                $additionalamount = "";

                                $ObjUserAccountData->comments .= ADDED_BALANCE.' : '.(float) $amount;
                                if(!empty($additional_amount) && $additional_amount != 0){
                                     $ObjUserAccountData->comments .= '<br>'.ADDITIONAL_BALANCE.' : '.(float) $additional_amount;
                                }

                      			 if(!empty($payment_processing_fees) && $payment_processing_fees != " "){
									$ObjUserAccountData->comments .= $fees;
                                }
                               
                                if(!empty($chk_order_request[0]->customer_notes)){
                                $ObjUserAccountData->comments .= '</br>'.COMMON_SITEMASTER_COMMENT .' : '.$chk_order_request[0]->customer_notes;
                                }
                            }else{
								$ObjUserAccountData->comments = '#'."<b>".$order_id."</b>"."<b>".' - '.PAYMENT_REQUEST_AUTO_CREDIT_HELP.$fees."</b>"."<b>".$order_id."</b>";
                            }    						
    						$ObjUserAccountData->date_added   = date('Y-m-d H:i:s');
    						$addaccountHistory 	= $ObjUserAccountMaster->addUserAccount($ObjUserAccountData);
    					}
    				}
    			}
    		}	
    			
    		$status='success';
    		$arr_currency_data = array();
    		$arr_currency_data = create_currency_session(SITE_CURRENCY_ID, $arr_currency_data);
    		$paid_amount=currency_format($chk_order_request[0]->amount,$arr_currency_data['currency_decimalpoint'],$arr_currency_data['currency_thousandpoint'],null,$arr_currency_data['currency_leftsymbol'],$arr_currency_data['currency_rightsymbol'],$arr_currency_data['currency_value']);
    		$DirectPaymentMethodsArr = getpaymentMethod($payment_method_id, $enable_payon_account = false); // Get payment method name
    		//$customer_email =$chk_order_request[0]->customers_email_address;
    				
    		$UserMasterObj->setWhere("AND  userid = :userid", $chk_order_request[0]->user_id, 'int');
    		$Users            = $UserMasterObj->getUser();
    		$user_sess_details = $Users[0];
    		
    		$payment_details->name = $chk_order_request[0]->orders_id;
    		$payment_details->payment_method = $DirectPaymentMethodsArr['payment_title'];
    		$payment_details->payment_date = date('m/d/Y');
    		$payment_details->transaction_id = $transaction_id;
    		$payment_details->amount = $paid_amount;
    		$payment_details->payment_status = $status;
    		$payment_details->comments = $chk_order_request[0]->customer_notes;
    		
    	    if($type=='payon_balance'){
    	        $user_sess_details['additional_amount'] = $additional_amount;
				$user_sess_details['processing_fees'] = $payment_processing_fees;
                Send_Customer_PayonBalance_Request_Mail($user_sess_details,$request_id, $status); //Send Notification to customer
    		}else{
                Send_Customer_DirectPayment_Request_Mail($request_id, $status); //Send Notification to customer
    		}
	    	    
		    $BasketMasterObj->deleteBasket(null, $userID,$payment_request=true);
	    }
		if($type == 'shipment'){			
			require_once(DIR_WS_MODEL . "InventoryShipmentRequestMaster.php");
			$ObjInventoryShipRequestMaster				= new InventoryShipmentRequestMaster();
			$ObjInventoryShipRequestMaster->setselect('r.ship_request_id')->setWhere("AND  r.payment_request_id = :r_payment_request_id", $request_id, 'int');
			$shipReqId= $ObjInventoryShipRequestMaster->getInventoryShipmentRequest();
			Send_Admin_Shipment_Request_Mail($shipReqId[0]->ship_request_id); //Send Notification to admin
			$Redirect_totab ="&tab=inventory";
			
			$__Session->ClearValue("_session_shiprequest_details");
			$__Session->ClearValue("_session_inventory_shippingtype");
			$__Session->Store();
			
		}			
		if($redirect === true) {			
            if($type=='standalone'){
            	show_page_header(FILE_DIRECT_PAYMENT_SUCCESS);exit;
            }else if($type=='payon_balance'){
            	show_page_header(FILE_USER_ACCCOUNT_HISTORY."?trck=payreq&type=payon_balance&message=ONLINE_PAYMENT_MSG_SUCCESS");exit;
            } else {
                show_page_header(FILE_ORDERS_DETAILS."?trck=payreq&OrderId=".$order_id."$Redirect_totab&msg=ONLINE_PAYMENT_MSG_SUCCESS");exit;
            }
		}
		
	}
	else // If response faillure
	{		
		$ObjPaymentRequestData->payment_response = ($existing_response!='')?$existing_response."|".$payment_response:$payment_response;
		$ObjPaymentRequestData->payment_request_id = $request_id;
		$ObjPaymentRequestData->payment_processing_fee = $payment_processing_fees;
		$ObjPaymentRequestData->updated_by ="U";
		$ObjPaymentRequestData->modified_date =date("Y-m-d H:i:s");
		$ObjPaymentRequest->editPaymentRequest($ObjPaymentRequestData,"fail_response");
		
		$BasketMasterObj->deleteBasket(null, $userID,$payment_request=true); // empty basket row only for payment request cart
		
		paymentwriteLog($payment_response,$PaymentDetails,"direct_payment");
		
		if($redirect === true) {
			if($type=='standalone'){
				 show_page_header(FILE_DIRECT_PAYMENT."?trck=payreq&message=&getpaymethod=".$payment_method_id);exit;
		    }else if($type=='payon_balance'){
    			 show_page_header(FILE_USER_ACCCOUNT_HISTORY."?trck=payreq&message=&type=payon_balance&getpaymethod=".$payment_method_id);exit;
	        }else {
	    		 show_page_header(FILE_DIRECT_PAYMENT.'?trck=payreq&message=&RequestId='.$request_id.'&getpaymethod='.$payment_method_id);exit;
           }
	   }
	}
}


function encodeOrderNo($order_id,$length=5)
{
	$new_order_no=$order_id."_".generateRandomNo($length);
	return $new_order_no;
}
function decodeOrderNo($order_id)
{
	$order_id = explode("_",$order_id);
	return $order_id[0];
}

/**
 * Encrypt or decrypt query string parameters before passing through payment gateway redirection
 *
 * @param string $string String to encrypt or decrypt
 * @param string $type encode/decode
 * @return string Encoded/Decoded string
 */
function encode_decode_params($string,$type='encode') {
	if($type == 'encode') {
		return base64_encode($string);
	} elseif ($type == 'decode') {
		$decodedata = base64_decode($string);
		parse_str($decodedata, $output);
		return $output;
	} else {
		return $string;
	}
}

//function for write error logs when response is failuer
function paymentwriteLog($response,$PaymentDetails,$payment_type){
    if($PaymentDetails['allow_debug'] == 1){
        $current_date =date('Y-m-d H:i:s');
        $fp = opsFileOpen(DIR_WS_IMAGES . "logs/payment_error5546.log", 'a', false);
        $current ='';
        $current .= "\n\n==================".$PaymentDetails['payment_method_name']."[".$current_date."]===============================\n\n";
        $current .= "Payment Type: ".$payment_type."\n";
        $current .=print_r($response,true);
        $current .= "\n\n=========================================================================\n\n";
        // Write the contents back to the file
        opsFileWrite($fp, $current);
        opsFileClose($fp);
    }
}

//Refund order function
function process_payment_refund($OrderDetails,$refund_data) {
    $paymentdetails = getPaymentMethodSettings($OrderDetails['payment_method']);
    require_once(DIR_WS_PAYMENT.$paymentdetails['directory'].'/payment_refund.php');
    if(!empty($paymentdetails['directory'])) {
        $funct_name = preg_replace("/[^A-Za-z0-9_]/", "", $paymentdetails['directory']).'_process_refund';
        $refund_details = $funct_name($OrderDetails,$paymentdetails,$refund_data);
    }
    return $refund_details;
}
?>
