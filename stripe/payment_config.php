<?php 
define_const(STRIPE_LBL_SECRET_KEY, "Secret Key");
define_const(STRIPE_LBL_PUBLISHABLE_KEY, "Publishable Key");
define_const(STRIPE_LBL_ENABLE_CUSTOMER_MAPPING, "Enable Customer Mapping");
define_const(STRIPE_LBL_ENABLE_PAYNOW_BUTTON, "Enable Payment Request Button ");
$helpMsgStripe = array(
    'HELP_PAYMENT_STRIPE_SECRET_KEY' => 'Secret key(api key) associated with your stripe account.',
    'HELP_PAYMENT_STRIPE_PUBLISHABLE_KEY' => 'Publishable key(public key) associated with your stripe account.',
    'HELP_PAYMENT_STRIPE_ENABLE_CUSTOMER_MAPPING' => 'Enabling customer mapping will let you create customers on stripe.',
    'HELP_PAYMENT_STRIPE_ENABLE_PAY_NOW' => 'Enabling Payment Request Button. Click on below link for more clarification <br/> <a href="https://stripe.com/docs/stripe-js/elements/payment-request-button">Click Here</a>',
    );
?>
<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">   
	<tr>
		<td class="form_caption" align="left"><?php echo STRIPE_LBL_SECRET_KEY; ?> : </td>
		<td><input type="text" name="txt_secret_key" id="txt_secret_key" value="<?php echo $PaymentDetails['txt_secret_key'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $helpMsgStripe['HELP_PAYMENT_STRIPE_SECRET_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo STRIPE_LBL_PUBLISHABLE_KEY; ?> : </td>
		<td align="left"><input type="text" name="txt_publishable_key" id="txt_publishable_key" value="<?php echo $PaymentDetails['txt_publishable_key'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $helpMsgStripe['HELP_PAYMENT_STRIPE_PUBLISHABLE_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
    </tr>
	<tr>
		<td width="30%" align="left" class="form_caption"><label for="enable_customer_mapping"><?php echo STRIPE_LBL_ENABLE_CUSTOMER_MAPPING; ?> : </label></td>
		<td align="left">
            <input type="hidden" name="enable_customer_mapping" value="0"/>
			 <input class="radiochk" type="checkbox" value="1" name="enable_customer_mapping" <?php if($PaymentDetails['enable_customer_mapping'] == "1"){ echo 'checked';  } ?>>
		</td>
    </tr>
    <tr>
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td align="left" class="help_msg"><?php echo $helpMsgStripe['HELP_PAYMENT_STRIPE_ENABLE_CUSTOMER_MAPPING']; ?></td>
	</tr>
    <tr>
		<td width="30%" align="left" class="form_caption"><label for="enable_paynow_button"><?php echo STRIPE_LBL_ENABLE_PAYNOW_BUTTON; ?> : </label></td>
		<td align="left">
			<input type="hidden" name="enable_paynow_button" value="0"/>
			 <input class="radiochk" type="checkbox" value="1" name="enable_paynow_button" <?php if($PaymentDetails['enable_paynow_button'] == "1"){ echo 'checked';  } ?>>
		</td>
    </tr>
    <tr>
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td align="left" class="help_msg"><?php echo $helpMsgStripe['HELP_PAYMENT_STRIPE_ENABLE_PAY_NOW']; ?></td>
	</tr>
</table>
