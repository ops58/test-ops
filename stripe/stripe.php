<?php 
require_once("../../lib/common.php");
require_once(DIR_WS_PAYMENT."/payment.php");
require_once(DIR_WS_LIB.'cart_class.php');
require_once('stripe-php/init.php');

$objUtilMaster = new UtilMaster();
$objUtilMaster->setCustomQuery("SELECT shortname, site_language_id FROM site_language where site_language_id = '".SITE_LANGUAGE_ID."'");
$lang_infos = $objUtilMaster->exec_query();
$currentLangShortName = $lang_infos[0]['shortname'];
$userEmail = getAccessObj()->getUserEmail();
$userID = getAccessObj()->getUserID();
global $__Session;
if($__Session->HasValue('_session_payment_request')) {
    $checkout_details = new Shopping_Cart_Data(true,false,true,true,false,true);
    $custom_value = "direct_payment";
    $description = "Payment Request# ".$RRequest->request('order_id');
} else {
    $checkout_details        = new Shopping_Cart_Data(true,false,true,true,false);
    $custom_value = "order";
    $lineItemsDesc = array();
    $lineItemsDesc[] = "Order# ".$RRequest->request('order_id');
    foreach($checkout_details->cart_details AS $item) {
        $lineItemsDesc[] = $item['product_title'] . ' : ' . $item['quantity']. ' ' . COMMON_QUANTITY;
    }
    $description = implode("\n", $lineItemsDesc);
}

$order_id = $RRequest->request('order_id');
$payment_method_id = $RRequest->request('payment_method_id');
$stripe_payment_intent = $RRequest->post('stripe_payment_intent');
$PaymentDetails = getPaymentMethodSettings($payment_method_id);
$billing_addr = $checkout_details->billing_address;
$allowCustomer = $PaymentDetails['enable_customer_mapping'];
$enablePaynow = $PaymentDetails['enable_paynow_button'];

\Stripe\Stripe::setApiKey($PaymentDetails['txt_secret_key']);

if(!empty($stripe_payment_intent)) {
    $order_charges = array();
    $charges = \Stripe\Charge::all([
        'payment_intent' => $stripe_payment_intent,
        // Limit the number of objects to return (the default is 10)
        //'limit' => 3,
    ]);
    $charges_data = $charges->data;
    $charges_arr = objectToArray($charges_data);
    $charges_payment_intents = array_flip(array_column($charges_arr, 'payment_intent'));
    $charges_data_index = $charges_payment_intents[$stripe_payment_intent];
    $order_charges = $charges_data[$charges_data_index];
    $stripe_details = serialize($order_charges);
    
    $transaction_id = $order_charges->id;
    
    if($__Session->HasValue('_session_payment_request')) { //Partial payment request response handling
        if($order_charges->status == 'succeeded') {
            process_payment_request($sucess=true,false,$order_id,$transaction_id,$stripe_details);
        } else {
            process_payment_request(false,$fail=true,$order_id,'',$stripe_details);
        }
    } else {
        if($order_charges->status == 'succeeded') {
            $remark = 'Success';
            updatepaymentdetails($order_id,$success=true,$transaction_id,$stripe_details,$PaymentDetails,$remark);
        } else {
            updatepaymentdetails($order_id,false,null,$stripe_details,$PaymentDetails,null);
        }
    }
    
}
$price = number_format($checkout_details->payment_method_amount, 2, '.', '');
$formatted_price = currency_format($price);

$arr_admin_cur_data = array();
$arr_admin_cur_data = create_currency_session('', $arr_currency_data);
$sel_curr_decimalpoint = $arr_admin_cur_data['currency_decimalpoint'];
$sel_curr_value = $arr_admin_cur_data['currency_value'];
$sel_curr_code = $arr_admin_cur_data['currency_code'];

//https://stripe.com/docs/currencies#zero-decimal
//For zero-decimal currencies, still provide amounts as an integer but without multiplying by 100. For example, to charge ¥500, provide an amount value of 500.
if($sel_curr_code == 'JPY'){
    $amount = ceil($price); // This payment doesn't allowed to pass float amount.
}
else{
    //FOR USD needs to pass value in cents.   
    $amount = $price *100;
}
//register customer to Stripe
if($allowCustomer) {
    $customers = \Stripe\Customer::all([
        'email' => $userEmail
        ]);
    $custData = $customers->data;
    $custId = '';
    if(!empty($custData)) {
        $count = count($custData);
        if($count > 1) {
            foreach($custData as $data) {
                if($data['description'] == $userID) {
                    $custId = $data->id;
                }
            }
        }
        if(empty($custId)) {
            $custId = $custData['0']->id;
        }        
    } else {
        $addCustomer = \Stripe\Customer::Create([
            'description' => $userID,
            'email' => $userEmail
            ]);
        $cust = $addCustomer->data;
        $customerData  = $addCustomer;
        if(!empty($cust)) {
            $custId = $cust['0']->id;
        }
        if(!empty($customerData) && isset($customerData->id)){
            $custId = $customerData->id;
        }
    }
}

try {
    $intent = \Stripe\PaymentIntent::create([
        'amount' => $amount,
        'currency' => $sel_curr_code,
        'description' => substr($description,0,1000), //max 1000 characters allowed by stripe
        'receipt_email' => $userEmail,
        'customer' => $custId //refrence of stripe customer id
    ]);
} catch (Exception $e) {
  
    $stripe_details = serialize(array('error_message'=>$e->getMessage()));
    if($__Session->HasValue('_session_payment_request')) { //Partial payment request response handling       
        process_payment_request(false,$fail=true,$order_id,'',$stripe_details);
    } else {
        updatepaymentdetails($order_id,false,null,$stripe_details,$PaymentDetails,null);
    }
   
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></title>
    	<link href="<?php echo DIR_HTTP_THIRDPARTY."bootstrap/css/bootstrapv4.css"; ?>" rel="stylesheet" type="text/css"></link>
    	<link href="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_CSS;?>theme.css" rel="stylesheet" type="text/css" media="screen" />
    	<script src="<?php echo DIR_HTTP_THIRDPARTY."js/jquery.min.js" ?>"></script>
    	<script src="<?php echo DIR_HTTP_THIRDPARTY."bootstrap/js/bootstrapv4.js"; ?>"></script>
	<link href="<?php echo DIR_HTTP_TEMPLATES_CSS."style.css";?>" rel="stylesheet" type="text/css" media="screen" />
    	<?php   
            if(is_file(DIR_WS_IMAGES.'custom_css/'.SITE_TEMPLATE.'.css')){ ?>
				<link href="<?php echo DIR_HTTP_IMAGES.'custom_css/'.SITE_TEMPLATE.'.css' ; ?>" rel="stylesheet" type="text/css" media="screen" />
		<?php } ?>
    	<script src="https://js.stripe.com/v3/"></script>
        <style type="text/css" >
            #contentContainer {
            	background:none;
            }
            .div_img_loader {
            	/*color:#df6c1b;*/
            	float:left;
            	font-size:72px;
            	text-align:left;
            	width:25%;
            	margin:-12px 0px 0px 0px;
            }
            .btn_pay_style{
            	/*background:#df6c1b;*/
            	color:#FFFFFF;
            	font-size:16px;
            	height:48px;
            	padding:3px 5px 3px 5px;
            }
            .wait_img_loading {
            	height:300px;
            }
            .wait_img_loading .img_load {
            	float:none;
            	padding:72px 0 10px 0;
            	text-align:center;
            	line-height:28px;
            	font-size:15px;	
            	/*font-weight:bold;*/
            }
            .wait_img_loading .img_load .load_message {
            	text-align:right;
            	line-height:28px;
            	font-size:34px;
            	font-weight:bold;
            	/*color:#df6c1b;*/
            	width:66%;
            	text-align:center;
            	float:left;
            	padding-left:90px;
            }
            .wait_img_loading .img_load .click_message {
            	text-align:center;
            	line-height:28px;
            	font-size:16px;
            }
            .StripeElement {
              box-sizing: border-box;
            
              height: 40px;
              width:100%;
              padding: 10px 12px;
            
              border: 1px solid transparent;
              border-radius: 4px;
              background-color: white;
            
              box-shadow: 0 1px 3px 0 #e6ebf1;
              -webkit-transition: box-shadow 150ms ease;
              transition: box-shadow 150ms ease;
            }
            
            .StripeElement--focus {
              box-shadow: 0 1px 3px 0 #cfd7df;
            }
            
            .StripeElement--invalid {
              border-color: #fa755a;
            }
            
            .StripeElement--webkit-autofill {
              background-color: #fefde5 !important;
            }
            #stripeDiv .page-header {border : none !important; background :none !important;}
            .stripe_form .logo img{
                max-width:300px; height:auto;            
            }
            .loading-container {
                width: 100%;
                height: 100%;
                position: absolute;
                left: 0;
                right: 0;
                top: 0;
                bottom: 0;
                margin: auto;
                background:rgba(0,0,0,0.7);
                z-index: 99;
                display: flex;

            }
            .loading-container .loading {
                height: 100px;
                width: 100px;
                border-width: 2px;
                margin: auto;
            }
            .loading-container #loading-text {
                margin-top: 0;
            }
            #loading-text {
                opacity: 0;
                position: absolute;
                left: 0;
                right: 0;
                margin: auto;
                top: 0;
                bottom: 0;
                display: flex;
                align-items: center;
                justify-content: center;
                margin-top: 0;
            }
            .loading-container{border-radius:0;}
        </style>
    </head>
    <body>
    <div class="text-center loader_picasa" id="loader_user"><div class="loading-container text-center"><div class="loading"></div><div id="loading-text">loading</div></div></div>       
        <form name="payment" method="post" action="">
            <div id="container" class="container mt-4 stripe_form">
            	<div id="header">
            		<div id="headerleft">
            		<!-- Header Left -->
            			<div class="logo">
            				<span><a href="<?php echo show_page_link(FILE_INDEX);?>"><img src="<? echo SITE_LOGO_PATH; ?>" /></a></span>
            			</div>
            		</div>
            	</div>
            	<!-- main contian End -->
            	<div id="contentContainer" class="wait_img_loading">
            		<div class="row justify-content-center" id="stripeDiv">            		
            			<div class="col-12 col-md-8">
            				<div class="row	pt-5">
            					<div class="col-12 col-md-6 page-header">
            						<h1><?php echo ORDER.' : #'.$order_id; ?></h1>
            						<h5 class="text-dark"><?php echo COMMON_AMOUNT.' : '.$formatted_price; ?></h5>
            					</div>
            					<?php if(!empty($billing_addr)) { ?>
            					<div class="col-12 col-md-6 d-flex justify-content-md-end">
            					<div class="card border-0 w-60" id="Billing_Details">
            						<div class="card-header bg-transparent border-bottom px-0 py-2">
            							<h4 class="card-title m-0 "><?php echo BILLING_ADDRESS; ?></h4>
        							</div>
        							<div class="card-body px-0">
        								<?php
                    					$mandatory_settings = user_field_settings();
                    					echo getAddressDetailsTable($billing_addr,null,null,null,$mandatory_settings);
                    					?>
        							</div>
        							</div>
            					</div>
            					<?php } ?>
            				</div>
            			</div>            			
        				<div class="col-12 col-md-8">
        					<?php echo $PaymentDetails['payment_note'][SITE_LANGUAGE_ID]; ?> 
        				</div>
        				<div class="col-12 col-md-8 pb-4">
                            <div class="row">
                                <?php
                                if($enablePaynow == 1){?>
                                    <div class="col-12">
                                        <div id="payment-request-button" class="mb-4 p-0  justify-content-center" style = "width:50%;">
                                                <!-- A Stripe Element will be inserted here. -->
                                        </div>
                                    </div><?php
                                 }?>
                                <div class="col-12">
                                    <div id = "paynowHelp" class = "mb-3">
                                        <?php if(defined('STRIPE_COMMON_CARD_MESSAGE')){
                                            echo STRIPE_COMMON_CARD_MESSAGE;
                                        } else { ?>
                                         <b> Or Please enter the credit card information & click the button to make the payment</b>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="card ">
                                        <div class="card-body">
                                            <h5 class="card-title">PAYMENT INFORMATION</h5>
                                            <div class="col-12">	
                                                <div class="form-row mb-3">
                                                    <label for="card-element"></label>
                                                    <div id="card-element">
                                                        <!-- a Stripe Element will be inserted here. -->
                                                    </div>
                                                        <!-- Used to display form errors -->
                                                    <div id="card-errors" role="alert" class="text-danger"></div>
                                                </div>
                                                <input type="hidden" name="order_id" id="order_id" value="<?php echo $order_id; ?>" />
                                                <input type="hidden" name="payment_method_id" id="payment_method_id" value="<?php echo $payment_method_id; ?>" />
                                                <input type="hidden" name="stripe_payment_intent" id="stripe_payment_intent" value="<?php echo $intent->id; ?>"/>
                                            </div>
                                            <input type="submit" class="btn btn-primary" name="btnpaysubmit" title="<?php echo LEBEL_CLICK_HERE_TO_PAY; ?>" value="<?php echo LEBEL_CLICK_HERE_TO_PAY;?>" >
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
            	</div>
        	</div>
        </form>
    </body>
</html>

<script>
var publishableKey = '<?php echo $PaymentDetails['txt_publishable_key']; ?>';
var payment_intent = '<?php echo $intent->client_secret; ?>';
var currency = '<?php echo strtolower($sel_curr_code); ?>';
var amount = <?php echo $amount; ?>;
var stripe = Stripe(publishableKey);
var elements = stripe.elements({'locale':'<?php echo $currentLangShortName; ?>'}); // to load the form language specific
var name =  '<?php echo $billing_addr['firstname'].' '.$billing_addr['lastname']; ?>';
var address_line1 = '<?php echo $billing_addr['street_address']; ?>';
var address_line2 = '<?php echo $billing_addr['suburb']; ?>';
var address_city = '<?php echo $billing_addr['city']; ?>';
var address_state = '<?php echo $billing_addr['state']; ?>';
var address_zip = '<?php echo $billing_addr['postcode']; ?>';
var address_country = '<?php echo $billing_addr['countrycode2']; ?>';
var enablePaynow = '<?php echo $enablePaynow; ?>';

$('#loader_user').hide();
// Create an instance of the card UI component
var cardElement = card = elements.create('card', {
'hidePostalCode' : true,
  'style': {
    'base': {
        'fontFamily': 'Arial, sans-serif',
        'fontSize': '18px',
        //'color': '#000000',
    },
    'invalid': {
        'color': 'red',
    },
  }
});

// Add an instance of the card UI component into the `card-element` <div>
card.mount('#card-element');

//Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
	paymentform.find('.btn-success').prop('disabled', false);
    if (event.error) {
        $("#card-errors").html(event.error.message);
        paymentform.find('.btn-success').prop('disabled', true);
    } else {
    	$("#card-errors").html('');
    }
});

var paymentform = $("#stripeDiv").closest('form');

$('input[name="btnpaysubmit"]').on("click", function(e){
    $('#loader_user').show();
	e.preventDefault();
	$('input[name="btnpaysubmit"]').prop('disabled', true);
	stripe.handleCardPayment(
	    payment_intent, cardElement, {
             payment_method_data : {
                 billing_details: {
                     name : name,
                     address : {
                         line1 : address_line1,
                         line2 : address_line2,
                         city : address_city,
                         state : address_state,
                         postal_code : address_zip,
                         country : address_country
                     }
                 }
             }
        }
    ).then(function(result) {
        if (result.error) {
            // Display error.message in your UI.
            $('#loader_user').hide();
    		$("#card-errors").html(result.error.message);
            $('input[name="btnpaysubmit"]').prop('disabled', false); // Re-enable submission
        } else {
            $('#loader_user').hide();
            // The payment has succeeded. Display a success message.
            paymentform.get(0).submit();
        }
        return false;
    });
});

if(enablePaynow) {
    var paymentRequest = stripe.paymentRequest({
        country: address_country,
        currency: currency,
        total: {
            label: 'label',
            amount: amount,
        },
        requestPayerName: true,
        requestPayerEmail: true,
    });

    var elements = stripe.elements();
    var prButton = elements.create('paymentRequestButton', {
        paymentRequest: paymentRequest,
    });

    // Check the availability of the Payment Request API first.
    paymentRequest.canMakePayment().then(function(result) {
        if (result) {
            prButton.mount('#payment-request-button');
            document.getElementById('paynowHelp').style.display = 'block';
            document.getElementById('or').style.display = 'block';
        } else {
            document.getElementById('payment-request-button').style.display = 'none';
            document.getElementById('paynowHelp').style.display = 'none';
            document.getElementById('or').style.display = 'none';
        }
    });

    paymentRequest.on('paymentmethod', function(ev) {
        // Confirm the PaymentIntent without handling potential next actions (yet).
        stripe.confirmCardPayment(
            payment_intent,
            {payment_method: ev.paymentMethod.id},
            {handleActions: false}
        ).then(function(confirmResult) {
            if (confirmResult.error) {
            ev.complete('fail');
            } else {
            ev.complete('success');
            if (confirmResult.paymentIntent.status === "requires_action") {
                stripe.confirmCardPayment(clientSecret).then(function(result) {
                if (result.error) {
                } else {
                }
                });
            } else {
                var paymentform = $("#stripeDiv").closest('form');
                paymentform.get(0).submit();
                // The payment has succeeded.
            }
            }
        });
    });
}
    
</script>
