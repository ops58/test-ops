<?php
	require_once("../../lib/common.php");
	require_once(DIR_WS_PAYMENT."/payment.php");
	require_once(DIR_WS_LIB.'cart_class.php');
	require_once('Veritrans.php');
	
	$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));
	if ($PaymentDetails['sel_payment_env'] == 1) {
		Veritrans_Config::$isProduction = true;
	} else {
	    Veritrans_Config::$isProduction = false;
	}
	
	if($__Session->HasValue('_session_payment_request')) {
		$checkout_details = new Shopping_Cart_Data(true,true,true,true,false,true);
		$custom_value = "direct_payment";
		$payment_type = 'PR_';
	} else {
		$checkout_details = new Shopping_Cart_Data(true,true,true,true,false);
		$custom_value = "order";
		$payment_type = 'OR_';
	}
	$shippingInfo = $checkout_details->shipping_address;
	$billingInfo = $checkout_details->billing_address;
	$userInfo = $checkout_details->customer_address;
	$price = ceil($checkout_details->payment_method_amount);
	$orderid = $RRequest->request('order_id');

	//Set Your server key
	Veritrans_Config::$serverKey = $PaymentDetails['txt_serverkey'];
	
	// To enable sanitization
	Veritrans_Config::$isSanitized = true;
	// To enable 3D-Secure
	Veritrans_Config::$is3ds = true;
	// Required
	$transaction_details = array(
	    'order_id' => $payment_type.$orderid.'_'.date('YmdHis'),
	    'gross_amount' => $price,//145000, // no decimal allowed for creditcard
	);
	// Optional
	$billing_address = array(
	    'first_name'    => $billingInfo['firstname'],
	    'last_name'     => $billingInfo['lastname'],
	    'address'       => $billingInfo['street_address'],
	    'city'          => $billingInfo['city'],
	    'postal_code'   => $billingInfo['postcode'],
	    'phone'         => $billingInfo['phone'],
	    'country_code'  => $billingInfo['countrycode3']
	);
	
	// Optional
	$shipping_address = array(
	    'first_name'    => $shippingInfo['firstname'],
	    'last_name'     => $shippingInfo['lastname'],
	    'address'       => $shippingInfo['street_address'],
	    'city'          => $shippingInfo['city'],
	    'postal_code'   => $shippingInfo['postcode'],
	    'phone'         => $shippingInfo['phone'],
	    'country_code'  => $shippingInfo['countrycode3']
	);
	
	$customer_details = array(
	    'first_name'    => SES_USER_FIRSTNAME, //optional
	    'last_name'     => SES_USER_LASTNAME, //optional
	    'email'         => getAccessObj()->getUserEmail(), //mandatory
	    'phone'         => $userInfo['phone'], //mandatory
	    'billing_address'  => $billing_address, //optional
	    'shipping_address' => $shipping_address //optional
	);
	
	/* $item_details = array(
	    'id' 		=> '1',
	    'price' 	=> $price,
	    'quantity' 	=> '1',
	    'name' 		=> 'Order Product'
	); */
	
	// Fill SNAP API parameter
	$params = array(
	    'transaction_details' => $transaction_details,
	    'customer_details' => $customer_details,
	   // 'item_details' => $item_details,
	    'callbacks' => array(
	        'finish' => show_page_link('payment/veritrans_v2/response.php'),
	        'custom_field1' => $custom_value
        )
	);
	
	// Get Snap Payment Page URL
	$post_url = '';
	$transaction_res = Veritrans_Snap::createTransaction($params);
	if (isset($transaction_res->redirect_url)) {
	    $post_url = $transaction_res->redirect_url;
	} else {
	    echo $transaction_res->error_messages[0];
	    exit;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></title>
	<link href="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_CSS;?>theme.css" rel="stylesheet" type="text/css" media="screen" />
<style type="text/css" >
#contentContainer {
	background:none;
}
.div_img_loader {
	/*color:#df6c1b;*/
	float:left;
	font-size:72px;
	text-align:left;
	width:25%;
	margin:-12px 0px 0px 0px;
}
.btn_pay_style{
	/*background:#df6c1b;*/
	color:#FFFFFF;
	font-size:16px;
	height:48px;
	padding:3px 5px 3px 5px;
}
.wait_img_loading {
	height:300px;
}
.wait_img_loading .img_load {
	float:none;
	padding:72px 0 10px 0;
	text-align:center;
	line-height:28px;
	font-size:15px;	
	/*font-weight:bold;*/
}
.wait_img_loading .img_load .load_message {
	text-align:right;
	line-height:28px;
	font-size:34px;
	font-weight:bold;
	/*color:#df6c1b;*/
	width:66%;
	text-align:center;
	float:left;
	padding-left:90px;
}
.wait_img_loading .img_load .click_message {
	text-align:center;
	line-height:28px;
	font-size:16px;
}
</style>
</head>
<body>
<form action="<?php echo $post_url; ?>" name="payment">
<div id="container">
	<div id="header">
		<div id="headerleft">
		<!-- Header Left -->
			<div class="logo">
				<span><a href="<?php echo show_page_link(FILE_INDEX);?>"><img src="<? echo SITE_LOGO_PATH; ?>" /></a></span>
			</div>
		</div>
	</div>
	<!-- main contian End -->
	<div id="contentContainer" class="wait_img_loading">
		<div class="img_load">
			<div class="page-section-header load_message"><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></div><div id="div_repeat_info" class="page-section-header div_img_loader">.</div>
			<div style="float:left;padding:20px 0 7px 7px;text-align:left;width:100%;text-align:center;"><?php echo LABEL_PAGE_LOAD_PROBLEM ;?></div>
			<div style="float:left;padding:8px 0 7px 7px;width:100%;"><?php if(file_exists(DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES.'img_click_to_pay.gif')) { ?>
				<label class="btn btn-primary btn_left_co"><input type="submit" id="btnpaysubmit" class="btn btn-primary btn_pay_style" name="btnpaysubmit" src="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES; ?>img_click_to_pay.gif" title="Click Here To Pay" value="<?php echo LEBEL_CLICK_HERE_TO_PAY;?>" alt="Click Here To Pay"></label>
			<?php } else { ?>
				<label class="btn btn-primary btn_left_co"><input type="submit" id="btnpaysubmit" class="btn btn-primary btn_pay_style" name="btnpaysubmit" title="Click Here To Pay" value="<?php echo LEBEL_CLICK_HERE_TO_PAY;?>" ></label>
			<?php } ?>
			</div>
		</div>
		
	</div>
</div>
</form>
<!-- Main End -->
<script>
document.payment.submit();
</script>
</body>
</html>