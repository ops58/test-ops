<?php
/**
 * This file is check the paypal response.
 *
 * @author     Radixweb <team.radixweb@gmail.com>
 * @copyright  Copyright (c) 2008, Radixweb
 * @version    1.0
 * @since      1.0
 */
require_once("../../lib/common.php");
require_once(DIR_WS_PAYMENT."payment.php");
require_once('Veritrans.php');

$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));

if ($PaymentDetails['sel_payment_env'] == 1) {
    Veritrans_Config::$isProduction = true;
} else {
    Veritrans_Config::$isProduction = false;
}
Veritrans_Config::$serverKey = $PaymentDetails['txt_serverkey'];
$notify = new Veritrans_Notification();

$file = fopen('err.txt', 'a');
$current_date = date('Y-m-d H:i:s');
$content = '';
$content .= '\n\n ==========================='.$current_date.'================================= \n\n';
$content .= 'Notify : ';
$content .= print_r($notify,true);
$content .= print_r($notify->transaction_id,true);
$content .= ': Request : ';
$content .= print_r($_REQUEST,true);
$content .= '\n\n ========================================================================= \n\n';

fwrite($file, $content);
fclose($file);

$transaction = $notify->transaction_status;
$type = $notify->payment_type;
$order_id = $notify->order_id;
$fraud = $notify->fraud_status;
$veritrans_details 	=   serialize($_REQUEST);
$status_code = $notify->status_code;
$transaction_id = $notify->transaction_id;
$custom_field1 = $notify->custom_field1;
$order_no = (isset($notify->order_id))?$notify->order_id:$RRequest->request('order_id');
$order_ref = explode('_', $order_no);
$payment_type = $order_ref[0];
$order_id = $order_ref[1];

if($payment_type == 'PR' || $custom_field1 == 'direct_payment') { //Partial payment request response handling
    if(in_array($status_code,array('200'))) {
        process_payment_request($sucess=true,false,$order_id,$transaction_id,$veritrans_details, false);
    } else {
        process_payment_request(false,$fail=true,$order_id,'',$veritrans_details, false);
    }
} else {
    //sometime transaction status get settletment =200 in payment notification but in redirection it's having 201 .
    //Actually order has been succesffuly placed with transaction id so in this case we needs to forcefully make it success.
    // This issue for http://givist.co.id
    /* if($status_code == '201'){
        $status_code =  '200';
    } */
    
    if(in_array($status_code,array('200'))) {
        $remark = 'Success';
        updatepaymentdetails($order_id,$success=true,$transaction_id,$veritrans_details,$PaymentDetails,$remark, true);
    } else {
        updatepaymentdetails($order_id,false,null,$veritrans_details,$PaymentDetails,null, true);
    }
}