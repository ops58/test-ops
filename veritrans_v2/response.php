<?php
require_once("../../lib/common.php");
require_once(DIR_WS_PAYMENT."payment.php");
require_once('Veritrans.php');

$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));

if ($PaymentDetails['sel_payment_env'] == 1) {
    Veritrans_Config::$isProduction = true;
} else {
    Veritrans_Config::$isProduction = false;
}
Veritrans_Config::$serverKey = $PaymentDetails['txt_serverkey'];

$order_status_obj = Veritrans_Transaction::status($RRequest->request('order_id'));
$veritrans_details 	=   serialize($_REQUEST);
$status_code = $order_status_obj->status_code;
$transaction_id = $order_status_obj->transaction_id;
$custom_field1 = $order_status_obj->custom_field1;
$order_no = (isset($order_status_obj->order_id))?$order_status_obj->order_id:$RRequest->request('order_id');
$order_ref = explode('_', $order_no);
$payment_type = $order_ref[0];
$order_id = $order_ref[1];

if($payment_type == 'PR' || $custom_field1 == 'direct_payment') { //Partial payment request response handling
    if(in_array($status_code,array('200'))) {
        process_payment_request($sucess=true,false,$order_id,$transaction_id,$veritrans_details);
    } else {
        process_payment_request(false,$fail=true,$order_id,'',$veritrans_details);
    }
} else {
    //sometime transaction status get settletment =200 in payment notification but in redirection it's having 201 .
    //Actually order has been succesffuly placed with transaction id so in this case we needs to forcefully make it success.
    // This issue for http://givist.co.id
    /* if($status_code == '201'){
        $status_code =  '200';
    } */
    
    if(in_array($status_code,array('200'))) {
        $remark = 'Success';
        updatepaymentdetails($order_id,$success=true,$transaction_id,$veritrans_details,$PaymentDetails,$remark);
    } else {
        updatepaymentdetails($order_id,false,null,$veritrans_details,$PaymentDetails,null);
    }
}