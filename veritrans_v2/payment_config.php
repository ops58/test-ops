<?php
define_const('VERITRANS_LBL_SERVER_KEY', 'Server Key');
define_const('VERITRANS_LBL_PAYMENT_OPTIONS', 'Available Payment Options');

/*Help messages */
$help_messages1= array (
    'HELP_PAYMENT_VERITRANS_MERCHANT_ID' => 'Set Merchant ID that you created on veritrans sandbox account.',
    'HELP_PAYMENT_VERITRANS_SERVER_KEY' => 'Set Server Key of that Account.',
    'HELP_PAYMENT_VERITRANS_CLIENT_KEY' => 'Set Client Key of that Account.',
    'HELP_PAYMENT_VERITRANS_ENVIRONMENT' => 'Live - Live Payment Environment is for Real Time transaction. <br />Testing - Testing Environment is used for testing the entire Payment Process.',
    'HELP_PAYMENT_VERITRANS_AVAILABLE_OPTIONS' => 'Please select payment options through which customer can able to pay'
    
);
$help_messages =array_merge($help_messages,$help_messages1);
?>
<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">   
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_ENVIRONMENT; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['sel_payment_env'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['sel_payment_env'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   ?>
		   <span><input type="radio" id="sel_payment_env" name="sel_payment_env" value="1" <?php echo $lchecked;?>><?php echo PAYMENT_LBL_LIVE_ENV; ?></span>
		   <span class="radio_distance"><input type="radio" id="sel_payment_env" name="sel_payment_env" value="0" <?php echo $tchecked;?>><?php echo PAYMENT_LBL_TESTING_ENV; ?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_VERITRANS_ENVIRONMENT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption" width="46%"><?php echo VERITRANS_LBL_SERVER_KEY; ?> : </td>
		<td align="left"><input type="text" name="txt_serverkey" id="txt_serverkey" value="<?php echo $PaymentDetails['txt_serverkey'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_VERITRANS_SERVER_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption" width="36%"><?php echo VERITRANS_LBL_PAYMENT_OPTIONS; ?> : </td>
		<td align="left">
			<select name="txt_payment_options[]" multiple>
				<option value="credit_card" <?php echo in_array("credit_card", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Credit Card</option>
				<option value="mandiri_clickpay" <?php echo in_array("mandiri_clickpay", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Mandiri Clickpay</option>
				<option value="cimb_clicks" <?php echo in_array("cimb_clicks", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Cimb Clicks</option>
				<option value="bca_klikbca" <?php echo in_array("bca_klikbca", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Bca Klikbcaoption</option>
				<option value="bca_klikpay" <?php echo in_array("bca_klikpay", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Bca Klikpay</option>
				<option value="bri_epay" <?php echo in_array("bri_epay", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Bri Epay</option>
				<option value="telkomsel_cash" <?php echo in_array("telkomsel_cash", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Telkomsel Cash</option>
				<option value="echannel" <?php echo in_array("echannel", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Echannel</option>
				<option value="mandiri_ecash" <?php echo in_array("mandiri_ecash", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Mandiri Ecash</option>
				<option value="permata_va" <?php echo in_array("permata_va", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Permata Va</option>				
				<option value="other_va" <?php echo in_array("other_va", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Other Va</option>
				<option value="gopay" <?php echo in_array("gopay", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >GOPay</option>
				<option value="bca_va" <?php echo in_array("bca_va", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Bca va</option>				
				<option value="bni_va" <?php echo in_array("bni_va", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Bni Va</option>
				<option value="kioson" <?php echo in_array("kioson", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Kioson</option>
				<option value="indomaret" <?php echo in_array("indomaret", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Indomaret</option>				
				<option value="gci" <?php echo in_array("gci", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Gci</option>				
				<option value="danamon_online" <?php echo in_array("danamon_online", $PaymentDetails['txt_payment_options'])?"selected = 'selected' ":"" ; ?> >Danamon Online</option>				
			</select>		
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_VERITRANS_AVAILABLE_OPTIONS'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
</table>