<?php
/**
 * This file is check the paypal response.
 *
 * @author     Radixweb <team.radixweb@gmail.com>
 * @copyright  Copyright (c) 2008, Radixweb
 * @version    1.0
 * @since      1.0
 */
require_once("../../lib/common.php");
require_once(DIR_WS_PAYMENT."payment.php");
require(DIR_WS_PAYMENT . "paypal_standard/ipn_cls.php");
require_once(DIR_WS_MODEL."PaymentDetailsMaster.php");
require_once(DIR_WS_MODEL."PaymentRequestMaster.php");

$error_email = "";
$from_error_email = "";

$ObjPaymentDetailsMaster = new PaymentDetailsMaster();
$ObjPaymentRequest		= new PaymentRequestMaster();

$order_number=$_REQUEST['OrderNo'];
if(isset($_POST['item_number']) && !empty($_POST['item_number'])) {
	$order_number=$_POST['item_number'];
}
if(isset($_REQUEST['cm']) && $_REQUEST['cm'] != '')
    $payment_type = $_REQUEST['cm'];
else if(isset($_REQUEST['custom']) && $_REQUEST['custom'] != '')
    $payment_type = $_REQUEST['custom'];
    
$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));

$payment_status_result = false;
if($payment_type == 'direct_payment') //Partial payment request response handling
{
	if(!isset($_REQUEST['pact_res']) || empty($_REQUEST['pact_res'])) {
            // IPN Notification handle
            $payment_status_result = false;
            $respon_data = $_POST;
            //use PaypalIPN;
            $ipn = new PaypalIPN();
            // Use the sandbox endpoint during testing.
            if ($PaymentDetails['sel_payment_env'] != 1) {
                $ipn->useSandbox();
            }
            $verified = $ipn->verifyIPN();
            if ($verified) {
                $payment_status_result = true;
                $transaction_id = $respon_data['txn_id'];
		        $order_number = $respon_data["item_number"];
            }

            $ObjPaymentRequest->setSelect(array("payment_request_details.*"));
            $ObjPaymentRequest->setWhere("AND  payment_request_id = :payment_request_id", $order_number, 'int');
            $chk_order_request = $ObjPaymentRequest->getPaymentRequest();
            $totalAmount = $chk_order_request[0]->amount + $chk_order_request[0]->payment_processing_fee;
            $OrderAmount = number_format($totalAmount, 2, '.', '');
            //if payment is success & order amount mathced with the actual order amunt then only switch order paid.
            $payment_status_result = (($OrderAmount == $respon_data['mc_gross'] || $OrderAmount == $respon_data['mc_gross'] - $respon_data['tax']) && $payment_status_result) ? true : false ;

            $ipn = false;

            /* $current_date =date('Y-m-d H:i:s');
            $file = dirname(__FILE__).'/error.txt';
            $fp = fopen($file, 'a');
            $current ='';
            $current .= "\n\n==================".$PaymentDetails['payment_title']."[".$current_date."]===".SITE_URL."==========IPN RESPONSE=========\n\n";
            $current .= print_r($respon_data,true);
            $current .= "Verified : ".print_r($verified,true);
            $payload = file_get_contents("php://input");
            $current .= "\n";
            $current .= "Payload : ".print_r($payload,true);$current .= "\n";
            $current .= "order Amount : ".print_r($OrderAmount,true);$current .= "\n";
            $current .= "Res Amount : ".print_r($respon_data['mc_gross'],true);
            $current .= "final res  : ".print_r($payment_status_result,true);$current .= "\n";
            $current .= "\n\n=========================================================================\n\n";
            // Write the contents back to the file
            fwrite($fp, $current);
            fclose($fp); */

	} else {
            // Paypal PDT Method Code
            if(isset($_REQUEST['add']) && $_REQUEST['add']=="success") {
                    $payment_status_result = true;
            }

            // In some cases wen IPN is on at that time we get "payment_status" in redirection instead of "st", so handled that case
            if($_REQUEST["payment_status"]) {
                    $payment_status_details = $_REQUEST["payment_status"];
            } else {
                    $payment_status_details = $_REQUEST["st"];
            }

            // In some cases wen IPN is on at that time we get "txn_id" in redirection instead of "tx", so handled that case
            if($_REQUEST["txn_id"]) {
                    $transaction_id = $_REQUEST["txn_id"];
            } else {
                    $transaction_id = $_REQUEST["tx"];
            }
            $transactionAmt = $_REQUEST["amt"];

            if(!empty($payment_status_details)) {
                $ObjPaymentRequest->setSelect(array("payment_request_details.*"));
                $ObjPaymentRequest->setWhere("AND  payment_request_id = :payment_request_id", $order_number, 'int');
                $chk_order_request = $ObjPaymentRequest->getPaymentRequest();
                $totalAmount = $chk_order_request[0]->amount + $chk_order_request[0]->payment_processing_fee;
                $OrderAmount = number_format($totalAmount, 2, '.', '');
                //if payment is success & order amount mathced with the actual order amunt then only switch order paid.
                $payment_status_result = ($OrderAmount == $transactionAmt && strtolower($payment_status_details) == 'completed' && $payment_status_result) ? true : false ;
            }
            $ipn = true;

            /* $current_date =date('Y-m-d H:i:s');
            $file = dirname(__FILE__).'/error.txt';
            $fp = fopen($file, 'a');
            $current ='';
            $current .= "\n\n==================".$PaymentDetails['payment_title']."[".$current_date."]===".SITE_URL."=========================\n\n";
            $current .= print_r($_REQUEST,true);
            $payload = file_get_contents("php://input");
            $current .= "\n";
            $current .= "Payload : ".print_r($payload,true);$current .= "\n";
            $current .= "order Amount : ".print_r($OrderAmount,true);$current .= "\n";
            $current .= "Res Amount : ".print_r($transactionAmt,true);
            $current .= "final res  : ".print_r($payment_status_result,true);$current .= "\n";
            $current .= "\n\n=========================================================================\n\n";
        // Write the contents back to the file
        fwrite($fp, $current);
        fclose($fp); */
	}
	
	if($payment_status_result===true) 
		process_payment_request($sucess=true,false,$order_number,$transaction_id,serialize($_REQUEST),$ipn);
	else
		process_payment_request(false,$fail=true,$order_number,'',serialize($_REQUEST),$ipn);
}
else { 
    
    $payment_status_result = false;
    if(!isset($_REQUEST['pact_res']) || empty($_REQUEST['pact_res'])) {
    	// IPN Notification handle
        $payment_status_result = false;
        $respon_data = $_POST;
        //use PaypalIPN;
        $ipn = new PaypalIPN();
        // Use the sandbox endpoint during testing.
        if ($PaymentDetails['sel_payment_env'] != 1) {
            $ipn->useSandbox();
        }
        $verified = $ipn->verifyIPN();
        if ($verified) {
            $payment_status_result = true;
            $transaction_id = $respon_data['txn_id'];
            $order_number = $respon_data["item_number"];
        }
        $ObjPaymentDetailsMaster->setSelect("orders.orders_status_id");
        $ObjPaymentDetailsMaster->setSelect("payment_details.*");
        $ObjPaymentDetailsMaster->setJoin('LEFT JOIN orders ON orders.orders_id=payment_details.orders_id');
        $ObjPaymentDetailsMaster->setWhere("AND payment_details.Orders_Id = :Orders_Id", $order_number, 'int');
        $payment_details = $ObjPaymentDetailsMaster->getPaymentDetails();
        $OrderPayment = $payment_details[0];
        $OrderAmount = number_format($OrderPayment->total_amount, 2, '.', '');
        //if payment is success & order amount mathced with the actual order amunt then only switch order paid.
        $payment_status_result = (($OrderAmount == $respon_data['mc_gross'] || $OrderAmount == $respon_data['mc_gross'] - $respon_data['tax']) && $payment_status_result) ? true : false ;
        $ipn = true;
        
        /* $current_date =date('Y-m-d H:i:s');
        $file = dirname(__FILE__).'/error.txt';
        $fp = fopen($file, 'a');
        $current ='';
        $current .= "\n\n==================".$PaymentDetails['payment_title']."[".$current_date."]===".SITE_URL."==========IPN RESPONSE=========\n\n";
        $current .= print_r($respon_data,true);
        $current .= "Verified : ".print_r($verified,true);
        $payload = file_get_contents("php://input");
        $current .= "\n";
        $current .= "Payload : ".print_r($payload,true);$current .= "\n";
        $current .= "order Amount : ".print_r($OrderAmount,true);$current .= "\n";
        $current .= "Res Amount : ".print_r($respon_data['mc_gross'],true);
        $current .= "final res  : ".print_r($payment_status_result,true);$current .= "\n";
        $current .= "\n\n=========================================================================\n\n";
        // Write the contents back to the file
        fwrite($fp, $current);
        fclose($fp); */
    	
    } else {
    	// Paypal PDT Method Code
    	if(isset($_REQUEST['add']) && $_REQUEST['add']=="success") {
    		$payment_status_result = true;
    	}
    	
    	$order_number=$_REQUEST['OrderNo'];
    	// In some cases wen IPN is on at that time we get "payment_status" in redirection instead of "st", so handled that case
    	if($_REQUEST["payment_status"]) {
    		$payment_status_details = $_REQUEST["payment_status"];
    	} else {
    		$payment_status_details = $_REQUEST["st"];
    	}    	
        // In some cases wen IPN is on at that time we get "txn_id" in redirection instead of "tx", so handled that case
        if(!empty($payment_status_details)) {
            if($_REQUEST["txn_id"]) {
                $transaction_id = $_REQUEST["txn_id"];
            } else {
                $transaction_id = $_REQUEST["tx"];
            }
            $transactionAmt = $_REQUEST["amt"];

            $ObjPaymentDetailsMaster->setSelect("orders.orders_status_id");
            $ObjPaymentDetailsMaster->setSelect("payment_details.*");
            $ObjPaymentDetailsMaster->setJoin('LEFT JOIN orders ON orders.orders_id=payment_details.orders_id');
            $ObjPaymentDetailsMaster->setWhere("AND payment_details.Orders_Id = :Orders_Id", $order_number, 'int');
            $payment_details = $ObjPaymentDetailsMaster->getPaymentDetails();
            $OrderPayment = $payment_details[0];
            $OrderAmount = number_format($OrderPayment->total_amount, 2, '.', '');
            //if payment is success & order amount mathced with the actual order amunt then only switch order paid.
            $payment_status_result = ($OrderAmount == $transactionAmt && strtolower($payment_status_details) == 'completed' && $payment_status_result) ? true : false ;
        }
        $ipn = false;
        
        /* $current_date =date('Y-m-d H:i:s');
        $file = dirname(__FILE__).'/error.txt';
        $fp = fopen($file, 'a');
        $current ='';
        $current .= "\n\n==================".$PaymentDetails['payment_title']."[".$current_date."]===========".SITE_URL."===================\n\n";
        $current .= print_r($_REQUEST,true);
        $payload = file_get_contents("php://input");
        $current .= "\n";
        $current .= "Payload : ".print_r($payload,true);$current .= "\n";
        $current .= "order Amount : ".print_r($OrderAmount,true);$current .= "\n";
        $current .= "Res Amount : ".print_r($transactionAmt,true);
        $current .= "final res  : ".print_r($payment_status_result,true);$current .= "\n";
        $current .= "\n\n=========================================================================\n\n";
        // Write the contents back to the file
        fwrite($fp, $current);
        fclose($fp); */
    }
    
    $paypal_details = serialize($_REQUEST);
    
    //if($paymentDetailsTrans == "") {
    	
    	if($payment_status_result===true) {
    		updatepaymentdetails($order_number,$success=true,$transaction_id,$paypal_details,$PaymentDetails,$payment_status_details,$ipn);
    		
    		if($insertid!='') {
    			$paypal_ipn->error_out("This was a successful transaction", $em_headers);
    		}
    	} else {
    		updatepaymentdetails($order_number,false,null,$paypal_details,$PaymentDetails,null,$ipn);
    	}
    //}
}

if(empty($_REQUEST['pact_res'])) {
    // Reply with an empty 200 response to indicate to paypal the IPN was received correctly
    header("HTTP/1.1 200 OK");
} 