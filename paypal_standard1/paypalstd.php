<?php
	require_once("../../lib/common.php");
	require_once(DIR_WS_PAYMENT."/payment.php");
	require_once(DIR_WS_LIB.'cart_class.php');
	
    if($__Session->HasValue('_session_payment_request')) {
		$checkout_details = new Shopping_Cart_Data(true,false,false,true,false,true);
		$custom_value = "direct_payment";
	} else {
		$checkout_details        = new Shopping_Cart_Data(true,true,true,true,false);
		$custom_value = "order";
	}
	$PaymentDetails = getPaymentMethodSettings($checkout_details->PaymentMethod);
	
	if ($PaymentDetails['sel_payment_env'] == 1) {
	    $post_url   ="https://www.paypal.com/cgi-bin/webscr";//for live
	} else {
	    $post_url   ="https://www.sandbox.paypal.com/cgi-bin/webscr";//for testing
	}
	
	dopayment($checkout_details);
	$amt = $checkout_details->payment_method_amount;
	/* if(number_format($amt, 2, '.', '') != number_format($_REQUEST['total'], 2, '.', '')) {
		show_page_header(FILE_THANKS.'?cancleMsg=true&OrderNo='.$_REQUEST['order_id'],true);
		exit;
	} */
	$orderid		= $_REQUEST['order_id'];
	$price			= number_format($amt, 2, '.', '');

	$paypal_email 	= $PaymentDetails['txt_business_email']; //user email address
	$item_name		= $PaymentDetails['txt_item_name'];
	$currency_code	= $PaymentDetails['txt_currency_code'];
	/*$currency_code 	= $_REQUEST['ccode'];*/
	$ipn_url        = show_page_link("payment/paypal_standard/response.php",true);
    $return_url     = show_page_link("payment/paypal_standard/response.php?pact_res=return&add=success&OrderNo=".$orderid."&custom=".$custom_value,true);
    $return_url = update_query_string( $return_url );
    $cancel_url     = show_page_link("payment/paypal_standard/response.php?pact_res=cancel&add=invalid&OrderNo=".$orderid."&custom=".$custom_value,true);
    $cancel_url = update_query_string( $cancel_url );
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></title>
	<link href="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_CSS;?>theme.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript">
var loopTime=setInterval("currentTime()",500);
</script>
<style type="text/css" >
#contentContainer {
	background:none;
}
.div_img_loader {
	/*color:#df6c1b;*/
	float:left;
	font-size:72px;
	text-align:left;
	width:25%;
	margin:-12px 0px 0px 0px;
}
.btn_pay_style{
	/*background:#df6c1b;*/
	color:#FFFFFF;
	font-size:16px;
	height:48px;
	padding:3px 5px 3px 5px;
}
.wait_img_loading {
	height:300px;
}
.wait_img_loading .img_load {
	float:none;
	padding:72px 0 10px 0;
	text-align:center;
	line-height:28px;
	font-size:15px;	
	/*font-weight:bold;*/
}
.wait_img_loading .img_load .load_message {
	text-align:right;
	line-height:28px;
	font-size:34px;
	font-weight:bold;
	/*color:#df6c1b;*/
	width:66%;
	text-align:center;
	float:left;
	padding-left:90px;
}
.wait_img_loading .img_load .click_message {
	text-align:center;
	line-height:28px;
	font-size:16px;
}
</style>
<script type="text/javascript">
<!-- Begin
/* This script and many more are available free online at
The JavaScript Source!! http://javascript.internet.com
Created by: Abraham Joffe :: http://www.abrahamjoffe.com.au/ */

var startTime=new Date();
var strDisplay = '.';
var repeat = 5;
var start = 0;
function currentTime(){
  var a=Math.floor((new Date()-startTime)/100)/10;
  if(start != repeat ) {
  	document.getElementById("div_repeat_info").innerHTML = document.getElementById("div_repeat_info").innerHTML + strDisplay;
  	start = start + 1;
  } else {
  	document.getElementById("div_repeat_info").innerHTML = strDisplay;
  	start = 0;
  }
  /*if (a%1==0) a+=".0";
  document.getElementById("div_img_loader").innerHTML=a;*/
}

window.onload=function(){
  clearTimeout(loopTime);
  strDisplay = document.getElementById("div_repeat_info").innerHTML
}

// End -->
</script>
</head>
<body>
<form action="<?php echo $post_url; ?>" method="post" name="payment">
<div id="container">
	
	
	<div id="header">
		<div id="headerleft">
		<!-- Header Left -->
			<div class="logo">
				<!--<span><a href="<?php echo show_page_link(FILE_INDEX);?>"><img src="<? echo DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES; ?>logo.gif" /></a></span>-->
				<span><a href="<?php echo show_page_link(FILE_INDEX);?>"><img src="<? echo SITE_LOGO_PATH; ?>" /></a></span>
			</div>
		</div>
	</div>
	<!-- main contian End -->
	<div id="contentContainer" class="wait_img_loading">
		<div class="img_load">
			<div class="page-section-header load_message"><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></div><div id="div_repeat_info" class="page-section-header div_img_loader">.</div>
			<div style="float:left;padding:20px 0 7px 7px;text-align:left;width:100%;text-align:center;"><?php echo LABEL_PAGE_LOAD_PROBLEM ;?></div>
            <div style="float:left;padding:8px 0 7px 7px;width:100%;"><?php if(opsIsFile(DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES . 'img_click_to_pay.gif', false)) { ?>
				<label class="btn btn-primary btn_left_co"><input type="submit" class="btn btn-primary btn_pay_style" name="btnpaysubmit" src="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES; ?>img_click_to_pay.gif" title="Click Here To Pay" value="<?php echo LEBEL_CLICK_HERE_TO_PAY;?>" alt="Click Here To Pay"></label>
			<?php } else { ?>
				<label class="btn btn-primary btn_left_co"><input type="submit" class="btn btn-primary btn_pay_style" name="btnpaysubmit" title="Click Here To Pay" value="<?php echo LEBEL_CLICK_HERE_TO_PAY;?>" ></label>
			<?php } ?></div>
		</div>
		
			<input type="hidden" name="cmd" value="_xclick">
			<input type="hidden" name="business" value="<?php echo $paypal_email; ?>">
			<input type="hidden" name="item_name" value="<?php echo $item_name; ?>">
			<input type="hidden" name="item_number" value="<?php echo $orderid;?>">
			<input type="hidden" name="amount" value="<?php echo $price;?>">
			<input type="hidden" name="notify_url" value="<?php echo $ipn_url; ?>">
			<input type="hidden" name="return"     value="<?php echo $return_url; ?>">
			<input type="hidden" name="cancel_return" value="<?php echo $cancel_url; ?>">
			<input type="hidden" name="currency_code" value="<?php echo $currency_code; ?>">
			<input type="hidden" name="custom" value="<?php echo $custom_value;?>">		
	</div>
	</div>
</div>
</form>
<!-- Main End -->
</body>
</html>
<script language="javascript">
document.payment.submit();
</script>