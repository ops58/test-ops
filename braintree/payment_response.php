<?php
require_once(__DIR__ ."/vendor/autoload.php");
if ($PaymentDetails['sel_payment_env'] == 1) {
    $payment_enviroment = 'production';
} else {
    $payment_enviroment = 'sandbox';
}
//echo '<pre>';print_r($checkout_details);echo '</pre>';exit;
$barintree_data['AMT'] 			    = number_format($checkout_details->payment_method_amount,2,'.','');
$barintree_data['CREDITCARDTYPE']	= $checkout_details->ship_pay_details['braintree_creditCardType'];
$barintree_data['ACCT'] 			= $checkout_details->ship_pay_details['braintree_creditCardNumber'];

$month = $checkout_details->ship_pay_details['braintree_expDateMonth'];
$year_tn = $checkout_details->ship_pay_details['braintree_expDateYear'];
$month_tn = str_pad($month, 2, '0', STR_PAD_LEFT);

$barintree_data['EXPDATE'] 		= $month_tn.'/'.$year_tn;
$barintree_data['CVV2'] 			= $checkout_details->ship_pay_details['braintree_cvv2Number'];
$barintree_data['FIRSTNAME'] 		= $checkout_details->ship_pay_details['braintree_firstName'];
$barintree_data['LASTNAME'] 		= $checkout_details->ship_pay_details['pp_pro_lastName'];
//echo '<pre>';print_r($barintree_data);echo '</pre>';exit;
Braintree\Configuration::environment($payment_enviroment); // Change to production
Braintree\Configuration::merchantId($PaymentDetails['merchantId']);
Braintree\Configuration::publicKey($PaymentDetails['publicKey']);
Braintree\Configuration::privateKey($PaymentDetails['privateKey']);

$sale_result = Braintree\Transaction::sale(array(
    'amount' => $barintree_data['AMT'],
    'creditCard' => array(
        'cardholderName' => $barintree_data['FIRSTNAME'].' '.$barintree_data['LASTNAME'],
        'number' => $barintree_data['ACCT'],
        'expirationDate' => $barintree_data['EXPDATE'],
        'cvv' => $barintree_data['CVV2']
    ),
    'customer' => array(
        'firstName' =>  $checkout_details->customer_address['firstname'],
        'lastName' => $checkout_details->customer_address['lastname'],
        'company' => $checkout_details->customer_address['companyname'],
        'email' => getAccessObj()->getUserEmail(),
        'phone' => $checkout_details->customer_address['phone'],
        'website' => SITE_URL
    ),
    'billing' => array(
        'firstName' => $checkout_details->billing_address["firstname"],
        'lastName' => $checkout_details->billing_address["lastname"],
        'company' => $checkout_details->billing_address["companyname"],
        'streetAddress' => $checkout_details->billing_address["street_address"],
        'extendedAddress' => $checkout_details->billing_address["suburb"],
        'locality' => $checkout_details->billing_address["city"],
        'postalCode' => $checkout_details->billing_address["postcode"],
        'countryName' => $checkout_details->billing_address["countryname"],
        'countryCodeAlpha2' => $checkout_details->billing_address["countrycode2"],
        'countryCodeAlpha3' => $checkout_details->billing_address["countrycode3"],
    ),
    'shipping' => array(
        'firstName' => $checkout_details->shipping_address["firstname"],
        'lastName' => $checkout_details->shipping_address["lastname"],
        'company' => $checkout_details->shipping_address["companyname"],
        'streetAddress' => $checkout_details->shipping_address["street_address"],
        'extendedAddress' => $checkout_details->shipping_address["suburb"],
        'locality' => $checkout_details->shipping_address["city"],
        'postalCode' => $checkout_details->shipping_address["postcode"],
        'countryName' => $checkout_details->shipping_address["countryname"],
        'countryCodeAlpha2' => $checkout_details->shipping_address["countrycode2"],
        'countryCodeAlpha3' => $checkout_details->shipping_address["countrycode3"],
    ),
    'options' => array(
        'submitForSettlement' => true,
        'storeInVaultOnSuccess' => true
    )
));

// Return Array which is required into order confirmation page.
$response_details = array();

// 0=>failure, 1=>success, 2=>cancel by user(This will be used to redirect to third party payment website i.e. Paypal Standard Method, CCAvenue )
$status = 0;
if($sale_result->success == '1') {
    $status = 1;
} else {
    $response_details['payment_ack_failure_string'] = 'message=Invalid Credit Card Details';
}

$response_details['payment_ack_status'] = $status;

// Acknowledge message like Success, Failure, Successs with warning etc..... received from payment gateway
$response_details['ack_status_message'] = $resArray['ACK'];

// Order Progress status id, set from admin side. if this is blank, it will take the default order status
$response_details["order_status"]	= $PaymentDetails[ELEMENT_ORDER_STATUS];

// Transaction Id return by gateway
$response_details['transaction_id'] = $sale_result->transaction->id;

// Selected currency code (3 digit) from admin side
$response_details['currency_code'] = $sale_result->transaction->currencyIsoCode;

// Comment sent from payment gateways
$response_details["payment_comment"] = '';

// Serialized data. if other details need to add into database, received from payment gateway
$response_details["payment_response_details"] = serialize($sale_result);

// 1 => send Confirmation mail, 0 => do not send Confirmation mail
$response_details["mail_status"] = 1;

// This will be used to redirect to third party payment website i.e. Paypal Standard Method, CCAvenue 
$response_details["redirect_url"] = '';