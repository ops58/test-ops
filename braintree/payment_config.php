<?php
define_const('BRAIN_TREE_MERCHANT_ID','Merchant Key');
define_const('BRAIN_TREE_PUBLIC_KEY','Public Key');
define_const('BRAIN_TREE_PRIVATE_KEY','Private Key');
$help_messages1 = array('HELP_BRAIN_TREE_MERCHANT_ID'=>'Enter the Merchant id available in braintree account',
    'HELP_BRAIN_TREE_PUBLIC_KEY'=>'Enter the Public Key available in braintree account',
    'HELP_BRAIN_TREE_PRIVATE_KEY'=>'Enter the Private Key available in braintree account',
);
$help_messages = array_merge($help_messages,$help_messages1);
?>
<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">   
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_ENVIRONMENT; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['sel_payment_env'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['sel_payment_env'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   
		   ?>
		   <span><input type="radio" id="sel_payment_env" name="sel_payment_env" value="1" <?php echo $lchecked;?>><?php echo PAYMENT_LBL_LIVE_ENV; ?></span>
		   <span class="radio_distance"><input type="radio" id="sel_payment_env" name="sel_payment_env" value="0" <?php echo $tchecked;?>><?php echo PAYMENT_LBL_TESTING_ENV; ?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_PRO_ENVIRONMENT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>		
	<tr>
		<td class="form_caption" align="left"><?php echo BRAIN_TREE_MERCHANT_ID; ?> : </td>
		<td><input type="text" name="merchantId" id="merchantId" value="<?php echo $PaymentDetails['merchantId'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_BRAIN_TREE_MERCHANT_ID'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo BRAIN_TREE_PUBLIC_KEY; ?> : </td>
		<td align="left"><input type="text" name="publicKey" id="publicKey" value="<?php echo $PaymentDetails['publicKey'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_BRAIN_TREE_PUBLIC_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo BRAIN_TREE_PRIVATE_KEY; ?> : </td>
		<td align="left"><input type="text" name="privateKey" id="privateKey" value="<?php echo $PaymentDetails['privateKey'];?>" class="text_box_std" /></td>
	</tr>	
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_BRAIN_TREE_PRIVATE_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
</table>
