<div class="row">
	<?php 
		$sitepaymentid=$data['sitepaymentid'];
		if($_GET['status']=='failure' && $data['error_status_message']==true) { 
			$status_message = POS_ERROR_MESSAGE;
			if(!empty($_REQUEST['message'])) {
				$status_message = $_REQUEST['message'];
			}
	?>
	<div class="text-danger text-center col-12"><?php echo $status_message; ?></div>
	<?php } ?>
	<div class="col-12">	
		<div class="text-danger text-right mb-3 mr-3"><small><?php echo COMMON_REQUIRED_INFOMATION; ?></small></div>	
		<div class="form-row mb-3">						
			<label for="transaction_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo COMMON_TRANSACTION_ID;?></label>
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<input type="text" name="p_transactionid" id="transaction_<?php echo $sitepaymentid; ?>" value="" maxlength="100" class=" form-control" placeholder="" data-rule-required="true" data-msg-required="<?php echo POS_ERROR_MESSAGE; ?>" onkeypress="return spacecValidation(event);">        	        	
					<span class="text-danger" id="TransactionError_<?php echo $sitepaymentid; ?>"></span>
					<span class="input-group-require text-danger pl-1"> * </span>
				</div>
			</div>
		</div>
	</div>
</div>