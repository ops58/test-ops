<?php 
function paypal_standard_process_refund($OrderDetails,$paymentdetails,$refund_data) {
    $refund_detail = array();
    if ($paymentdetails['sel_payment_env'] == 1) {
        $post_url   ="https://api.paypal.com/v1/oauth2/token";//for live
    } else {
        $post_url   ="https://api.sandbox.paypal.com/v1/oauth2/token";//for testing
    }
    
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $post_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "grant_type=client_credentials",
        CURLOPT_USERPWD => $paymentdetails['txt_client_id'].":".$paymentdetails['txt_client_secret'],
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
        ),
    ));
    
    $response = curl_exec($curl);
    $err = curl_error($curl);
    
    curl_close($curl);
    if(!empty($response)){
        $response_data = json_decode($response,true);
        if(array_key_exists('access_token',$response_data)){
            $token = $response_data['access_token'];
            
            if ($paymentdetails['sel_payment_env'] == 1) {
                $post_url   ="https://api-3t.paypal.com/nvp";//for live
            } else {
                $post_url   ="https://api-3t.sandbox.paypal.com/nvp";//for testing
            }
            $postfields = array(
                'USER' => $paymentdetails['txt_username'],
                'PWD' => $paymentdetails['txt_password'],
                'SIGNATURE' => $paymentdetails['txt_signature'],
                'METHOD' => "RefundTransaction",
                'VERSION' => "94",
                'TRANSACTIONID' => $OrderDetails['transactionid'],
                'REFUNDTYPE' =>(empty($refund_data['orders_products_id'])?'Full':'Partial'),
                
            );
            $amount = array();
            if(!empty($refund_data['orders_products_id'])){
                $amount = array(
                'AMT' => $refund_data['refund_amount']);
            }
            if(!empty($amount))
                $postfields11 = array_merge($postfields,$amount);
            else
                $postfields11 = $postfields;
            
            $postfields = http_build_query($postfields11);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $post_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $postfields,
                CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer $token",
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded",
                    "postman-token: ded75c19-012f-bae9-cf9a-5bed01941410"
                ),
            ));
            
            $refund_response = curl_exec($curl);
            $refund_err = curl_error($curl);
            if(!empty($refund_response)){
                parse_str($refund_response, $refund_response1);
                if(array_key_exists('REFUNDTRANSACTIONID',$refund_response1) && !empty($refund_response1['REFUNDTRANSACTIONID'])){
                    $refund_response1['payment_method'] = $paymentdetails['payment_method_name'];
                    $refund_detail['status'] = 'success';
                    $refund_detail['refund_response'] = json_encode($refund_response1);
                    $refund_detail['error'] = '';
                }else{
                    $refund_detail['orders_id'] = $OrderDetails['orders_id'];
                    if(!empty($refund_data['orders_products_id']))
                        $refund_detail['orders_products_id'] = $refund_data['orders_products_id'];
                    $refund_detail['refund_amount'] = $refund_data['refund_amount'];
                    $refund_detail['status'] = 'failure';
                    $refund_detail['error'] = $refund_response1['L_LONGMESSAGE0'];
                }
            }
        }else {
            $refund_detail['orders_id'] = $OrderDetails['orders_id'];
            if(!empty($refund_data['orders_products_id']))
                $refund_detail['orders_products_id'] = $refund_data['orders_products_id'];
            $refund_detail['refund_amount'] = $refund_data['refund_amount'];
            $error = $response_data['error_description'];
            $refund_detail['status'] = 'failure';
            $refund_detail['error'] = $error;
        }
    } else {
        $refund_detail['orders_id'] = $OrderDetails['orders_id'];
        if(!empty($refund_data['orders_products_id']))
            $refund_detail['orders_products_id'] = $refund_data['orders_products_id'];
        $refund_detail['refund_amount'] = $refund_data['refund_amount'];
        $refund_detail['status'] = 'failure';
        $refund_detail['error'] = $err;
    }
    
    if(!empty($refund_detail['error'])){
        paymentwriteLog($refund_detail,$paymentdetails,"refund_order_payment");
    }
    return $refund_detail;
}
?>