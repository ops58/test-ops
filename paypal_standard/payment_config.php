<?php 

define_const('PAYPAL_STD_LBL_CLIENT_ID','Client Id');
define_const('PAYPAL_STD_LBL_CLIENT_SECRET','Client Secret');
define_const('PAYPAL_REFUND_ACCOUNT_DETAIL','Refund Account Detail');

$help_messages1 = array (
    'HELP_PAYPAL_STD_CLIENT_ID' => 'Please provide Client Id which was provided by Paypal Standard Account.',
    'HELP_PAYPAL_STD_CLIENT_SECRET' => 'Please provide Client Secret which was provided by Paypal Standard Account.',
    );
$help_messages = array_merge($help_messages,$help_messages1);
?>
<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">   
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_ENVIRONMENT; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['sel_payment_env'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['sel_payment_env'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   
		   ?>
		   <span><input type="radio" id="sel_payment_env" name="sel_payment_env" value="1" <?php echo $lchecked;?>><?php echo PAYMENT_LBL_LIVE_ENV; ?></span>
		   <span class="radio_distance"><input type="radio" id="sel_payment_env" name="sel_payment_env" value="0" <?php echo $tchecked;?>><?php echo PAYMENT_LBL_TESTING_ENV; ?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_ENVIRONMENT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo PAYPAL_STD_LBL_BUSINESS_EMAIL; ?> : </td>
		<td align="left"><input type="text" name="txt_business_email" id="txt_business_email" value="<?php echo $PaymentDetails['txt_business_email'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_BUSINESS_EMAIL'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td  align="left" class="form_caption"><?php echo PAYPAL_PRO_PAYMENT_ACTION;?> : </td>
		<td align="left">
		<?php
			if($PaymentDetails['pay_action'] == PAYPAL_PRO_PAYMENT_SALE){
				$Paycheckedsale  = "checked";
			} else {
				$Paycheckedsale  = "";
			}
			if($PaymentDetails['pay_action'] == PAYPAL_PRO_PAYMENT_ORDER){
				$Paycheckedorder  = "checked";
			} else {
				$Paycheckedorder = "";
			}
		    if($PaymentDetails['pay_action'] == PAYPAL_PRO_PAYMENT_AUTHORIZATION){
				$Paycheckedauth  = "checked";
			} else {
				$Paycheckedauth = "";
			}
		
		?>
		<span><input type="radio" id="pay_action" name="pay_action" value="<?php echo PAYPAL_PRO_PAYMENT_SALE;?>" <?php echo $Paycheckedsale;?>><?php echo PAYPAL_PRO_PAYMENT_SALE;?></span>
		<span class="radio_distance"><input type="radio" id="pay_action" name="pay_action" value="<?php echo PAYPAL_PRO_PAYMENT_ORDER;?>" <?php echo $Paycheckedorder;?>><?php echo PAYPAL_PRO_PAYMENT_ORDER;?></span>
		<span class="radio_distance"><input type="radio" id="pay_action" name="pay_action" value="<?php echo PAYPAL_PRO_PAYMENT_AUTHORIZATION;?>" <?php echo $Paycheckedauth;?>><?php echo PAYPAL_PRO_PAYMENT_AUTHORIZATION;?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_PAYMENT_ACTION'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>	
	<tr>
		<td align="left" class="form_caption"><?php echo PAYPAL_STD_LBL_ITEM_NAME; ?> : </td>
		<td align="left"><input type="text" class="text_box_std" name="txt_item_name" id="txt_item_name" value="<?php echo $PaymentDetails['txt_item_name'];?>" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_ITEM_NAME'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo PAYPAL_STD_LBL_CURRENCY_CODE; ?> : </td>
		<td align="left">
			<select name="txt_currency_code" id="txt_currency_code" class="select_small">
				<option value="<?php echo 'AUD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'AUD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_AUSTRALIAN_DOLLAR.' => AUD'; ?></option>
				<option value="<?php echo 'BRL'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'BRL') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_BRAZILIAN_REAL.' => BRL'; ?></option>
				<option value="<?php echo 'CAD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CAD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_CANADIAN_DOLLAR.' => CAD'; ?></option>
				<option value="<?php echo 'CZK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CZK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_CZECH_KORUNA.' => CZK'; ?></option>
				<option value="<?php echo 'DKK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'DKK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_DANISH_KRONE.' => DKK'; ?></option>
				 <option value="<?php echo 'DOP'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'DOP') { echo 'selected'; } ?>><?php echo 'Dominican Republic peso => DOP'; ?></option>
				<option value="<?php echo 'EUR'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'EUR') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_EURO.' => EUR'; ?></option>
				<option value="<?php echo 'EGP'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'EGP') { echo 'selected'; } ?>><?php echo 'Egyptian => EGP'; ?></option>
				<option value="<?php echo 'HKD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'HKD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_HONGKONG_DOLLAR.' => HKD'; ?></option>
				<option value="<?php echo 'HUF'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'HUF') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_HUNGARIAN_FORINT.' => HUF'; ?></option>
				<option value="<?php echo 'ILS'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'ILS') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_ISRAELI_NEW_SHEQEL.' => ILS'; ?></option>
				<option value="<?php echo 'INR'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'INR') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_ISRAELI_NEW_SHEQEL.' => INR'; ?></option>
                <option value="<?php echo 'IDR'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'IDR') { echo 'selected'; } ?>><?php echo 'INDONESIA Rupiah => IDR'; ?></option>
				<option value="<?php echo 'JPY'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'JPY') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_JAPANESE_YEN.' => JPY'; ?></option>
				<option value="<?php echo 'MYR'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'MYR') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_MALAYSIAN_RINGGIT.' => MYR'; ?></option>
				<option value="<?php echo 'MXN'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'MXN') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_MEXICAN_PESO.' => MXN'; ?></option>
				<option value="<?php echo 'NOK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'NOK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_NORWEGIAN_KRONE.' => NOK'; ?></option>
				<option value="<?php echo 'NZD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'NZD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_NEW_ZEALAND_DOLLAR.' =>NZD'; ?></option>
				<option value="<?php echo 'PHP'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'PHP') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_PHILIPPINE_PESO.' => PHP'; ?></option>
				<option value="<?php echo 'PLN'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'PLN') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_POLISH_ZLOTY.' => PLN'; ?></option>
				<option value="<?php echo 'GBP'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'GBP') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_POUND_STERLING.' => GBP'; ?></option>
				<option value="<?php echo 'SGD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'SGD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SINGAPORE_DOLLAR.' => SGD'; ?></option>
				<option value="<?php echo 'SEK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'SEK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SWEDISH_KRONA.' => SEK'; ?></option>
				<option value="<?php echo 'CHF'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CHF') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SWISS_FRANC.' => CHF'; ?></option>
				<option value="<?php echo 'TWD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'TWD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_TAIWAN_NEW_DOLLAR.' => TWD'; ?></option>
				<option value="<?php echo 'THB'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'THB') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_THAI_BAHT.' => THB'; ?></option>
				<option value="<?php echo 'USD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'USD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_US_DOLLAR.' => USD'; ?></option>
				<option value="<?php echo 'TRY'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'TRY') { echo 'selected'; } ?>><?php echo 'Turkish lira'.' => TRY'; ?></option>
			</select>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_CURRENCY_CODE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>	
	<tr>
		<td width="30%" align="left" class="grayheader"><?php echo PAYPAL_REFUND_ACCOUNT_DETAIL; ?> :</td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo PAYPAL_PRO_LBL_USERNAME; ?> : </td>
		<td><input type="text" name="txt_username" id="txt_username" value="<?php echo $PaymentDetails['txt_username'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_PRO_USERNAME'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo PAYPAL_PRO_LBL_PASSWORD; ?> : </td>
		<td align="left"><input type="text" name="txt_password" id="txt_password" value="<?php echo $PaymentDetails['txt_password'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_PRO_PASSWORD'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo PAYPAL_PRO_LBL_SIGNATURE; ?> : </td>
		<td align="left"><input type="text" name="txt_signature" id="txt_signature" value="<?php echo $PaymentDetails['txt_signature'];?>" class="text_box_std" /></td>
	</tr>	
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_PRO_SIGNATURE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo PAYPAL_STD_LBL_CLIENT_ID; ?> : </td>
		<td align="left"><input type="text" name="txt_client_id" id="txt_client_id" value="<?php echo $PaymentDetails['txt_client_id'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYPAL_STD_CLIENT_ID'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo PAYPAL_STD_LBL_CLIENT_SECRET; ?> : </td>
		<td align="left"><input type="text" name="txt_client_secret" id="txt_client_secret" value="<?php echo $PaymentDetails['txt_client_secret'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYPAL_STD_CLIENT_SECRET'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
</table>