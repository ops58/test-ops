<?php
require_once("../../lib/common.php");
require_once(DIR_WS_PAYMENT."/payment.php");
require_once(DIR_WS_MODEL."PaymentMethodMaster.php");
require_once(DIR_WS_PAYMENT . 'quickbook/vendor/autoload.php');
use QuickBooksOnline\API\DataService\DataService;


function processCode()
{

    // Create SDK instance
    $ObjPaymentMethodData = new PaymentMethodData();
    $ObjPaymentMethodMaster = new PaymentMethodMaster();
    $ObjPaymentMethodMaster->setWhere("AND directory = :directory", 'quickbook', 'string');
    $DataDomainPayment = $ObjPaymentMethodMaster->getPaymentMethod();
    if($DataDomainPayment)
        $PaymentMethodDetails = unserialize($DataDomainPayment[0]['payment_details']);
    else
        $PaymentMethodDetails = array();
    if($PaymentMethodDetails['sel_payment_env'] == "1"){
        $baseUrl = 'production';
    } else {
        $baseUrl = 'development';
    }
    $dataService = DataService::Configure(array(
        'auth_mode' => 'oauth2',
        'ClientID' => $PaymentMethodDetails['txt_client_id'],
        'ClientSecret' =>  $PaymentMethodDetails['txt_client_secret'],
        'RedirectURI' => SITE_URL.'payment/quickbook/oauth.php',
        'scope' => 'com.intuit.quickbooks.payment',
        'baseUrl' => "development"
    ));
    $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
    $parseUrl = parseAuthRedirectUrl($_SERVER['QUERY_STRING']);

    /*
     * Update the OAuth2Token
     */
    $accessToken = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken($parseUrl['code'], $parseUrl['realmId']);
    $dataService->updateOAuth2Token($accessToken);

    /*
     * Save the accessToken
     */
    
    $PaymentMethodDetails['txt_access_token'] = $accessToken->getAccessToken();
    $PaymentMethodDetails['txt_refresh_token'] = $accessToken->getRefreshToken();
    $PaymentMethodDetails['txt_x_refresh_token_expires_in'] = $accessToken->getRefreshTokenExpiresAt();
    $PaymentMethodDetails['txt_expires_in'] = $accessToken->getAccessTokenExpiresAt();
    $PaymentMethodDetails['txt_company_id'] = $parseUrl['realmId'];
    $PaymentMethodDetails['txt_auth_code'] = $parseUrl['code'];
    
    $ObjPaymentMethodData->payment_details = serialize($PaymentMethodDetails);
    $ObjPaymentMethodData->payment_method_id = $DataDomainPayment[0]['payment_method_id'];
    $ObjPaymentMethodMaster->editPaymentMethod($ObjPaymentMethodData);
    show_page_header(SITE_URL."payment/quickbook/success.php");
}

function parseAuthRedirectUrl($url)
{
    parse_str($url,$qsArray);
    return array(
        'code' => $qsArray['code'],
        'realmId' => $qsArray['realmId']
    );
}

$result = processCode();