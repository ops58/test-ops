<?php
require_once(__DIR__ . '/vendor/autoload.php');
use QuickBooksOnline\API\DataService\DataService;

define_const('QUICKBOOK_LBL_CLIENT_ID','Client Id');
define_const('QUICKBOOK_LBL_CLIENT_SECRET','Client Secret');

$help_messages1 = array (
    'HELP_QUICKBOOK_CLIENT_ID' => 'Please provide Client Id which was provided by Quickbook Account.',
    'HELP_QUICKBOOK_CLIENT_SECRET' => 'Please provide Client Secret which was provided by Quickbook Account.',
    'HELP_PAYMENT_QUICKBOOK_CURRENCY_CODE' => 'The default currency of the Quickbook account.',
    'HELP_PAYMENT_QUICKBOOK_CONNECT' => '<ol class="spaced">
    							<li>Go to <code>https://developer.intuit.com/v2/ui#/app/startcreate</code></li>
    							<li>Click on <code>My Apps</code> header link, uou can use existing app if already exists or create a new one. Click on <code>Create new app</code> button to create a new app.</li>
    							<li>After clicking on your app, inside development tab you can get reuqired keys.</li>
                                <li>Also set redirection url <code>'.SITE_URL.'payment/quickbook/oauth.php</code> in Redirect URIs 
     section below the keys.</li>
    							<li>Copy and paste required keys in given configuration form and click on <code>Connect To QuickBooks</code> button.</li>
    							<li>After click on QuickBook connect button popup window will be open, in that window follow required steps and authorize app to connect QuickBook.</li>
                                <li><b>Please click on Quickbook Connect button once again whenever you switch your account from Test to Live & update production keys .Please make sure that admin must load with HTTPS to connect quickbook</b></li>
    						</ol>'
    );
$help_messages = array_merge($help_messages,$help_messages1);
$quickbooks_oauth_url = SITE_URL.'payment/quickbook/oauth.php';

if($PaymentDetails['sel_payment_env'] == "1"){
    $baseUrl = 'production';
} else {
    $baseUrl = 'development';
}
$authUrl = '';
if ($PaymentDetails['txt_client_id'] != ''){
    $dataService = DataService::Configure(array(
        'auth_mode' => 'oauth2',
        'ClientID' => $PaymentDetails['txt_client_id'],
        'ClientSecret' =>  $PaymentDetails['txt_client_secret'],
        'RedirectURI' => SITE_URL.'payment/quickbook/oauth.php',
        'scope' => 'com.intuit.quickbooks.payment',
        'baseUrl' => "$baseUrl"
    ));
    $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
    $authUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();
}
// when refresh token is not available or refresh token has been expired then display connect button
$refresh_token_expiry_time = strtotime($PaymentDetails['txt_x_refresh_token_expires_in']);
$current_date = strtotime(date('Y/m/d H:i:s'));
//if ($PaymentDetails['txt_refresh_token'] == '' || (!empty($refresh_token_expiry_time) && ($current_date > $refresh_token_expiry_time))){
?>
<script type="text/javascript" src="https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere.js"></script>
        <script type="text/javascript">
        intuit.ipp.anywhere.setup({
        	grantUrl: '<?php print($authUrl) ?>'
        });
        </script>
        <style> #quick_connect a:focus{ outline:none!important; }</style>
		<ipp:connectToIntuit class="pull-right" id="quick_connect"></ipp:connectToIntuit>
<?php //} ?>
<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">   
	<tr>											
		<td colspan="2"><?php echo $help_messages['HELP_PAYMENT_QUICKBOOK_CONNECT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_ENVIRONMENT; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['sel_payment_env'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['sel_payment_env'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   
		   ?>
		   <span><input type="radio" id="sel_payment_env" name="sel_payment_env" value="1" <?php echo $lchecked;?>><?php echo PAYMENT_LBL_LIVE_ENV; ?></span>
		   <span class="radio_distance"><input type="radio" id="sel_payment_env" name="sel_payment_env" value="0" <?php echo $tchecked;?>><?php echo PAYMENT_LBL_TESTING_ENV; ?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_ENVIRONMENT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo QUICKBOOK_LBL_CLIENT_ID; ?> : </td>
		<td align="left"><input type="text" name="txt_client_id" id="txt_client_id" value="<?php echo $PaymentDetails['txt_client_id'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_QUICKBOOK_CLIENT_ID'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo QUICKBOOK_LBL_CLIENT_SECRET; ?> : </td>
		<td align="left"><input type="text" name="txt_client_secret" id="txt_client_secret" value="<?php echo $PaymentDetails['txt_client_secret'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_QUICKBOOK_CLIENT_SECRET'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo PAYPAL_STD_LBL_CURRENCY_CODE; ?> : </td>
		<td align="left">
			<select name="txt_currency_code" id="txt_currency_code" class="select_small">
				<option value="<?php echo 'AUD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'AUD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_AUSTRALIAN_DOLLAR.' => AUD'; ?></option>
				<option value="<?php echo 'BRL'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'BRL') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_BRAZILIAN_REAL.' => BRL'; ?></option>
				<option value="<?php echo 'CAD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CAD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_CANADIAN_DOLLAR.' => CAD'; ?></option>
				<option value="<?php echo 'CZK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CZK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_CZECH_KORUNA.' => CZK'; ?></option>
				<option value="<?php echo 'DKK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'DKK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_DANISH_KRONE.' => DKK'; ?></option>
				 <option value="<?php echo 'DOP'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'DOP') { echo 'selected'; } ?>><?php echo 'Dominican Republic peso => DOP'; ?></option>
				<option value="<?php echo 'EUR'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'EUR') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_EURO.' => EUR'; ?></option>
				<option value="<?php echo 'EGP'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'EGP') { echo 'selected'; } ?>><?php echo 'Egyptian => EGP'; ?></option>
				<option value="<?php echo 'HKD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'HKD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_HONGKONG_DOLLAR.' => HKD'; ?></option>
				<option value="<?php echo 'HUF'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'HUF') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_HUNGARIAN_FORINT.' => HUF'; ?></option>
				<option value="<?php echo 'ILS'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'ILS') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_ISRAELI_NEW_SHEQEL.' => ILS'; ?></option>
				<option value="<?php echo 'INR'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'INR') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_ISRAELI_NEW_SHEQEL.' => INR'; ?></option>
				<option value="<?php echo 'JPY'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'JPY') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_JAPANESE_YEN.' => JPY'; ?></option>
				<option value="<?php echo 'MYR'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'MYR') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_MALAYSIAN_RINGGIT.' => MYR'; ?></option>
				<option value="<?php echo 'MXN'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'MXN') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_MEXICAN_PESO.' => MXN'; ?></option>
				<option value="<?php echo 'NOK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'NOK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_NORWEGIAN_KRONE.' => NOK'; ?></option>
				<option value="<?php echo 'NZD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'NZD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_NEW_ZEALAND_DOLLAR.' =>NZD'; ?></option>
				<option value="<?php echo 'PHP'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'PHP') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_PHILIPPINE_PESO.' => PHP'; ?></option>
				<option value="<?php echo 'PLN'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'PLN') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_POLISH_ZLOTY.' => PLN'; ?></option>
				<option value="<?php echo 'GBP'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'GBP') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_POUND_STERLING.' => GBP'; ?></option>
				<option value="<?php echo 'SGD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'SGD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SINGAPORE_DOLLAR.' => SGD'; ?></option>
				<option value="<?php echo 'SEK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'SEK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SWEDISH_KRONA.' => SEK'; ?></option>
				<option value="<?php echo 'CHF'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CHF') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SWISS_FRANC.' => CHF'; ?></option>
				<option value="<?php echo 'TWD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'TWD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_TAIWAN_NEW_DOLLAR.' => TWD'; ?></option>
				<option value="<?php echo 'THB'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'THB') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_THAI_BAHT.' => THB'; ?></option>
				<option value="<?php echo 'USD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'USD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_US_DOLLAR.' => USD'; ?></option>
				<option value="<?php echo 'TRY'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'TRY') { echo 'selected'; } ?>><?php echo 'Turkish lira'.' => TRY'; ?></option>
			</select>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_QUICKBOOK_CURRENCY_CODE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	<input type="hidden" name="txt_access_token" id="txt_access_token" value="<?php echo $PaymentDetails['txt_access_token'];?>" class="text_box_std" />
	<input type="hidden" name="txt_refresh_token" id="txt_refresh_token" value="<?php echo $PaymentDetails['txt_refresh_token'];?>" class="text_box_std" />
	<input type="hidden" name="txt_x_refresh_token_expires_in" id="txt_x_refresh_token_expires_in" value="<?php echo $PaymentDetails['txt_x_refresh_token_expires_in'];?>" class="text_box_std" />
	<input type="hidden" name="txt_expires_in" id="txt_expires_in" value="<?php echo $PaymentDetails['txt_expires_in'];?>" class="text_box_std" />
	<input type="hidden" name="txt_company_id" id="txt_company_id" value="<?php echo $PaymentDetails['txt_company_id'];?>" class="text_box_std" />
	<input type="hidden" name="txt_auth_code" id="txt_auth_code" value="<?php echo $PaymentDetails['txt_auth_code'];?>" class="text_box_std" />
</table>