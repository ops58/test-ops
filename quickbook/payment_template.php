<div class="row">
	<?php 
        define('QUICKBOOK_ERROR_MESSAGE','Something wrong');
		$sitepaymentid=$data['sitepaymentid'];
		if(getRequestObj()->get('status')=='failure' && $data['error_status_message']==true) { 
			$status_message = QUICKBOOK_ERROR_MESSAGE;
			if(!empty(getRequestObj()->request('message'))) {
				$status_message = getRequestObj()->request('message');
			}
	?>
	<div class="text-danger text-center col-12"><?php echo $status_message; ?></div>
	<?php } ?>
	<div class="col-12">	
		<div class="text-danger text-right mb-3 mr-3"><small><?php echo COMMON_REQUIRED_INFOMATION; ?></small></div>	
		<div class="form-row mb-3">
			<label for="firstName_<?php echo $sitepaymentid; ?>" class="col-12 col-md-4"><?php echo PAYPAL_PRO_LABEL_FIRSTNAME;?></label>							
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<input type="text" name="quickbook_firstName" id="firstName_<?php echo $sitepaymentid; ?>" value="" maxlength="50" class=" form-control" placeholder="" data-rule-required="true" data-msg-required="<?php echo COMMON_FULLNAME_IS_REQUIRED; ?>">        	        	
					<span class="text-danger" id="FNameError_<?php echo $sitepaymentid; ?>"></span>
					<span class="input-group-require text-danger pl-1"> * </span>
				</div>
			</div>
		</div>
		<div class="form-row mb-3">
			<label for="creditCardType_<?php echo $sitepaymentid; ?>" class="col-12 col-md-4"><?php echo PAYPAL_PRO_LABEL_CARD_TYPE;?></label>
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<select class="select_width custom-select" name="quickbook_creditCardType" id="creditCardType_<?php echo $sitepaymentid; ?>" data-rule-required="true">
						<option value="visa" selected="selected" ><?php echo CREDITCARD_LABEL_VISACARD;?></option>
						<option value="mastercard" ><?php echo CREDITCARD_LABEL_MASTERCARD;?></option>
						<option value="discover" ><?php echo CREDITCARD_LABEL_DISCOVERCARD;?></option>
						<option value="amex" ><?php echo CREDITCARD_LABEL_AMERICANCARD;?></option>
					</select>										        	        	
					<span class="input-group-require text-danger pl-1"></span>
				</div>
			</div>
		</div>
		<div class="form-row mb-3">
			<label for="creditCardNumber_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo PAYPAL_PRO_LABEL_CARD_NUMBER;?></label>
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<input type="text" name="quickbook_creditCardNumber" id="creditCardNumber_<?php echo $sitepaymentid; ?>" value="" maxlength="16" class=" form-control" placeholder="" data-rule-required="true" data-msg-required="<?php echo PAYPAL_PRO_CARD_NUMBER_IS_REQUIRED; ?>" data-msg-creditcardtypes="<?php echo	PAYPAL_PRO_INVALID_CARD_NUMBER;?>">        	        	
					<span class="text-danger" id="CNumberError_<?php echo $sitepaymentid; ?>"></span>
					<span class="input-group-require text-danger pl-1"> * </span>
				</div>
			</div>
		</div>
		<div class="form-row mb-3">
			<label for="expDateMonth_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo PAYPAL_PRO_LABEL_CARD_EXPIRED;?></label>
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<select class="select_width custom-select mr-2" name="quickbook_expDateMonth" id="expDateMonth_<?php echo $sitepaymentid; ?>">
						<option value="1" >01</option>
						<option value="2" >02</option>
						<option value="3" >03</option>
						<option value="4" >04</option>
						<option value="5" >05</option>
						<option value="6" >06</option>
						<option value="7" >07</option>
						<option value="8" >08</option>
						<option value="9" >09</option>
						<option value="10" >10</option>
						<option value="11" >11</option>
						<option value="12" >12</option>
					</select>
					<?php $current_year = date('Y'); ?>
					<select class="select_width custom-select" name="quickbook_expDateYear" id="expDateYear_<?php echo $sitepaymentid; ?>" data-msg-datecompare="<?php echo PAYPAL_PRO_INVALID_EXPIRED_DATE; ?>">
						<?php for($i=0;$i<10;$i++) { ?>
							<option value="<?php echo $current_year; ?>" ><?php echo $current_year; ?></option>
						<?php 	$current_year++; } ?>
					</select>
					<span class="text-danger" id="ExpireDateError_<?php echo $sitepaymentid; ?>"></span>
					<span class="input-group-require text-danger pl-1"></span>
				</div>	
			</div>
		</div>
		<div class="form-row mb-3">
			<label for="cvv2Number_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo PAYPAL_PRO_LABEL_CARD_VERIFY_NUMBER;?></label>
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<input type="text" name="quickbook_cvv2Number" id="cvv2Number_<?php echo $sitepaymentid; ?>" value="" maxlength="4" class=" form-control" placeholder="" data-rule-required="true" data-msg-required="<?php echo PAYPAL_PRO_VERIFICATION_NUMBER_IS_REQUIRED; ?>" data-rule-minlength="3" data-msg-minlength="<?php echo PAYPAL_PRO_INVALID_VERIFICATION_NUMBER; ?>" onkeypress="return NumericValidation(event);">        	        	
					<span class="input-group-require text-danger pl-1"> * </span>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$JS = <<<JS
<script>
jQuery(document).ready(function($){
	$("#creditCardNumber_$sitepaymentid").rules("add", {
  		creditcardtypes : function(e) {
  			var cardtype = {};
  			cardtype[$("#creditCardType_$sitepaymentid").val()] = true;
  			return cardtype;
  		}
	});
	
	$('#creditCardType_$sitepaymentid').change(function(){
		if($("#creditCardNumber_$sitepaymentid").val()){
			$("#creditCardNumber_$sitepaymentid").valid();
		}
	});
	
	$("#expDateYear_$sitepaymentid").rules("add", {
	    datecompare: function(e) {
            return ["ge", 
                $('#expDateYear_$sitepaymentid').val() + '/' + $('#expDateMonth_$sitepaymentid').val() + '/' + new Date().getDate(), 
                new Date().getFullYear()+'/'+(new Date().getMonth()+1)+'/'+new Date().getDate()
            ];
        }
	});
	
	$('#expDateMonth_$sitepaymentid').change(function(){
	    $("#expDateYear_$sitepaymentid").valid();
	});
	
});
</script>
JS;
global $jscode;
$jscode .= $JS;
?>