<?php
require_once(__DIR__ . '/vendor/autoload.php');
use QuickBooksOnline\API\DataService\DataService;

$ObjPaymentMethodData = new PaymentMethodData();
$quickbook_data['amount'] 			= number_format($checkout_details->payment_method_amount, 2, '.', '');
$quickbook_data['currency']         = $PaymentDetails["txt_currency_code"];
$quickbook_data['context']          = array('mobile'=>false,"isEcommerce"=>true);

$quickbook_data['card'] = array();
$quickbook_data['card']['name']	            = $checkout_details->ship_pay_details['quickbook_firstName'];
$quickbook_data['card']['cvc'] 			    = $checkout_details->ship_pay_details['quickbook_cvv2Number'];
$quickbook_data['card']['number'] 			= $checkout_details->ship_pay_details['quickbook_creditCardNumber'];


$month = $checkout_details->ship_pay_details['quickbook_expDateMonth'];
$year_tn = $checkout_details->ship_pay_details['quickbook_expDateYear'];
$month_tn = str_pad($month, 2, '0', STR_PAD_LEFT);

$quickbook_data['card']['expYear'] 		    = $year_tn;
$quickbook_data['card']['expMonth'] 			= $month_tn;
$quickbook_data['card']['address']['streetAddress'] 	= $checkout_details->billing_address["street_address"];
$quickbook_data['card']['address']['region'] 		= $checkout_details->billing_address["state"];
$quickbook_data['card']['address']['country'] 	    = $checkout_details->billing_address["countrycode2"];
$quickbook_data['card']['address']['city'] 		    = $checkout_details->billing_address["city"];
$quickbook_data['card']['address']['postalCode'] 	= $checkout_details->billing_address["postcode"];

if ($PaymentDetails['sel_payment_env'] == 1) {
	define('QUICKBOOK_URL', 'https://api.intuit.com');
	$baseurl = 'production';
} else {
	define('QUICKBOOK_URL', 'https://sandbox.api.intuit.com');
	$baseurl = 'development';
}

define('API_CLIENTID', $PaymentDetails['txt_client_id']);
define('API_SECRETKEY', $PaymentDetails['txt_client_secret']);
define('API_COMPANYID',$PaymentDetails['txt_company_id']);

$redirect_url = (!empty($PaymentDetails['txt_redirect_url']))?$PaymentDetails['txt_redirect_url']:SITE_URL.'payment/quickbook/oauth.php';
 // request for access token using refresh token and update
$dataService = DataService::Configure(array(
    'auth_mode' => 'oauth2',
    'ClientID' => API_CLIENTID,
    'ClientSecret' =>  API_SECRETKEY,
    'RedirectURI' => $redirect_url,
    'baseUrl' => $baseurl,
    'refreshTokenKey' => $PaymentDetails['txt_refresh_token'],
    'QBORealmID' => API_COMPANYID,
));
$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
$refreshedAccessTokenObj = $OAuth2LoginHelper->refreshToken();
$dataService->updateOAuth2Token($refreshedAccessTokenObj);

$PaymentDetails['txt_access_token'] = $refreshedAccessTokenObj->getAccessToken();
$PaymentDetails['txt_refresh_token'] = $refreshedAccessTokenObj->getRefreshToken();
$PaymentDetails['txt_x_refresh_token_expires_in'] = $refreshedAccessTokenObj->getRefreshTokenExpiresAt();
$PaymentDetails['txt_expires_in'] = $refreshedAccessTokenObj->getAccessTokenExpiresAt();

$ObjPaymentMethodData->payment_details = serialize($PaymentDetails);
$ObjPaymentMethodData->payment_method_id = $DataDomainPayment['payment_method_id'];
$ObjSitePaymentMethodMaster->editPaymentMethod($ObjPaymentMethodData);

define('API_ACCESS_TOKEN',$PaymentDetails['txt_access_token']);

// Make the API call to Quickbook for payment
$requestId = date('ymdHis').rand();
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => QUICKBOOK_URL."/quickbooks/v4/payments/charges",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => json_encode($quickbook_data),
    CURLOPT_HTTPHEADER => array(
        'accept: application/json',
        'authorization: Bearer '.API_ACCESS_TOKEN.'',
        'cache-control: no-cache',
        'content-type: application/json',
        'request-id: '.$requestId.'',
        'company-id: '.API_COMPANYID.'',
    ),
));

$json_response = curl_exec($curl);
$response = json_decode($json_response,true);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    echo $response;
}

// Return Array which is required into order confirmation page.
$response_details = array();

$status = 0;
if(array_key_exists('status', $response) && in_array(strtoupper($response['status']), array('DECLINED','CANCELLED'))) {
	$response_details['payment_ack_failure_string'] = 'message='.$response['status'];
}elseif (array_key_exists('errors', $response)) {
    $response_details['payment_ack_failure_string'] = 'message='.$response['errors'][0]['message'];
}
if(array_key_exists('status', $response) && in_array(strtoupper($response['status']), array('AUTHORIZED','CAPTURED','SETTLED'))) {
    $status = 1;
}
$response_details['payment_ack_status'] = $status;

// Acknowledge message like Success, Failure, Successs with warning etc..... received from payment gateway
$response_details['ack_status_message'] = $response['status'];

// Order Progress status id, set from admin side. if this is blank, it will take the default order status
$response_details["order_status"]	= $PaymentDetails[ELEMENT_ORDER_STATUS];

// Transaction Id return by gateway
$response_details['transaction_id'] = $response['id'];

// Selected currency code (3 digit) from admin side
$response_details['currency_code'] = $PaymentDetails['txt_currency_code'];

// Comment sent from payment gateways
$response_details["payment_comment"] = '';

// Serialized data. if other details need to add into database, received from payment gateway
$response_details["payment_response_details"] = serialize($response);

// 1 => send Confirmation mail, 0 => do not send Confirmation mail
$response_details["mail_status"] = 1;

// This will be used to redirect to third party payment website i.e. Paypal Standard Method, CCAvenue 
$response_details["redirect_url"] = '';