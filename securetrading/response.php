<?php
/**
 * This file is check the firstdata response.
 *
 * @author     Radixweb <team.radixweb@gmail.com>
 * @copyright  Copyright (c) 2008, Radixweb
 * @version    1.0
 * @since      1.0
 */


require_once("../../lib/common.php");
require_once(DIR_WS_PAYMENT."payment.php");
$orderref = $RRequest->post('orderref');
if(isset($orderref) && !empty($orderref)) {
	$order_number=$orderref;
}
$stresult = $RRequest->request('stresult');
if($__Session->HasValue('_session_payment_request')) //Partial payment request response handling
{
	if(isset($stresult) && $stresult==1) 
		process_payment_request($sucess=true,false,$order_number,$transaction_id,serialize($_REQUEST));
	else
		process_payment_request(false,$fail=true,$order_number,'',serialize($_REQUEST));
}
$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));

$payment_status_result = false;
if(isset($stresult) && $stresult==1) {
	$payment_status_result = true;
}

if($transaction_id != '') {
	$ObjPaymentDetailsMaster->setWhere("AND transactionid = :transactionid", $transaction_id, 'string');
	$paymentDetailsTrans = $ObjPaymentDetailsMaster->getPaymentDetails(null,$searchPayment);
}
$firstdata_detail = serialize($_REQUEST);

if($paymentDetailsTrans == "" ) {
	$insertedOrdId = $order_number;
	if($payment_status_result===true) {
		$transaction_id = $transaction_id;
		$remark = 'Success';
		updatepaymentdetails($insertedOrdId,$success=true,$transaction_id,$firstdata_detail,$PaymentDetails,$remark);
	} else {
		updatepaymentdetails($insertedOrdId,false,null,$firstdata_detail,$PaymentDetails,null);
	}
}
