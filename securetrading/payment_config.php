<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">   	
	<tr>
		<td width="26%" class="form_caption" align="left"><?php echo SECTRANS_LOGIN_KEY; ?> : </td>
		<td align="left"><input type="text" name="txt_login_key" id="txt_login_key" value="<?php echo $PaymentDetails['txt_login_key'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SECTRANS_LOGIN_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	<!--Merchant email address-->
	<tr>
		<td width="26%" class="form_caption" align="left"><?php echo SECTRANS_MERCHANT_EMAIL; ?> : </td>
		<td align="left"><input type="text" name="txt_merchant_email" id="txt_merchant_email" value="<?php echo $PaymentDetails['txt_merchant_email'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SECTRANS_MERCHANT_EMAIL'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<!--Merchant email address-->
	<tr>
		<td align="left" class="form_caption"><?php echo FIRSTDATA_LBL_CURRENCY_CODE; ?> : </td>
		<td align="left">
			<select name="txt_currency_code" id="txt_currency_code" class="select_small">
				<option value="<?php echo 'AUD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'AUD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_AUSTRALIAN_DOLLAR.' => AUD'; ?></option>
				<option value="<?php echo 'BRL'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'BRL') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_BRAZILIAN_REAL.' => BRL'; ?></option>
				<option value="<?php echo 'CAD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CAD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_CANADIAN_DOLLAR.' => CAD'; ?></option>
				<option value="<?php echo 'CZK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CZK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_CZECH_KORUNA.' => CZK'; ?></option>
				<option value="<?php echo 'DKK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'DKK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_DANISH_KRONE.' => DKK'; ?></option>
				<option value="<?php echo 'EUR'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'EUR') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_EURO.' => EUR'; ?></option>
				<option value="<?php echo 'HKD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'HKD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_HONGKONG_DOLLAR.' => HKD'; ?></option>
				<option value="<?php echo 'HUF'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'HUF') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_HUNGARIAN_FORINT.' => HUF'; ?></option>
				<option value="<?php echo 'ILS'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'ILS') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_ISRAELI_NEW_SHEQEL.' => ILS'; ?></option>
				<option value="<?php echo 'JPY'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'JPY') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_JAPANESE_YEN.' => JPY'; ?></option>
				<option value="<?php echo 'MYR'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'MYR') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_MALAYSIAN_RINGGIT.' => MYR'; ?></option>
				<option value="<?php echo 'MXN'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'MXN') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_MEXICAN_PESO.' => MXN'; ?></option>
				<option value="<?php echo 'NOK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'NOK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_NORWEGIAN_KRONE.' => NOK'; ?></option>
				<option value="<?php echo 'NZD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'NZD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_NEW_ZEALAND_DOLLAR.' =>NZD'; ?></option>
				<option value="<?php echo 'PHP'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'PHP') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_PHILIPPINE_PESO.' => PHP'; ?></option>
				<option value="<?php echo 'PLN'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'PLN') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_POLISH_ZLOTY.' => PLN'; ?></option>
				<option value="<?php echo 'GBP'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'GBP') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_POUND_STERLING.' => GBP'; ?></option>
				<option value="<?php echo 'SGD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'SGD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SINGAPORE_DOLLAR.' => SGD'; ?></option>
				<option value="<?php echo 'SEK'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'SEK') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SWEDISH_KRONA.' => SEK'; ?></option>
				<option value="<?php echo 'CHF'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CHF') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_SWISS_FRANC.' => CHF'; ?></option>
				<option value="<?php echo 'TWD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'TWD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_TAIWAN_NEW_DOLLAR.' => TWD'; ?></option>
				<option value="<?php echo 'THB'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'THB') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_THAI_BAHT.' => THB'; ?></option>
				<option value="<?php echo 'USD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'USD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_US_DOLLAR.' => USD'; ?></option>
			</select>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_FIRSTDATA_CURRENCY_CODE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>	
</table>