<?php
	require_once("../../lib/common.php");
	require_once(DIR_WS_PAYMENT."/payment.php");
	require_once(DIR_WS_LIB.'cart_class.php');

	$checkout_details = new Shopping_Cart_Data(true,true,true,true,false);
	
	$shippingInfo = $checkout_details->shipping_address;
	$billingInfo = $checkout_details->billing_address;
	
	$_REQUEST = array_merge($_REQUEST,encode_decode_params($RRequest->request("s"),'decode')); // decode the params
	$paymentMethodID = $RRequest->request('payment_method_id');
	$payment_method_id = (isset($paymentMethodID) && $paymentMethodID!="")?$paymentMethodID:basename(__DIR__);
	$PaymentDetails = getPaymentMethodSettings($payment_method_id);
		
	$orderid		= $RRequest->request('order_id');
	$amount			= $RRequest->request('total')*100;
	
	$merchant_login	= $PaymentDetails['txt_login_key'];
	$currency_code	= $PaymentDetails['txt_currency_code'];
	$merchant_mail	= $PaymentDetails['txt_merchant_email'];
	$currency_code	= $RRequest->request('ccode');
	
	$town=$billingInfo['city'];
	$country=$billingInfo['countryname'];			
	$address=$billingInfo['street_address'];
	$state=$billingInfo['state'];
	$postcode=$billingInfo['postcode'];		
	$shipping_phone=$billingInfo['phone'];		
	$billing_firstname = $billingInfo['firstname'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></title>
	<link href="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_CSS;?>theme.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript">
var loopTime=setInterval("currentTime()",500);
</script>
<style type="text/css" >
#contentContainer {
	background:none;
}
.div_img_loader {
	/*color:#ffffff;*/
	float:left;
	font-size:72px;
	text-align:left;
	width:25%;
	margin:40px 0px 0px 0px;
}
.btn_pay_style{
	/*background:#17222d;*/
	color:#FFFFFF;
	font-size:16px;
	height:48px;
	padding:3px 5px 3px 5px;
	border:0;
}
.wait_img_loading {
	height:300px;
}
.wait_img_loading .img_load {
	float:none;
	padding:72px 0 10px 0;
	text-align:center;
	line-height:28px;
	font-size:15px;	
	/*font-weight:bold;*/
}
.wait_img_loading .img_load .load_message {
	text-align:right;
	line-height:28px;
	font-size:34px;
	font-weight:bold;
	/*color:#ffffff;*/
	width:66%;
	text-align:center;
	float:left;
	padding-left:172px;
	margin-top: 50px;
}
.wait_img_loading .img_load .click_message {
	text-align:center;
	line-height:28px;
	font-size:16px;
}
</style>
<script type="text/javascript">
<!-- Begin
/* This script and many more are available free online at
The JavaScript Source!! http://javascript.internet.com
Created by: Abraham Joffe :: http://www.abrahamjoffe.com.au/ */

var startTime=new Date();
var strDisplay = '.';
var repeat = 5;
var start = 0;
function currentTime(){
  var a=Math.floor((new Date()-startTime)/100)/10;
  if(start != repeat ) {
  	document.getElementById("div_repeat_info").innerHTML = document.getElementById("div_repeat_info").innerHTML + strDisplay;
  	start = start + 1;
  } else {
  	document.getElementById("div_repeat_info").innerHTML = strDisplay;
  	start = 0;
  }
  /*if (a%1==0) a+=".0";
  document.getElementById("div_img_loader").innerHTML=a;*/
}

window.onload=function(){
  clearTimeout(loopTime);
  strDisplay = document.getElementById("div_repeat_info").innerHTML
}

// End -->
</script>
</head>

<body>
<form action="https://securetrading.net/authorize/form.cgi" method="POST" name="payment">
<div id="container">
	
	
	<div id="header">
		<div id="headerleft">
		<!-- Header Left -->
			<div class="logo" style="margin: 60px 0px 0px 0px;">				
				<span><a href="<?php echo show_page_link(FILE_INDEX);?>"><img src="<? echo SITE_LOGO_PATH; ?>" /></a></span>
			</div>
		</div>
	</div>
	<!-- main contian End -->
	<div id="contentContainer" class="wait_img_loading">
		<div class="img_load">
			<div class="page-section-header load_message"><?php echo "Please wait..."; ?></div>
			<div style="float:left;padding:10px 0 7px 7px;text-align:left;width:100%;text-align:center;"><?php echo LABEL_PAGE_LOAD_PROBLEM ;?></div>
			<div style="float:left;padding:0px 0 7px 7px;width:100%;"><?php if(file_exists(DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES.'img_click_to_pay.gif')) { ?>
				<label class="btn btn-primary btn_left_co"><input type="submit" class="btn btn-primary btn_pay_style" style="font-size: 15px;" name="btnpaysubmit" src="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES; ?>img_click_to_pay.gif" title="Click here to continue..." value="<?php echo "Click here to continue...";?>" alt="Click here to continue..."></label>
			<?php } else { ?>
				<label class="btn btn-primary btn_left_co"><input type="submit" class="btn btn-primary btn_pay_style" name="btnpaysubmit" title="Click here to continue..." value="<?php echo "Click here to continue...";?>" ></label>
			<?php } ?></div>
		</div>
			
		    <input type="hidden" name="merchant" value="<?=$merchant_login;?>"/>
			<input type="hidden" name="amount" value="<?=$amount;?>"/>
			<INPUT TYPE="hidden" NAME="town" value="<?=$town;?>">
			<INPUT TYPE="hidden" NAME="county" value="<?=$state;?>">
			<INPUT TYPE="hidden" NAME="country" value="<?=$country;?>">
			<INPUT TYPE="hidden" NAME="address" value="<?=$address;?>">
			<INPUT TYPE="hidden" NAME="merchantemail" VALUE="<?=$merchant_mail?>">
			<INPUT TYPE="hidden" NAME="currency" VALUE="<?=$currency_code;?>">
			<input type="hidden" value="<?=getAccessObj()->getUserEmail()?>" name="email"/>
			<input type="hidden" value="<?=$shipping_phone;?>" name="telephone"/>
			<input type="hidden" value="<?=$billing_firstname;?>" name="name"/>
			<input type="hidden" value="<?=$postcode;?>" name="postcode"/>
			<input type="hidden" name="orderref" value="<?php echo $RRequest->request('order_id'); ?>" />
			<input type="hidden" name="callbackurl" value="1"/>
			<input type="hidden" name="failureurl" value="1"/>
			<input type="hidden" name="orderinfo" value="Vision Impress" />
			<?php 
			$merchantFieldData = 'onevisionfour18445GBP1040A6y5RGE3';
			$merchantHash = md5($merchantFieldData);
			?>
			<input type="hidden" name="st_sitesecurity" value="<?php echo $merchantHash; ?>" />
		</div>
	</div>
</div>
</form>
<!-- Main End -->
</body>
</html>
<script language="javascript">
document.payment.submit();
</script>