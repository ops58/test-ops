<?php
$action = $RRequest->request('action');
if($action == 'delete'){
    require_once("../../lib/common.php");
    require_once ("languages/" . DIR_CURRENT_LANGUAGE.'.php');
}else {
    require_once("lib/common.php");
    require_once(DIR_WS_PAYMENT."paytrace_v2/languages/" . DIR_CURRENT_LANGUAGE.'.php');
}
require_once ('PhpApiSettings.php');
require_once ('Utilities.php');
require_once(DIR_WS_PAYMENT."/payment.php");
require_once(DIR_WS_MODEL . "PaymentMethodMaster.php");

$UtilMasterObj    = new UtilMaster();
$ObjSitePaymentMethodMaster = new PaymentMethodMaster();
$table_query ='CREATE TABLE IF NOT EXISTS `paytrace_master` (
			  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
			  `userid` int(11) NOT NULL,
			  `paytrace_customer_id` varchar(255) NOT NULL,
              `user_card_number` varchar(255) NOT NULL,
              `response_data` varchar(255) NOT NULL,  
			  PRIMARY KEY (`auto_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';
$UtilMasterObj->setCustomQuery($table_query);
$UtilMasterObj->exec_query();


$UtilMasterObj->setFrom('paytrace_master')->setWhere("AND userid = :userid",getAccessObj()->getUserID(),"int");
$data_array = $UtilMasterObj->exec_query();
$auth_cim_selectd = 'new';
$deleteMessage = PAYTRACE_DELETE_CARD_CONFIRM;

// Get Payment Method Details
$ObjSitePaymentMethodMaster->setSelect(array("payment_method_id","payment_details"))
->setWhere("AND directory = :directory","paytrace_v2","string");
$DataDomainPayment = $ObjSitePaymentMethodMaster->getPaymentMethod();
if ($DataDomainPayment) {
    $paymentMethodID = $DataDomainPayment[0]['payment_method_id'];
    $PaymentDetails  = unserialize($DataDomainPayment[0]['payment_details']);
}

if($RRequest->request('action') == 'delete'){
    $uid = $RRequest->request('id');
    $result = "failure";
    $oauth_result = oAuthTokenGenerator($PaymentDetails['txt_username'],$PaymentDetails['txt_password']);
    $token_response = json_decode($oauth_result['temp_json_response'],true);
    
    if(!empty($token_response['error'])){
        $resArray = $token_response;
        $failure = true;
       // $result = $token_response['error_description'];
       $result = 'failure';
    }else {
        $oauth_token = sprintf("Bearer %s",$token_response['access_token']);
        $UtilMasterObj->setFrom('paytrace_master')
        ->setWhere("auto_id  = :auto_id",$uid,"int");
        $data = $UtilMasterObj->exec_query();
        $data = $data[0];
        $customerProfileId = $data['paytrace_customer_id'];
        //$paymentProfileId = $data['payment_profile_id'];
        $request_data = array(
            'customer_id' => $customerProfileId
        );
        $request_data = json_encode($request_data);
        $ch = curl_init();
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization:'.$oauth_token;
        
        //echo "<br> Auth token : " .$oauth_token ;
        // set URL and other appropriate options for curl to make the request
        curl_setopt($ch, CURLOPT_URL, URL_DELETE_CUSTOMER);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        // curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        // following SSL settings should be removed in production code.
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        
        //Execute the request
        $response = curl_exec($ch);
        $curl_error = curl_error($ch).' '. curl_errno($ch);
        
        //echo "<br>" .$response ;
        $response = json_decode($response);
        
        if ($response->response_code == 162){
            $delete_query = "DELETE FROM paytrace_master WHERE auto_id=".$uid;
            $UtilMasterObj->exec_query($delete_query);
            $result = 'success';
        }
        if($response === false){
            $result['curl_error'] = $curl_error;
        
            // close cURL resource, and free up system resources
            curl_close($ch);
        }
    }
    echo json_encode(array("result"=>$result));
    exit();
}
?>

<div class="row">
	<?php 
		$sitepaymentid=$data['sitepaymentid'];
		if($RRequest->get('status')=='failure' /* && $data['error_status_message']==true */) { 
			$status_message = PAYPAL_PRO_ERROR_MESSAGE;
			if(!empty($RRequest->request('message'))) {
				$status_message = $RRequest->request('message');
			}
	?>
	<div class="text-danger text-center col-12"><?php echo $status_message; ?></div>
	<?php } ?>
	<div class="col-12">
		
		
		<div class="calc_loading" style="display:none;">
			<div class="loading-container-sm">
				<div class="loading"></div>
				<div id="loading-text"><?php echo COMMON_WAIT;?></div>
         	</div>
         </div>
         
        <?php if(!empty($data_array) && $PaymentDetails['txt_save_card'] == 1 ){  ?>
		<div class="form-group row mb-0">
    			<input type="hidden" name="payment_option" id="payment_option" value="" />
    			<input type="hidden" name="payment_vault_id" id="payment_vault_id" value="<?php echo $data_array[0]['id']; ?>" />
    			<?php foreach ($data_array as $key=>$value){  ?>
    			<div class="<?php echo $value['auto_id'];?> mb-3 col-12">
    				<div class="custom-control custom-radio">
        				<?php $data_label = PAYTRACE_PAY_WITH.' <strong>'.$value['user_card_number'].'</strong>'; ?>
        				<input type="radio" name="payment_type" id="<?php echo $value['auto_id']?>" onchange="show_hide_paytrace_payment(this.id);" class="payment_radio existing pretty <?php echo $value['auto_id'];?> existing custom-control-input" <?php if($key == 0){echo 'checked="checked"';}?> value="<?php echo $value['auto_id']; ?>"  style="margin-left:20px;"/>
        				<label for="<?php echo $value['auto_id']?>" class="custom-control-label"><?php echo $data_label;?><a href="javascript:void(0);" id="<?php echo $value['auto_id'];?>" class="remove ml-2" title="Remove this card"><i class="fa fa-trash"></i></a></label>
    				</div>
    			</div>
    			 <?php } ?>
		 </div>
		 <?php } ?> 
		 <?php if($PaymentDetails['txt_save_card'] == 1 ){  ?>
    		 <div class="col-12">
     			<div class="new custom-control custom-radio">
					<input type="radio" name="payment_type" id="new" onchange="show_hide_paytrace_payment(this.id);" class="new custom-control-input" value="new" style="margin-left:20px;" <?php if($auth_cim_selectd == 'new') echo 'checked';?>/>
					<label for="new" class="custom-control-label"><?php echo PAYTRACE_PAY_NEW_DETAIL;?></label>
				</div>
    		</div>
		<?php } ?>
		 
		 
		<input type="hidden" name="payment_type_new" id="payment_type_new" />
		<div class="form-horizontal" id="auth_form">
		
	
		
		<div class="text-danger text-right mb-3 mr-3"><small><?php echo COMMON_REQUIRED_INFOMATION; ?></small></div>	
		<div class="form-row mb-3">
			<label for="creditCardType_<?php echo $sitepaymentid; ?>" class="col-12 col-md-4"><?php echo PAYPAL_PRO_LABEL_CARD_TYPE;?></label>
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<select class="select_width custom-select" name="paytrace_creditCardType" id="creditCardType_<?php echo $sitepaymentid; ?>" data-rule-required="true">
						<option value="visa" selected="selected" ><?php echo CREDITCARD_LABEL_VISACARD;?></option>
						<option value="mastercard" ><?php echo CREDITCARD_LABEL_MASTERCARD;?></option>
						<option value="discover" ><?php echo CREDITCARD_LABEL_DISCOVERCARD;?></option>
						<option value="amex" ><?php echo CREDITCARD_LABEL_AMERICANCARD;?></option>
					</select>										        	        	
					<span class="input-group-require text-danger pl-1"></span>
				</div>
			</div>
		</div>
		<div class="form-row mb-3">
			<label for="creditCardNumber_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo PAYPAL_PRO_LABEL_CARD_NUMBER;?></label>
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<input type="text" name="paytrace_creditCardNumber" id="creditCardNumber_<?php echo $sitepaymentid; ?>" value="" maxlength="16" class=" form-control" placeholder="" data-rule-required="true" data-msg-required="<?php echo PAYPAL_PRO_CARD_NUMBER_IS_REQUIRED; ?>" data-msg-creditcardtypes="<?php echo	PAYPAL_PRO_INVALID_CARD_NUMBER;?>">        	        	
					<span class="text-danger" id="CNumberError_<?php echo $sitepaymentid; ?>"></span>
					<span class="input-group-require text-danger pl-1"> * </span>
				</div>
			</div>
		</div>
		<div class="form-row mb-3">
			<label for="expDateMonth_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo PAYPAL_PRO_LABEL_CARD_EXPIRED;?></label>
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<select class="select_width custom-select mr-2" name="paytrace_expDateMonth" id="expDateMonth_<?php echo $sitepaymentid; ?>">
						<option value="1" >01</option>
						<option value="2" >02</option>
						<option value="3" >03</option>
						<option value="4" >04</option>
						<option value="5" >05</option>
						<option value="6" >06</option>
						<option value="7" >07</option>
						<option value="8" >08</option>
						<option value="9" >09</option>
						<option value="10" >10</option>
						<option value="11" >11</option>
						<option value="12" >12</option>
					</select>
					<?php $current_year = date('Y'); ?>
					<select class="select_width custom-select" name="paytrace_expDateYear" id="expDateYear_<?php echo $sitepaymentid; ?>" data-msg-datecompare="<?php echo PAYPAL_PRO_INVALID_EXPIRED_DATE; ?>">
						<?php for($i=0;$i<10;$i++) { ?>
							<option value="<?php echo $current_year; ?>" ><?php echo $current_year; ?></option>
						<?php 	$current_year++; } ?>
					</select>
					<span class="text-danger" id="ExpireDateError_<?php echo $sitepaymentid; ?>"></span>
					<span class="input-group-require text-danger pl-1"></span>
				</div>	
			</div>
		</div>
		<div class="form-row mb-3">
			<label for="cvv2Number_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo PAYPAL_PRO_LABEL_CARD_VERIFY_NUMBER;?></label>
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<input type="text" name="paytrace_cvv2Number" id="cvv2Number_<?php echo $sitepaymentid; ?>" value="" maxlength="4" class=" form-control" placeholder="" data-rule-required="true" data-msg-required="<?php echo PAYPAL_PRO_VERIFICATION_NUMBER_IS_REQUIRED; ?>" data-rule-minlength="3" data-msg-minlength="<?php echo PAYPAL_PRO_INVALID_VERIFICATION_NUMBER; ?>" onkeypress="return NumericValidation(event);">        	        	
					<span class="input-group-require text-danger pl-1"> * </span>
				</div>
			</div>
		</div>
	    
	     <?php if($PaymentDetails['txt_save_card'] == 1 ){  ?>
	    <div class="row mb-3"> 
	        <div class="col-12">
                <div class="custom-control custom-checkbox">
    				<input type="checkbox" class="custom-control-input" id="save_this_card" name="save_this_card" value="1"/>
    				<label class="custom-control-label" for="save_this_card">Do you want to save this card? </label>
				</div>
	        </div>
        </div>
	    
	    <?php }?>
	    
	</div>
	</div>
</div>
<?php
$PAYTRACE_CARD_DELETE_SUCCESS = PAYTRACE_DELETE_CARD_SUCCESS;
$PAYTRACE_CARD_DELETE_FAILURE = PAYTRACE_DELETE_CARD_FAILURE;

$JS = <<<JS
<script>

function show_hide_paytrace_payment(id){
	var payId = $(document).find("#cim_payment_method_id").val();
	$("#paymethod"+payId).find('.addressof').hide();
	$("#paymethod"+payId).find('#addressof_'+id).show();
	if(id == 'new'){ 
		$("#paymethod"+payId).find("#payment_option").val('');
		$("#auth_form").show();
	}else{
		$("#paymethod"+payId).find("#payment_option").val('1');
		$("#auth_form").hide();
	}
	$('#payment_type_new').val(id);
}

jQuery(document).ready(function($){
   $('.remove').click(function(){
		if(confirm("$deleteMessage")){
		    var mid = $(this).attr('id');
		    $('.calc_loading').show();
		    $.ajax({
				url : "/payment/paytrace_v2/payment_template.php?id="+mid+"&action=delete",
				dataType : "json",
				success : function(data){
					$('.calc_loading').hide();
					if(data.result == "success"){
						$('.'+mid).remove();
						$('#payment_option').val(0);
						$("#new").prop("checked", true);
						$('#auth_form').show();
						$.bootstrapGrowl("$PAYTRACE_CARD_DELETE_SUCCESS",{type:'success', delay:'4000'});
					}else{
						$.bootstrapGrowl("$PAYTRACE_CARD_DELETE_FAILURE",{type:'danger', delay:'4000'});
					}
				}
			});
		}
	});
	var cardChecked = $(".existing").first(); 
	if($('#existing').attr('checked')) {
    	$('#payment_option').val(1);
	   	$('#auth_form').hide();
	}
	if(cardChecked.is(":checked")){
		$("#payment_option").val('1');
 		$("#auth_form").hide();
 		$("#payment_vault_id").val($(cardChecked).attr('id'));
	}
    show_hide_paytrace_payment("$auth_cim_selectd");
});

jQuery(document).ready(function($){
	$("#creditCardNumber_$sitepaymentid").rules("add", {
  		creditcardtypes : function(e) {
  			var cardtype = {};
  			cardtype[$("#creditCardType_$sitepaymentid").val()] = true;
  			return cardtype;
  		}
	});
	
	$('#creditCardType_$sitepaymentid').change(function(){
		if($("#creditCardNumber_$sitepaymentid").val()){
			$("#creditCardNumber_$sitepaymentid").valid();
		}
	});
	
	$("#expDateYear_$sitepaymentid").rules("add", {
	    datecompare: function(e) {
            return ["ge", 
                $('#expDateYear_$sitepaymentid').val() + '/' + $('#expDateMonth_$sitepaymentid').val() + '/' + new Date().getDate(), 
                new Date().getFullYear()+'/'+(new Date().getMonth()+1)+'/'+new Date().getDate()
            ];
        }
	});
	
	$('#expDateMonth_$sitepaymentid').change(function(){
	    $("#expDateYear_$sitepaymentid").valid();
	});
	
});
</script>
JS;
global $jscode;
$jscode .= $JS;
?>