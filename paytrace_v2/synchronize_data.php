<?php
$Customer_Payment_Details = array();

$Customer_Payment_Details[] = array(
	'Label' => PAYPAL_PRO_LABEL_CARD_HOLDER_NAME,
	'Value' => $CustomerPaymentDetails['pp_pro_firstName'] . ' ' . $CustomerPaymentDetails['pp_pro_lastName']
);

$Customer_Payment_Details[] = array(
	'Label' => PAYPAL_PRO_LABEL_CARD_TYPE,
	'Value' => $CustomerPaymentDetails['paytrace_creditCardType']
);

$Customer_Payment_Details[] = array(
	'Label' => PAYPAL_PRO_LABEL_CARD_NUMBER,
	'Value' => 'XXXXXXXXXXXX'.substr($CustomerPaymentDetails['paytrace_creditCardNumber'],12)
);

$Customer_Payment_Details[] = array(
	'Label' => PAYPAL_PRO_LABEL_CARD_EXPIRED,
	'Value' => $CustomerPaymentDetails['paytrace_expDateMonth'] . ',  ' . $CustomerPaymentDetails['paytrace_expDateYear']
);

$Customer_Payment_Details[] = array(
	'Label' => PAYPAL_PRO_LABEL_CARD_VERIFY_NUMBER,
	'Value' => $CustomerPaymentDetails['paytrace_cvv2Number']
);


?>