<?php 
define_const('PAYTRACE_PAY_NEW_DETAIL',"Pay with new card");
define_const('PAYTRACE_PAY_WITH',' Pay with');
define_const('PAYTRACE_DELETE_CARD_SUCCESS','Card has been deleted successfully');
define_const('PAYTRACE_DELETE_CARD_FAILURE','Card Can not be deleted due to some error');
define_const('PAYTRACE_DELETE_CARD_CONFIRM','Are you sure you want to delete this card?');
?>