<?php 
define_const('PAYTRACE_LBL_USERNAME','Username');
define_const('PAYTRACE_LBL_PASSWORD','Password');
define_const('PAYTRACE_LBL_SAVE_CARD','Save Card');

$help_messages1 = array (
    'HELP_PAYTRACE_USERNAME' => 'Please provide username which was provided by Paytrace Account.',
    'HELP_PAYTRACE_PASSWORD' => 'Please provide password which was provided by Paytrace Account.',
    'HELP_PAYTRACE_SAVE_CARD' => "Enable to store card details of the customer on Paytrace merchant account.<br>Customer can see list of saved cards on shopping cart page and able to choose existing save card to make payment. ",
);
$help_messages = array_merge($help_messages,$help_messages1);
?>
<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">   
	<tr>
		<td class="form_caption" align="left"><?php echo PAYTRACE_LBL_USERNAME; ?> : </td>
		<td><input type="text" name="txt_username" id="txt_username" value="<?php echo $PaymentDetails['txt_username'];?>" class="text_box_std col-md-3" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYTRACE_USERNAME'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo PAYTRACE_LBL_PASSWORD; ?> : </td>
		<td align="left"><div class="input-group col-md-3"><input type="password" name="txt_password" id="txt_password" value="<?php echo $PaymentDetails['txt_password'];?>" class="text_box_std form-control" /><span class="input-group-addon password_toggle"><i class="pwd-icon fa fa-eye"></i></span></div></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYTRACE_PASSWORD'];?></td>
	</tr>
	
	
	<tr>
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	
	<tr>
		<td width="30%" align="left" class="form_caption"><?php echo PAYTRACE_LBL_SAVE_CARD; ?> : </td>
		<td align="left">
			<input type="hidden" name="txt_save_card" value="0"/>
		  	<input class="radiochk" type="checkbox" value="1" name="txt_save_card" <?php if($PaymentDetails['txt_save_card'] == "1"){ echo 'checked';  } ?>>
		</td>
	</tr>	
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYTRACE_SAVE_CARD'];?></td>
	</tr>
	
</table>
