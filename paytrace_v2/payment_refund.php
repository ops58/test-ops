<?php 
require_once ('PhpApiSettings.php');
require_once ('Utilities.php');
require_once(DIR_WS_MODEL . "UtilMaster.php");

function paytrace_v2_process_refund($OrderDetails,$paymentdetails,$refund_data) {
    
    $refund_detail = array();    
    $oauth_result = oAuthTokenGenerator($paymentdetails['txt_username'],$paymentdetails['txt_password']);
    $token_response = json_decode($oauth_result['temp_json_response'],true);
  
    if(!empty($token_response['error'])){
        $resArray = $token_response;
        $failure = true;
        $failure_msg = $token_response['error_description'];
        $refund_detail['orders_id'] = $OrderDetails['orders_id'];
        if(!empty($refund_data['orders_products_id']))
            $refund_detail['orders_products_id'] = $refund_data['orders_products_id'];
            $refund_detail['refund_amount'] = $refund_data['refund_amount'];
            $refund_detail['status'] = 'failure';
            $refund_detail['error'] = $failure_msg;
            
    } else {
        
        $oauth_token = sprintf("Bearer %s",$token_response['access_token']);
        $request_data = array(            
            "transaction_id" => $OrderDetails['transactionid']
        );
        
        $request_data = json_encode($request_data);       
        //Check current traction is pending for settlement Or already settled
        $trans_result = processTransaction($oauth_token,$request_data, URL_EXPORT_BY_TRANSACTION_ID);       
        $transaction_sts_result = json_decode($trans_result['temp_json_response'],true);   
        
        $void_transaction = false;
        $refund_action = URL_REFUND_BY_TRANSACTION_ID;
        if($transaction_sts_result['transactions'][0]['status_code'] == 'P'){ //Pending for settlement
            $void_transaction = true;
            $refund_action = URL_VOID_TRANSACTION;
        }             

        // Request for refund/void transaction
        $request_data = array(
            "amount" => $refund_data['refund_amount'],
            "transaction_id" => $OrderDetails['transactionid']
        );
        $request_data = json_encode($request_data);
        $trans_result = processTransaction($oauth_token,$request_data, $refund_action );       
       
        if($trans_result['curl_error'] ){
            $failure = true;
            $failure_msg = $trans_result['curl_error'];
        } else {
            $refund_response = json_decode($trans_result['temp_json_response'],true);              
            if($trans_result['http_status_code'] != 200){
                $failure = true;
                $failure_msg = $trans_result['curl_error'];
                if (empty($failure_msg)){
                    $failure_msg = $refund_response['status_message'];
                    if(!empty($refund_response['errors'])){
                        foreach ($refund_response['errors'] as $key=>$val){
                            $failure_msg.= " Code :".$key." Message : ".$val[0];
                        }
                    }                    
                }
            }// 106 for refund & 109 for Void
            else if($refund_response['success']== true && ($refund_response['response_code'] == 106 || $refund_response['response_code'] == 109)) {
                // For transaction successfully approved
                $failure = false;
            }
        }
        
        if($failure ==  true){
            $refund_detail['orders_id'] = $OrderDetails['orders_id'];
            if(!empty($refund_data['orders_products_id']))
                $refund_detail['orders_products_id'] = $refund_data['orders_products_id'];
                $refund_detail['refund_amount'] = $refund_data['refund_amount'];
                $refund_detail['status'] = 'failure';
                $refund_detail['error'] = $failure_msg;
        }
        else {
            parse_str($refund_response, $refund_response1);
            $refund_response1['payment_method'] = $paymentdetails['payment_method_name'];
            $refund_detail['status'] = 'success';
            $refund_detail['refund_response'] = json_encode(array_merge($refund_response,$refund_response1));
            $refund_detail['error'] = '';            
        }
        
        if(!empty($refund_detail['error'])){
            paymentwriteLog($refund_detail,$paymentdetails,"refund_order_payment");
        }
    }
   
    return $refund_detail;
}
?>