<?php
require_once ('PhpApiSettings.php');
require_once ('Utilities.php');
require_once(DIR_WS_MODEL . "UtilMaster.php");

$UtilMasterObj    = new UtilMaster();
$user_id  			= getAccessObj()->getUserID();
$oauth_result = oAuthTokenGenerator(USERNAME,PASSWORD);
$token_response = json_decode($oauth_result['temp_json_response'],true);
$orderid = date("YmdHis");
$customer_id = $user_id.date("YmdHis");
if(!empty($token_response['error'])){
    $resArray = $token_response;
    $failure = true;
    $failure_msg = $token_response['error_description'];
} else {
    $oauth_token = sprintf("Bearer %s",$token_response['access_token']);
    $month = $checkout_details->ship_pay_details['paytrace_expDateMonth'];
    $year_tn = $checkout_details->ship_pay_details['paytrace_expDateYear'];
    $month_tn = str_pad($month, 2, '0', STR_PAD_LEFT);
    
    //If Payment is to be done by new card and not by any existing card.
    if ($checkout_details->ship_pay_details['payment_type_new'] == 'new'){
        //If new card is not be saved than make simple api call for transaction.
        if (!$checkout_details->ship_pay_details['save_this_card'] || $PaymentDetails['txt_save_card'] == 0 ){
            $request_data = array(
                "amount" => number_format($checkout_details->payment_method_amount,2),
                "invoice_id" => $orderid,
                "credit_card"=> array (
                    "number"=> $checkout_details->ship_pay_details['paytrace_creditCardNumber'],
                    "expiration_month"=> $month_tn,
                    "expiration_year"=> $year_tn),
                "csc"=> $checkout_details->ship_pay_details['paytrace_cvv2Number'],
                "billing_address"=> array(
                    "name"=> $checkout_details->billing_address['firstname']." ".$checkout_details->billing_address['lastname'],
                    "street_address"=> $checkout_details->billing_address["street_address"],
                    "city"=> $checkout_details->billing_address["city"],
                    "state"=> $checkout_details->billing_address["statecode"],
                    "zip"=> $checkout_details->billing_address["postcode"])
            );
            $request_data = json_encode($request_data);
            $resArray = $trans_result = processTransaction($oauth_token,$request_data, URL_KEYED_SALE );
        }else{
            //If card is to be saved,than first store the card(Create profile) and make transaction by saved card.
            if ($checkout_details->ship_pay_details['save_this_card']){
                $request_data = array(
                    "customer_id" => $customer_id,
                    "credit_card"=> array (
                        "number"=> $checkout_details->ship_pay_details['paytrace_creditCardNumber'],
                        "expiration_month"=> $month_tn,
                        "expiration_year"=> $year_tn),
                    "csc"=> $checkout_details->ship_pay_details['paytrace_cvv2Number'],
                    "billing_address"=> array(
                        "name"=> $checkout_details->billing_address['firstname']." ".$checkout_details->billing_address['lastname'],
                        "street_address"=> $checkout_details->billing_address["street_address"],
                        "city"=> $checkout_details->billing_address["city"],
                        "state"=> $checkout_details->billing_address["statecode"],
                        "zip"=> $checkout_details->billing_address["postcode"])
                );
                $request_data = json_encode($request_data);
                $ch = curl_init();
                $header[] = 'Content-type: application/json';
                $header[] = 'Authorization:'.$oauth_token;
                 
                //echo "<br> Auth token : " .$oauth_token ;
                // set URL and other appropriate options for curl to make the request
                curl_setopt($ch, CURLOPT_URL, URL_CREATE_CUSTOMER);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                // curl_setopt($ch, CURLOPT_HEADER, true);
                curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $request_data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                 
                // following SSL settings should be removed in production code.
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
                curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
                
                //Execute the request
                $response = curl_exec($ch);
                $curl_error = curl_error($ch).' '. curl_errno($ch);
                
                //echo "<br>" .$response ;
                
                if($response === false){
                    $result['curl_error'] = $curl_error;
                
                    // close cURL resource, and free up system resources
                    curl_close($ch);
                }
                $res_array = $response;
                $response = json_decode($response);
                //save the card in table when response code is 160(Success for creating the customer profile)
                if ($response->success == true && $response->response_code == '160' && !empty($response->customer_id)){//check : condition needs to be changed according to the response
                    $UtilMasterObj->setCustomQuery("INSERT INTO paytrace_master(userid, paytrace_customer_id,user_card_number,response_data) VALUES (:userid, :paytrace_customer_id, :user_card_number, :response_data)",
                        array(':userid'=>$user_id,':paytrace_customer_id'=>$response->customer_id,':user_card_number'=>$response->masked_card_number,':response_data'=>$res_array),array(':userid'=>'int',':paytrace_customer_id'=>'string',':user_card_number'=>'string',':response_data'=>'string'));
                    $UtilMasterObj->exec_query();
                }
                $request_data = array(
                    "amount" => number_format($checkout_details->payment_method_amount,2),
                    "invoice_id" => $orderid,
                    "customer_id" => $response->customer_id
                );
                $request_data = json_encode($request_data);
                $resArray = $trans_result = processTransaction($oauth_token,$request_data,URL_VAULT_SALE_BY_CUSTOMER_ID);
            }
        }
    }else{
        //Use any existing card for making the payment.
        $UtilMasterObj->setSelect('paytrace_customer_id')
        ->setFrom('paytrace_master')
        ->setWhere("AND auto_id = :auto_id",$checkout_details->ship_pay_details['payment_type_new'],"int");
        $customer_arr 	= $UtilMasterObj->exec_query();
        $customer_id     = $customer_arr[0]['paytrace_customer_id'];
        $request_data = array(
            "amount" => number_format($checkout_details->payment_method_amount,2),
            "invoice_id" => $orderid,
            "customer_id" => $customer_id
        );
        $request_data = json_encode($request_data);
        $resArray = $trans_result = processTransaction($oauth_token,$request_data, URL_VAULT_SALE_BY_CUSTOMER_ID );
        
    }
    
    if($trans_result['curl_error'] ){
        $failure = true;
        $failure_msg = $trans_result['curl_error'];
    } else {
        $json = json_decode($trans_result['temp_json_response'],true);
        
        if($trans_result['http_status_code'] != 200){
            $failure = true;
            $failure_msg = $trans_result['curl_error'];
            if (empty($failure_msg)){
                $failure_msg = $json['status_message'];
            }
            $ack_status_message = $json['status_message'];
        }
        else if($json['success']== true && $json['response_code'] == 101) {
            // For transaction successfully approved
            $failure = false;
            $ack_status_message = $json['status_message'];
            $transaction_id = $json['transaction_id'];
        }
    }
}

// Return Array which is required into order confirmation page.
$response_details = array();

// 0=>failure, 1=>success, 2=>cancel by user(This will be used to redirect to third party payment website i.e. Paypal Standard Method, CCAvenue )
$status = 1;
if($failure === true) {
	$status = 0;
	$response_details['payment_ack_failure_string'] = 'message='.$failure_msg;
}
$response_details['payment_ack_status'] = $status;

// Acknowledge message like Success, Failure, Successs with warning etc..... received from payment gateway
$response_details['ack_status_message'] = $ack_status_message;

// Order Progress status id, set from admin side. if this is blank, it will take the default order status
$response_details["order_status"]	= $PaymentDetails[ELEMENT_ORDER_STATUS];

// Transaction Id return by gateway
$response_details['transaction_id'] = $transaction_id;

// Selected currency code (3 digit) from admin side
$response_details['currency_code'] = $PaymentDetails['txt_currency_code'];

// Comment sent from payment gateways
$response_details["payment_comment"] = '';

// Serialized data. if other details need to add into database, received from payment gateway
$response_details["payment_response_details"] = serialize($resArray);

// 1 => send Confirmation mail, 0 => do not send Confirmation mail
$response_details["mail_status"] = 1;

// This will be used to redirect to third party payment website i.e. Paypal Standard Method, CCAvenue 
$response_details["redirect_url"] = '';