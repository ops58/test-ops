<?php
require_once("../../lib/common.php");
require_once(DIR_WS_PAYMENT."/payment.php");
require_once(DIR_WS_LIB.'cart_class.php');

$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));
$reference = $RRequest->request('order_id');
if($__Session->HasValue('_session_payment_request')) {
	$checkout_details = new Shopping_Cart_Data(true,false,false,true,false,true);
	$orderid = $idempotency_key        = "PR_".$reference."_".generateRandomNo(5);
} else {
	$checkout_details        = new Shopping_Cart_Data(true,false,false,true,false);
	$orderid = $idempotency_key        = "OR_".$reference."_".generateRandomNo(5);
}
//https://developer.squareup.com/docs/build-basics/working-with-monetary-amounts
if($PaymentDetails['txt_currency_code'] == 'JPY'){
    $amount = ceil($checkout_details->payment_method_amount); // This payment doesn't allowed to pass float amount.
}
else{
    //FOR USD needs to pass value in cents.
    $amount = number_format($checkout_details->payment_method_amount,2,'.',''); // This payment doesn't allowed to pass float amount.
    $amount = $amount *100;
    $amount = intval((string)$amount); // Allow only integer and getting amount as double with above code hence convert to integer
}

$redirect_url =  show_page_link("payment/squareup/response.php",true);
$redirect_url =  str_replace("/","\/",$redirect_url);	   

/* $request = array(
    "redirect_url" => $redirect_url,
    'idempotency_key' => $idempotency_key,
    'ask_for_shipping_address' => false,
    'merchant_support_email' => $PaymentDetails['txt_email_address'],
    'order' => array('reference_id' => $orderid,
                     'line_items' => array(array(
                         'name' => $PaymentDetails['txt_item_name'],
                         'quantity' => "1",
                         'base_price_money' => array(
                          'amount' => $amount,
                             'currency' => (empty($PaymentDetails['txt_currency_code'])?'GBP':$PaymentDetails['txt_currency_code'])
                         )
                 ) ) ),
    'pre_populate_buyer_email' => getAccessObj()->getUserEmail()
); */
// For New Version API structure is not like above . If any clients having issue inform them to update API version. 
$request = array(
    "redirect_url" => $redirect_url,
    'idempotency_key' => $idempotency_key,
    'ask_for_shipping_address' => false,
    'merchant_support_email' => $PaymentDetails['txt_email_address'],
    'order' => array(
        'idempotency_key' => generateRandomNo(4),
        'order' => array('reference_id' => $orderid,'location_id' => $PaymentDetails['txt_location_id'],
            'customer_id' => 'CUS'.getAccessObj()->getUserID(),
            'metadata' => array('type'=>'SquareupCheckout'),
            'metadata' => array('type'=>'SquareupCheckout'),
                'line_items' => array(array(
                    'name' => $PaymentDetails['txt_item_name'],
                    'quantity' => "1",
                    'base_price_money' => array(
                        'amount' => $amount,
                        'currency' => (empty($PaymentDetails['txt_currency_code'])?'GBP':$PaymentDetails['txt_currency_code'])
                    )
                ) ) ) ),
    'pre_populate_buyer_email' => getAccessObj()->getUserEmail()
);

$curl = curl_init();
if ($PaymentDetails['sel_payment_env'] == 1) {
    $post_url   ="https://connect.squareup.com/v2/locations/";//for live
} else {
    $post_url   ="https://connect.squareupsandbox.com/v2/locations/";//for testing
}

$apiURL = $post_url.$PaymentDetails['txt_location_id']."/checkouts";
$PaymentDetails['txt_access_token'];
curl_setopt_array($curl, array(
    CURLOPT_URL => $apiURL,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => json_encode($request),
    CURLOPT_HTTPHEADER => array(
        "content-type: application/json",
        "Authorization: Bearer ".$PaymentDetails['txt_access_token']
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);
$response = json_decode($response); 
if(!empty($response->checkout->checkout_page_url))
   header('Location:' . $response->checkout->checkout_page_url);
else{
   $errormsg = array("Error"=>$response->errors[0]->detail);
   $response_details = serialize($errormsg);
   if($__Session->HasValue('_session_payment_request')) { //Partial payment request response handling
        process_payment_request(false,$fail=true,$reference,'',$response_details);
    } else {
        updatepaymentdetails($reference,false,null,$response_details,$PaymentDetails,null,false);
    }
}
?>