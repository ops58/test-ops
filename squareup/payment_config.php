<?php require_once(DIR_WS_PAYMENT."squareup/languages/" . DIR_CURRENT_LANGUAGE.'.php'); ?>
<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">   
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_ENVIRONMENT; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['sel_payment_env'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['sel_payment_env'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   
		   ?>
		   <span><input type="radio" id="sel_payment_env" name="sel_payment_env" value="1" <?php echo $lchecked;?>><?php echo PAYMENT_LBL_LIVE_ENV; ?></span>
		   <span class="radio_distance"><input type="radio" id="sel_payment_env" name="sel_payment_env" value="0" <?php echo $tchecked;?>><?php echo PAYMENT_LBL_TESTING_ENV; ?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_ENVIRONMENT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	
	
	<tr>
		<td class="form_caption" align="left"><?php echo SQUARE_APPLICATION_ID_LABEL; ?> : </td>
		<td><input type="text" name="txt_application_id" id="txt_application_id" value="<?php echo $PaymentDetails['txt_application_id'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages1['HELP_PAYMENT_SQUARE_APPLICATION_ID'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo SQUARE_ACCESS_TOKEN_LABEL; ?> : </td>
		<td align="left"><input type="text" name="txt_access_token" id="txt_access_token" value="<?php echo $PaymentDetails['txt_access_token'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages1['HELP_PAYMENT_SQUARE_ACCESS_TOKEN'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo SQUARE_LOCATION_ID_LABEL; ?> : </td>
		<td align="left"><input type="text" name="txt_location_id" id="txt_location_id" value="<?php echo $PaymentDetails['txt_location_id'];?>" class="text_box_std" /></td>
	</tr>	
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages1['HELP_PAYMENT_SQUARE_LOCATION_ID'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo SQUARE_SUPPORT_EMAIL_ID_LABEL; ?> : </td>
		<td align="left"><input type="text" name="txt_email_address" id="txt_email_address" value="<?php echo $PaymentDetails['txt_email_address'];?>" class="text_box_std" /></td>
	</tr>	
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages1['HELP_PAYMENT_SQUARE_SUPPORT_EMAIL_ID'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo PAYPAL_STD_LBL_ITEM_NAME; ?> : </td>
		<td align="left"><input type="text" class="text_box_std" name="txt_item_name" id="txt_item_name" value="<?php echo $PaymentDetails['txt_item_name'];?>" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_ITEM_NAME'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo PAYPAL_STD_LBL_CURRENCY_CODE; ?> : </td>
		<td align="left">
			<select name="txt_currency_code" id="txt_currency_code" class="select_small">				
				<option value="<?php echo 'GBP'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'GBP') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_POUND_STERLING.' => GBP'; ?></option>				
				<option value="<?php echo 'USD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'USD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_US_DOLLAR.' => USD'; ?></option>
                <option value="<?php echo 'CAD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'CAD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_CANADIAN_DOLLAR.' => CAD'; ?></option>
				<option value="<?php echo 'JPY'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'JPY') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_JAPANESE_YEN.' => JPY'; ?></option>
			</select>            
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_CURRENCY_CODE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
</table>