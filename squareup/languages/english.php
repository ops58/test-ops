<?php
//payulatam payment gateway//
define_const('SQUARE_APPLICATION_ID_LABEL', 'Application Id');
define_const('SQUARE_ACCESS_TOKEN_LABEL', 'Access Token');
define_const('SQUARE_LOCATION_ID_LABEL', 'Location Id');
define_const('SQUARE_SUPPORT_EMAIL_ID_LABEL', 'Support Email Id');

/*Help messages */
	$help_messages1= array (
    	'HELP_PAYMENT_SQUARE_APPLICATION_ID' => 'Application Id that was created on Square api site.',
        'HELP_PAYMENT_SQUARE_ACCESS_TOKEN' => 'Access token that was generated on Square api site.',
        'HELP_PAYMENT_SQUARE_LOCATION_ID' => 'Location Id that was generated on Square api site.',
        'HELP_PAYMENT_SQUARE_SUPPORT_EMAIL_ID' => 'Support Email address of Merchant.',
	);
	$help_messages =array_merge($help_messages,$help_messages1);
?>