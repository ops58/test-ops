<?php
/* https://developer.squareup.com/docs/webhooks-api/subscribe-to-events#subscribe-for-a-webhook-notification
Please click on this link . Here will get steps to how to subscrbe.
In your application click on Webhook . Click on 'Add Endpoint' .
A popup will be open.
In popup enter below URL.
https://yourdomain.com/payment/squareup/payment_notification.php
In last you needs to choose 2 events.
1) Payment.created
2) Payment.updated */

    require_once("../../lib/common.php");
    require_once(DIR_WS_PAYMENT."payment.php");
    require_once(DIR_WS_MODEL."PaymentDetailsMaster.php");
    $PaymentDetails = getPaymentMethodSettings(basename(__DIR__));

    $requestBody = file_get_contents('php://input');
    $responseData = json_decode($requestBody, true);
    $eventType = $responseData['type'];
    $orderId = $responseData['data']['object']['payment']['order_id'];

    if($eventType == 'payment.updated') {
        $eventPaymentCardStatus = $responseData['data']['object']['payment']['card_details']['status'];

        /* $current_date =date('Y-m-d H:i:s');
        $file = dirname(__FILE__).'/callback_data.txt';
        $fp = fopen($file, 'a');
        $current ='';
        $current .= "\n\n===callback payload=====[".$current_date."]=======".SITE_URL."==========\n\n";
        $current .= print_r($responseData,true);
        $current .= "\n\n=========================================================================\n\n";
        // Write the contents back to the file
        fwrite($fp, $current);
        fclose($fp);
        $err = curl_error($curl); */

        if($PaymentDetails['sel_payment_env'] == 1) {
            $url =  "https://connect.squareup.com/v2/orders/$orderId";
        } else {
            $url =  "https://connect.squareupsandbox.com/v2/orders/$orderId";
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
                "Authorization: Bearer ".$PaymentDetails['txt_access_token'],
                "Square-Version: 2021-03-17"
            ),
        ));

        $response = curl_exec($curl);
        $responseDetails = serialize($response);
        $err = curl_error($curl);

        /* $file = dirname(__FILE__).'/callback_data.txt';
        $fp = fopen($file, 'a');
        $current ='';
        $current .= "\n\n====callback orde rresosne===[".$current_date."]=======".SITE_URL."==========\n\n";
        $current .= print_r($url,true);
        $current .= "\n";
        $current .= print_r(json_decode($responseDetails,true),true);
        $current .= "\n\n=========================================================================\n\n";
        // Write the contents back to the file
        fwrite($fp, $current);
        fclose($fp);     */

        if(!empty($response)){
            //Metadata Type we have passed durinf request raised from squareup.php file
            if($response['order']['metadata']['type'] == 'SquareupCheckout') {
                $transactionId = $response['order']['id'];
                $referenceId = $response['order']['reference_id'];
                $arrReference = explode("_", $referenceId);
                $paymentType = $arrReference[0];
                $orderId = $arrReference[1];

                if(isset($paymentType) && $paymentType == 'PR') { //Partial payment request response handling
                    if($eventPaymentCardStatus == 'CAPTURED' && isset($response['order']['state']) &&  ($response['order']['state'] == "COMPLETED")) {
                        process_payment_request($sucess=true,false,$orderId,$transactionId, $responseDetails,false);
                    } else {
                        process_payment_request(false,$fail=true,$orderId,'',$responseDetails,false);
                    }
                } else if(isset($paymentType) && $paymentType == 'OR') { //Partial payment request response handling
                    if($eventPaymentCardStatus == 'CAPTURED' && isset($response['order']['state']) &&  ($response['order']['state'] == "COMPLETED")){
                        $remark = 'Success';
                        updatepaymentdetails($orderId,$success=true,$transactionId,$responseDetails,$PaymentDetails,$remark,true);
                    } else {
                        updatepaymentdetails($orderId,false,null,$responseDetails,$PaymentDetails,null,true);
                    }
                }
            }
        }
    }
?>