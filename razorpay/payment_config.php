<?php 

define_const('RAZORPAY_CLIENT_KEY','Client Key');
define_const('RAZORPAY_CLIENT_SECRET','Client Secret');
define_const('RAZORPAY_MERCHANT_NAME','Merchant Name');

$help_messages1 = array (
    'HELP_RAZORPAY_CLIENT_KEY' => 'Please provide Client key which was provided by Razorpay Account.',
    'HELP_RAZORPAY_CLIENT_SECRET' => 'Please provide Client Secret which was provided by Razorpay Account.',
    'HELP_RAZORPAY_MERCHANT_NAME' => 'Please provide Merchant Name to be shown in Checkout Form.',
    );
$help_messages = array_merge($help_messages,$help_messages1);
?>
<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">  
	<tr>
		<td class="form_caption" align="left"><?php echo RAZORPAY_CLIENT_KEY; ?> : </td>
		<td align="left"><input type="text" name="txt_client_key" id="txt_client_key" value="<?php echo $PaymentDetails['txt_client_key'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_RAZORPAY_CLIENT_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo RAZORPAY_CLIENT_SECRET; ?> : </td>
		<td align="left"><input type="text" name="txt_client_secret" id="txt_client_secret" value="<?php echo $PaymentDetails['txt_client_secret'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_RAZORPAY_CLIENT_SECRET'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	
	<tr>
		<td class="form_caption" align="left"><?php echo RAZORPAY_MERCHANT_NAME; ?> : </td>
		<td align="left"><input type="text" name="txt_merchant_name" id="txt_merchant_name" value="<?php echo $PaymentDetails['txt_merchant_name'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_RAZORPAY_MERCHANT_NAME'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
</table>