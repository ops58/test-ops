<?php 
    require_once("../../lib/common.php");
    require_once(DIR_WS_PAYMENT."/payment.php");
    require_once(DIR_WS_LIB.'cart_class.php');
    
    $PaymentDetails = getPaymentMethodSettings(basename(__DIR__));
    $orderid		= $RRequest->request('order_id');
    if($__Session->HasValue('_session_payment_request')) {
        $checkout_details = new Shopping_Cart_Data(true,true,true,true,true,true);
        $custom_value = "direct_payment";
    } else {
        $checkout_details        = new Shopping_Cart_Data(true,true,true,true,true);
        $custom_value = "order";
    }
    $email          = getAccessObj()->getUserEmail();
    $customer_name = $checkout_details->billing_address['firstname'].' '.$checkout_details->billing_address['lastname'];
    $amt = $checkout_details->payment_method_amount;
    $amt = number_format($amt,2,".","");
    $amount = $amt*100; //Its allow price only in cents
    $client_key     = $PaymentDetails['txt_client_key'];
    $client_secret 	= $PaymentDetails['txt_client_secret'];
    $merchant_name  = $PaymentDetails['txt_merchant_name'];
    $response_url   = show_page_link("payment/razorpay/response.php",true);
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></title>
	<link href="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_CSS;?>theme.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript">
var loopTime=setInterval("currentTime()",500);
</script>
<style type="text/css" >
#contentContainer {
	background:none;
}
.div_img_loader {
	/*color:#df6c1b;*/
	float:left;
	font-size:72px;
	text-align:left;
	width:25%;
	margin:-12px 0px 0px 0px;
}
.btn_pay_style{
	/*background:#df6c1b;*/
	color:#FFFFFF;
	font-size:16px;
	height:48px;
	padding:3px 5px 3px 5px;
}
.wait_img_loading .img_load {
	float:none;
	padding:72px 0 10px 0;
	text-align:center;
	line-height:28px;
	font-size:15px;	
	/*font-weight:bold;*/
}
.wait_img_loading .img_load .load_message {
	text-align:right;
	line-height:28px;
	font-size:34px;
	font-weight:bold;
	/*color:#df6c1b;*/
	width:66%;
	text-align:center;
	float:left;
	padding-left:90px;
}
.wait_img_loading .img_load .click_message {
	text-align:center;
	line-height:28px;
	font-size:16px;
}
</style>
<script src='https://checkout.razorpay.com/v1/checkout.js'></script>
	<script src="<?php echo DIR_HTTP_THIRDPARTY.'js/jquery.js'; ?>" ></script>
	<script src="<?php echo DIR_HTTP_THIRDPARTY.'js/jquery-ui.js'; ?>" ></script>
</head>
<body>
<div id="container">
	<!-- main contian End -->
    <div id="contentContainer" class="wait_img_loading">
		<input type="hidden" name="key" id="key" value="<?php echo $client_key; ?>">
		<input type="hidden" name="amount" id="amount" value="<?php echo $amount; ?>">
		<input type="hidden" name="name" id="name" value="<?php echo $merchant_name; ?>">
		<input type="hidden" name="description" id="description" value="product">
		<input type="hidden" name="prefill_email" id="prefill_email" value="<?php echo $email; ?>" />
		<input type="hidden" name="prefill_name" id="prefill_name" value="<?php echo $customer_name; ?>" />
		<input type="hidden" name="notes" id="notes" value="<?php echo $custom_value; ?>">
		<input type="hidden" name="response_url" id="response_url" value="<?php echo $response_url; ?>" />
		<input type="hidden" name="invoice_no" id="invoice_no" value="<?php echo $orderid; ?>" />
    </div>
    <input type="button" name="pay_now" id="pay_now" value="dsdsdsd" style="display:none;" />
    <div id="contentContainer" class="wait_img_loading">
		<div class="img_load">
			<div class="page-section-header load_message"><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></div><div id="div_repeat_info" class="page-section-header div_img_loader">.</div>			
	</div>
</div>
<!-- Main End -->
</body>
<script>
$(document).ready(function () {
	$('#pay_now').addClass('hide');
	$("div#contentContainer").hide();
	var key = $('#key').val();
	var amount = $('#amount').val();
	var name = $('#name').val();
	var description = $('#description').val();
	var prefill_email = $('#prefill_email').val();
	var prefill_name = $('#prefill_name').val();
	var notes = $('#notes').val();
	var response_url = $('#response_url').val();
	var invoice_no = $('#invoice_no').val();
	
	var options = {
	    "key": key,
	    "amount": amount, // 2000 paise = INR 20
	    "name": name,
	    "description": description,
	    "modal.backdropclose" : false,
	    "modal.escape" : true,	    
	    "handler": function (response){
	    	$("div#contentContainer").show();
	        var razorpay_payment_id = response.razorpay_payment_id;
	        if(razorpay_payment_id != ''){
	        	response_url = response_url+"?razorpay_payment_id="+razorpay_payment_id+"&amount="+amount+"&notes="+notes+"&reference_no="+invoice_no;
	        	document.location.replace(response_url);
	    	}
	    },
	    "prefill": {
	        "email": prefill_email,
	        "name" : prefill_name
	    },
	    "notes": {
		    "order_id":invoice_no,
	        "payment_type": notes
	    },
	    "theme": {
	        "color": "#F37254"
	    },
	    "modal": {
	        "ondismiss": function(){
	        	$("div#contentContainer").show();
	        	response_url = response_url+"?reference_no="+invoice_no+"&notes="+notes;
	        	document.location.replace(response_url);
	        }
	    }
	};
	var rzp1 = new Razorpay(options);

	document.getElementById('pay_now').onclick = function(e){
	    rzp1.open();
	    e.preventDefault();
	}
	$('#pay_now').trigger('click');
});
</script>
</html>