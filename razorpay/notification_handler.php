<?php 
//Webhook - https://razorpay.com/docs/webhooks/integrations/#setting-up
//secret set into account = 1234567890
require_once("../../lib/common.php");
require_once(DIR_WS_PAYMENT."/payment.php");
require_once(DIR_WS_MODEL."PaymentDetailsMaster.php");
require_once(DIR_WS_MODEL."PaymentRequestMaster.php");
$PaymentDetailsMasterObj= new PaymentDetailsMaster();
$ObjPaymentRequest		= new PaymentRequestMaster();

$payload = file_get_contents("php://input");
$payload_arr = json_decode($payload , true);

$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));
$client_key     = $PaymentDetails['txt_client_key'];
$client_secret 	= $PaymentDetails['txt_client_secret'];

/* $current_date =date('Y-m-d H:i:s');
$file = dirname(__FILE__).'/error.txt';
$fp = fopen($file, 'a');
$current ='';
$current .= print_r($payload_arr,true);
$current .= "\n\n===============================".$current_date."=====================================\n\n";
// Write the contents back to the file
fwrite($fp, $current);
fclose($fp); */

if($payload_arr['event'] == 'payment.authorized'){
    
    $razorpay_payment_id = $payload_arr['payload']['payment']['entity']['id'];
    $payment_status = $payload_arr['payload']['payment']['entity']['status'];
    $order_id = $payload_arr['payload']['payment']['entity']['notes']['order_id'];
    $payment_type = $payload_arr['payload']['payment']['entity']['notes']['payment_type'];
    $amount = $payload_arr['payload']['payment']['entity']['amount'];
    
    /* $current_date =date('Y-m-d H:i:s');
    $file = dirname(__FILE__).'/error.txt';
    $fp = fopen($file, 'a');
    $current ='';    
    $current .= print_r("payment id :".$razorpay_payment_id,true);
    $current .= "\n";
    $current .= print_r("Payment status :".$payment_status,true);
    $current .= "\n";
    $current .= print_r("Payment type :".$payment_type,true);
    $current .= "\n";
    $current .= print_r("Amount :".$amount,true);
    $current .= "\n";
    $current .= "\n\n=========================================================================\n\n";
    // Write the contents back to the file
    fwrite($fp, $current);
    fclose($fp); */
    
    $capture_payment = false;
    if(!empty($razorpay_payment_id)){
        //to find the status of payment
        $apiURL = "https://api.razorpay.com/v1/payments/$razorpay_payment_id";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_USERPWD => "$client_key:$client_secret",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));
        $pay_response = curl_exec($curl);
        $err = curl_error($curl);
        $pay_response = json_decode($pay_response,true);
        $current_payment_status = $pay_response['status'];
        $capture_payment = ($current_payment_status == 'captured')?false:true;
    }
    
   /*  $current_date =date('Y-m-d H:i:s');
    $file = dirname(__FILE__).'/error.txt';
    $fp = fopen($file, 'a');
    $current ='';
    $current .= print_r("Capture:".$order_id." -".$capture_payment,true);   
    $current .= "\n\n=========================================================================\n\n";
    // Write the contents back to the file
    fwrite($fp, $current);
    fclose($fp); */
    
    // If we call already captured payment once again api will throw error.
    if($payment_status == 'authorized' && $capture_payment == true){
        
        $apiURL = "https://api.razorpay.com/v1/payments/$razorpay_payment_id/capture";
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "amount=$amount",
            CURLOPT_USERPWD => "$client_key:$client_secret",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        $response = json_decode($response,true);
        $response_details = serialize($response);
        
        
        /* $current_date =date('Y-m-d H:i:s');
        $file = dirname(__FILE__).'/error.txt';
        $fp = fopen($file, 'a');
        $current ='';
        $current .= print_r("capture Response :",true);
        $current .= print_r($response,true);
        $current .= "\n\n=========================================================================\n\n";
        // Write the contents back to the file
        fwrite($fp, $current);
        fclose($fp); */
        
        
         if($payment_type == 'direct_payment') {        
            if(isset($response['status']) && $response['status'] == "captured"){
                process_payment_request($sucess=true,false,$order_id,$response['id'], $response_details,false);
            }else {
                process_payment_request(false,$fail=true,$order_id,'',$response_details,false);
            }
        } else if($payment_type == 'order') {
            if(isset($response['status']) && $response['status'] == "captured"){
                $remark = 'Success';
                updatepaymentdetails($order_id,$success=true,$response['id'],$response_details,$PaymentDetails,$remark,true);
            }else {
                updatepaymentdetails($order_id,false,null,$response_details,$PaymentDetails,null,true);
            }
        } 
        
    }
}
?>