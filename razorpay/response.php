<?php 
require_once("../../lib/common.php");
require_once(DIR_WS_PAYMENT."/payment.php");
require_once(DIR_WS_MODEL."PaymentDetailsMaster.php");
require_once(DIR_WS_MODEL."PaymentRequestMaster.php");
$PaymentDetailsMasterObj= new PaymentDetailsMaster();
$ObjPaymentRequest		= new PaymentRequestMaster();

require_once(DIR_WS_LIB.'cart_class.php');
define_const('RAZORPAY_CLOSE_IFRAME_RESPONSE','Payment has been cancelled by customer by closing payment popup.');

$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));
$razorpay_payment_id = initRequestValue('razorpay_payment_id');
$amount = initRequestValue('amount');
$client_key     = $PaymentDetails['txt_client_key'];
$client_secret 	= $PaymentDetails['txt_client_secret'];
$invoice_no   = initRequestValue('reference_no');
$payment_type = initRequestValue('notes');

$capture_payment = false;
if(!empty($razorpay_payment_id)){
    //to find the status of payment
    $apiURL = "https://api.razorpay.com/v1/payments/$razorpay_payment_id";
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $apiURL,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_USERPWD => "$client_key:$client_secret",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
        ),
    ));
    $pay_response = curl_exec($curl);
    $err = curl_error($curl);
    $pay_response = json_decode($pay_response,true);
    $current_payment_status = $pay_response['status'];
    $capture_payment = ($current_payment_status == 'captured')?false:true;
    if($current_payment_status == 'captured'){
        $response['status'] = 'captured';
    }
}

/* $current_date =date('Y-m-d H:i:s');
$file = dirname(__FILE__).'/error.txt';
$fp = fopen($file, 'a');
$current ='';
$current .= "\n\n===========================Response Page  $current_date =========================\n\n";
$current .= print_r("captured require or not :".$invoice_no." - ".$capture_payment,true);
$current .= print_r("Razor payment id- ".$razorpay_payment_id,true);
$current .= print_r($pay_response,true);
$current .= "\n\n=========================================================================\n\n";
// Write the contents back to the file
fwrite($fp, $current);
fclose($fp); */

$reference = $invoice_no;
if( $capture_payment == true) {
    if(!empty($razorpay_payment_id) ) {
        $apiURL = "https://api.razorpay.com/v1/payments/$razorpay_payment_id/capture";
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "amount=$amount",
            CURLOPT_USERPWD => "$client_key:$client_secret",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        $response = json_decode($response,true);
        $response_details = serialize($response);
        
        /* $current_date =date('Y-m-d H:i:s');
        $file = dirname(__FILE__).'/error.txt';
        $fp = fopen($file, 'a');
        $current ='';
        $current .= "\n\n===========================Response Page captured response  $current_date =========================\n\n";
        $current .= print_r($response,true);
        $current .= "\n\n=========================================================================\n\n";
        // Write the contents back to the file
        fwrite($fp, $current);
        fclose($fp); */
        
    } else {
        $response = array('error' => array('description' => RAZORPAY_CLOSE_IFRAME_RESPONSE));
        $response_details = serialize($response);
    }
    $reference = isset($response['notes']['order_id'])?$response['notes']['order_id']:"";
}

if($payment_type == 'direct_payment' && !empty($razorpay_payment_id)) {
    
    if(isset($response['status']) && $response['status'] == "captured"){
        process_payment_request($sucess=true,false,$reference,$response['id'], $response_details);
    }else {
        process_payment_request(false,$fail=true,$invoice_no,'',$response_details);
    }
    
} else if($payment_type == 'direct_payment' && empty($razorpay_payment_id)) {
    process_payment_request(false,$fail=true,$invoice_no,'',$response_details);
} else if($payment_type == 'order' && !empty($razorpay_payment_id)) {
    if(isset($response['status']) && $response['status'] == "captured"){
        $remark = 'Success';
        updatepaymentdetails($reference,$success=true,$response['id'],$response_details,$PaymentDetails,$remark,false);
    }else {
        updatepaymentdetails($invoice_no,false,null,$response_details,$PaymentDetails,null,false);
    }
} else if($payment_type == 'order' && empty($razorpay_payment_id)) {
    updatepaymentdetails($invoice_no,false,null,$response_details,$PaymentDetails,null,false);
}
?>