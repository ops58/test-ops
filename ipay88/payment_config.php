<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">   
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_ENVIRONMENT; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['sel_payment_env'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['sel_payment_env'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   
		   ?>
		   <span><input type="radio" id="sel_payment_env" name="sel_payment_env" value="1" <?php echo $lchecked;?>><?php echo PAYMENT_LBL_LIVE_ENV; ?></span>
		   <span class="radio_distance"><input type="radio" id="sel_payment_env" name="sel_payment_env" value="0" <?php echo $tchecked;?>><?php echo PAYMENT_LBL_TESTING_ENV; ?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_ENVIRONMENT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<!-- <tr>
		<td class="form_caption" align="left"><?php //echo PAYPAL_STD_LBL_BUSINESS_EMAIL; ?> : </td>
		<td align="left"><input type="text" name="txt_business_email" id="txt_business_email" value="<?php //echo $PaymentDetails['txt_business_email'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php //echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php //echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_BUSINESS_EMAIL'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php //echo draw_separator($sep_width,$sep_height);?></td>
	</tr>-->
	<tr>
		<td align="left" class="form_caption"><?php echo PAYPAL_STD_LBL_ITEM_NAME; ?> : </td>
		<td align="left"><input type="text" class="text_box_std" name="txt_item_name" id="txt_item_name" value="<?php echo $PaymentDetails['txt_item_name'];?>" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_ITEM_NAME'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo MERCHANT_CODE; ?> : </td>
		<td align="left"><input type="text" name="txt_merchant_code" id="txt_merchant_code" value="<?php echo $PaymentDetails['txt_merchant_code'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_MERCHANT_CODE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>	
	<tr>
		<td class="form_caption" align="left"><?php echo MERCHANT_KEY; ?> : </td>
		<td align="left"><input type="text" name="txt_merchant_key" id="txt_merchant_key" value="<?php echo $PaymentDetails['txt_merchant_key'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_MERCHANT_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>	
	<tr>
		<td class="form_caption" align="left"><?php echo COMMON_CURRENCY; ?> : </td>
		<td align="left">
			<select name="txt_currency_code" id="txt_currency_code" class="select_small" style="width:200px;">
			    <option value="<?php echo 'EUR'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'EUR') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_EURO.' => EUR'; ?></option>
				<option value="<?php echo 'MYR'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'MYR') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_MALAYSIAN_RINGGIT.' => MYR'; ?></option>
				<option value="<?php echo 'USD'; ?>" <?php if($PaymentDetails['txt_currency_code'] == 'USD') { echo 'selected'; } ?>><?php echo PAYPAL_STD_CURRENCY_US_DOLLAR.' => USD'; ?></option>
			</select>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_CURRENCY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>		
</table>