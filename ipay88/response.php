<?php
/**
 * This file is check the ipay response.
 *
 * @author     Radixweb <team.radixweb@gmail.com>
 * @copyright  Copyright (c) 2008, Radixweb
 * @version    1.0
 * @since      1.0
 */
require_once ("../../lib/common.php");
require_once (DIR_WS_PAYMENT . "payment.php");
require (DIR_WS_PAYMENT . "ipay88/ipay88_cls.php");
$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));

$order_number = $RRequest->request('RefNo');
$transaction_id = $RRequest->request('TransId');
$payment_status = $RRequest->request('Status');
$response_details   = serialize($RRequest->request());
$remark = $RRequest->request('Remark');
if(isset($remark) && $remark == 'direct_payment') { //Partial payment request response handling
    $ipn = true;   
    if($payment_status == '1')
        process_payment_request($sucess=true,false,$order_number,$transaction_id, $response_details,$ipn);
    else
        process_payment_request(false,$fail=true,$order_number,'',$response_details,$ipn);
    
} else if(isset($remark) && $remark == 'order') { //Order  payment request response handling
    $ipn = false;   
    if($payment_status == '1') {
        $remark = 'Success';
        updatepaymentdetails($order_number,$success=true,$transaction_id,$response_details,$PaymentDetails,$remark,$ipn);
    } else {
        updatepaymentdetails($order_number,false,null,$response_details,$PaymentDetails,null,$ipn);
    }
}
?>