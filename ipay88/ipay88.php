<?php
	require_once("../../lib/common.php");
	require_once(DIR_WS_PAYMENT."/payment.php");
	require_once(DIR_WS_LIB.'cart_class.php');
	require_once(DIR_WS_MODEL . "PaymentMethodMaster.php");	

    $PaymentDetails = getPaymentMethodSettings(basename(__DIR__));	
     if($__Session->HasValue('_session_payment_request')) {
         $checkout_details = new Shopping_Cart_Data(true,false,false,true,false,true);
         $remark_value = "direct_payment";
     } else {
         $checkout_details = new Shopping_Cart_Data(true,false,false,true,false);
         $remark_value = "order";
     }
     $post_url   ="https://payment.ipay88.com.my/epayment/entry.asp";//for live
    if ($PaymentDetails['sel_payment_env'] == 1) {	     
       $amt = $checkout_details->payment_method_amount;
       $total_amount	   = number_format($amt,2,'.',',');
       $amount			   = str_replace(".","",$total_amount);
       $amount			   = str_replace(",","",$amount);      
    } else {
    	$amount	   = 1;
    	$total_amount	   = '1';
    }
	 
	//dopayment($checkout_details);   
    $orderid = $RRequest->request('order_id');
    $refno			= $orderid;
    $payment_method_id = $RRequest->request('payment_method_id');
    $merchantID		= $PaymentDetails['txt_merchant_code'];
    $vkey			= $PaymentDetails['txt_merchant_key'];
    $currency_code	= $PaymentDetails['txt_currency_code'];
	//$paypal_email 	= $PaymentDetails['txt_business_email']; //user email address
    $item_name		= (empty($PaymentDetails['txt_item_name']))?'Onprintshop':$PaymentDetails['txt_item_name'];
    $name			= SES_USER_FIRSTNAME.' '.SES_USER_LASTNAME;		// login user name
    $email			= getAccessObj()->getUserEmail();								// login user Email
    $phone_number	= $checkout_details->customer_address["phone"];;
    $return_url     	= show_page_link("payment/ipay88/response.php?pact_res=return&add=success&RefNo=".$orderid,true);
    $backend_url     	= show_page_link("payment/ipay88/notification.php",true);
	$signature_method 	= $vkey.$merchantID.$refno.$amount.$currency_code;
	$signature 			= base64_encode(hex2binI(sha1($signature_method)));
	
	function hex2binI($hexSource) {
		for ($i=0;$i<strlen($hexSource);$i=$i+2) {
			$bin .= chr(hexdec(substr($hexSource,$i,2)));
		}
		return $bin;
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></title>
	<link href="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_CSS;?>theme.css" rel="stylesheet" type="text/css" media="screen" />
<?php 
$arr_javascript_exclude[] = 'qm.js';
$arr_javascript_exclude[] = 'qm_item_bullets.js';
//addCSSFile($arr_css_include,$arr_css_exclude);
//addJavaScriptFile($arr_javascript_include,$arr_javascript_exclude); 
?>
<script type="text/javascript">
var loopTime=setInterval("currentTime()",500);
</script>
<style type="text/css" >
#contentContainer {
	background:none;
}
.div_img_loader {
	/*color:#df6c1b;*/
	float:left;
	font-size:72px;
	text-align:left;
	width:25%;
	margin:-12px 0px 0px 0px;
}
.btn_pay_style{
	/*background:#df6c1b;*/
	color:#FFFFFF;
	font-size:16px;
	height:48px;
	padding:3px 5px 3px 5px;
}
.wait_img_loading {
	height:300px;
}
.wait_img_loading .img_load {
	float:none;
	padding:72px 0 10px 0;
	text-align:center;
	line-height:28px;
	font-size:15px;	
	/*font-weight:bold;*/
}
.wait_img_loading .img_load .load_message {
	text-align:right;
	line-height:28px;
	font-size:34px;
	font-weight:bold;
	/*color:#df6c1b;*/
	width:66%;
	text-align:center;
	float:left;
	padding-left:90px;
}
.wait_img_loading .img_load .click_message {
	text-align:center;
	line-height:28px;
	font-size:16px;
}
</style>
<script type="text/javascript">
<!-- Begin
/* This script and many more are available free online at
The JavaScript Source!! http://javascript.internet.com
Created by: Abraham Joffe :: http://www.abrahamjoffe.com.au/ */

var startTime=new Date();
var strDisplay = '.';
var repeat = 5;
var start = 0;
function currentTime(){
  var a=Math.floor((new Date()-startTime)/100)/10;
  if(start != repeat ) {
  	document.getElementById("div_repeat_info").innerHTML = document.getElementById("div_repeat_info").innerHTML + strDisplay;
  	start = start + 1;
  } else {
  	document.getElementById("div_repeat_info").innerHTML = strDisplay;
  	start = 0;
  }
  /*if (a%1==0) a+=".0";
  document.getElementById("div_img_loader").innerHTML=a;*/
}

window.onload=function(){
  clearTimeout(loopTime);
  strDisplay = document.getElementById("div_repeat_info").innerHTML
}

// End -->
</script>
</head>
<body>
<form action="<?php echo $post_url; ?>" method="post" name="payment">

<div id="container">
	<div id="header">
		<div id="headerleft">
		<!-- Header Left -->
			<div class="logo">
				<!--<span><a href="<?php echo show_page_link(FILE_INDEX);?>"><img src="<? echo DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES; ?>logo.gif" /></a></span>-->
				<span><a href="<?php echo show_page_link(FILE_INDEX);?>"><img src="<? echo SITE_LOGO_PATH; ?>" /></a></span>
			</div>
		</div>
	</div>
	<!-- main contian End -->
	<div id="contentContainer" class="wait_img_loading">
		<div class="img_load">
			<div class="page-section-header load_message"><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></div><div id="div_repeat_info" class="page-section-header div_img_loader">.</div>
			<div style="float:left;padding:20px 0 7px 7px;text-align:left;width:100%;text-align:center;"><?php echo LABEL_PAGE_LOAD_PROBLEM ;?></div>
			<div style="float:left;padding:8px 0 7px 7px;width:100%;"><?php if(file_exists(DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES.'img_click_to_pay.gif')) { ?>
				<label class="btn btn-primary btn_left_co"><input type="submit" class="btn btn-primary btn_pay_style" name="btnpaysubmit" src="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES; ?>img_click_to_pay.gif" title="Click Here To Pay" value="<?php echo LEBEL_CLICK_HERE_TO_PAY;?>" alt="Click Here To Pay"></label>
			<?php } else { ?>
				<label class="btn btn-primary btn_left_co"><input type="submit" class="btn btn-primary btn_pay_style" name="btnpaysubmit" title="Click Here To Pay" value="<?php echo LEBEL_CLICK_HERE_TO_PAY;?>" ></label>
			<?php } ?></div>
		</div>
		
		<input type="hidden" name=MerchantCode value="<?php echo $merchantID;?>">
		<input type="hidden" name="PaymentId" value="">  <!--Refer to Appendix I.pdf file for MYR gateway-->
		<input type="hidden" name="RefNo" value="<?php echo $refno;?>">
		<input type="hidden" name="Amount" value="<?php echo $total_amount; ?>">
		<input type="hidden" name="Currency" value="<?php echo $currency_code;?>">
		<input type="hidden" name="ProdDesc" value="<?php echo $item_name; ?>">
		<input type="hidden" name="UserName" value="<?php echo $name;?>">
		<input type="hidden" name="UserEmail" value="<?php echo $email;?>">
		<input type="hidden" name="UserContact" value="<?php echo $phone_number; ?>">
		<input type="hidden" name="Remark" value="<?php echo $remark_value; ?>">
		<input type="hidden" name="Signature" value='<?php echo $signature?>'>  <!--// create this url given by doc -> http://www.mobile88.com/epayment/testing/TestSignature.asp-->
		<INPUT type="hidden" name="Lang" value="UTF-8">
		<input type="hidden" name="ResponseURL" value="<?php echo $return_url;?>"> 
		<input type="hidden" name="BackendURL" value="<?php echo $backend_url;?>">
		<!--<INPUT type="submit" value="Proceed with Payment" name="Submit">-->
		
</FORM>

<script language="javascript">
	document.payment.submit();
</script>