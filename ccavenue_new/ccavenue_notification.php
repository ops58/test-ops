<?php

	/**
	 * This file is check dynamic event notification sent from ccavenue.
	 * Merchant has to configure url in his merchant account -> Dynamic Event Notification -> Order Status ECHO URL.
	 *
	 * @author     Radixweb <team.radixweb@gmail.com>
	 * @copyright  Copyright (c) 2008, Radixweb
	 * @version    1.0
	 * @since      1.0
	 */
	require_once("../../lib/common.php");
	require_once("ccavenue_function.php");
	require_once(DIR_WS_PAYMENT."/payment.php");
	require_once(DIR_WS_MODEL."/OrderMaster.php");	
    require_once(DIR_WS_MODEL."PaymentDetailsMaster.php");
    require_once(DIR_WS_MODEL."PaymentRequestMaster.php");
    require_once(DIR_WS_MODEL . "PaymentMethodMaster.php");
	$ObjPaymentMethodMaster = new PaymentMethodMaster();
	/**
	 * Object Declaration
	 */
	$OrderMasterObj   		  = new OrderMaster();
    $ObjPaymentDetailsMaster = new PaymentDetailsMaster();
    $ObjPaymentRequest		= new PaymentRequestMaster();

    $orderNumber = $RRequest->request('order_id');
    $paymentReqst = false;

    $ObjPaymentMethodMaster->setSelect(array("count(*) as cnt "));
    $ObjPaymentMethodMaster->setWhere("AND  directory = :directory", 'ccavenue_new', 'string');
    $ObjPaymentMethodMaster->setWhere(" AND status = :status" ,'1','string');
    $DataDomainPayment = $ObjPaymentMethodMaster->getPaymentMethod();
    $totalActivemethod = $DataDomainPayment[0]->cnt;

    if($totalActivemethod > 1) {
        if(strpos($orderNumber, 'PR_') !== false) {
            $paymentReqst = true;
            $orderNumber = end(explode("_",$orderNumber));
        } 
        if($paymentReqst == false) {
            $ObjPaymentDetailsMaster->setSelect("payment_method");
            $ObjPaymentDetailsMaster->setWhere("AND orders_id = :Orders_Id", $orderNumber, 'int');
            $payment_details = $ObjPaymentDetailsMaster->getPaymentDetails();
            $OrderPayment = $payment_details[0];
            $pmid = $OrderPayment['payment_method'];
        }  else {
            $ObjPaymentRequest->setSelect(array("payment_request_details.payment_method_id"));
            $ObjPaymentRequest->setWhere("AND  payment_request_id = :payment_request_id", $orderNumber, 'int');
            $chk_order_request = $ObjPaymentRequest->getPaymentRequest();
            $pmid = $chk_order_request[0]->payment_method_id;
        }
    }
    $paymentMethodId = (isset($pmid) && $pmid != "") ? $pmid : basename(__DIR__);

	// Get Payment Method Details
	$PaymentDetails = getPaymentMethodSettings($paymentMethodId);
	
	$res_working_key 	= 	$PaymentDetails["working_key"]; //put in the 32 bit working key in the quotes provided here
    $encResponse = $RRequest->post("encResp"); //This is the response sent by the CCAvenue Server
	$rcvdString			=	decrypt($encResponse,$res_working_key);		//Crypto Decryption used as per the specified working key.
	$order_status		=	"";
	$decryptValues		=	explode('&', $rcvdString);
	$dataSize=sizeof($decryptValues);
	
	for($i = 0; $i < $dataSize; $i++) 
	{
		$information=explode('=',$decryptValues[$i]);
		switch ($i){
			case 0:
				$res_order_id = $information[1];
				break;
			
			case 1:
				$transaction_id = $information[1];
				break;
		
			case 3:
				$order_status=$information[1];
				break;
			
			case 10:
				$res_amount = $information[1];
				break;
			
			case 26:
			    $custom_val = $information[1];
			    break;
		}
	}
	
	$mygate_details 	=   serialize($decryptValues);	
	
	//If Response is true then add/Update the Entry else give the error message
	if($custom_val != 'Order') //Partial payment request response handling
	{
        $orderNumber = str_replace("PR_","",$res_order_id);
        
		if(strtoupper($order_status) === "SUCCESS")
				process_payment_request($sucess=true,false,$orderNumber,$transaction_id,serialize($decryptValues),false);
		else
				process_payment_request(false,$fail=true,$orderNumber,'',serialize($decryptValues),false);
	}
	else {
    	$OrderMasterObj->setWhere("AND order_id_type = :order_id_type", $res_order_id, 'string');
    	$actualo_id = $OrderMasterObj->getOrder();
    	
    	$insertedOrdId = $actualo_id[0]['orders_id'];
    	
    	if(strtoupper($order_status) === "SUCCESS"){
    		$transaction_id = $transaction_id;
    		$remark = 'Success';
    		
    		updatepaymentdetails($insertedOrdId,$success=true,$transaction_id,$mygate_details,$PaymentDetails,$remark,true);
    	} else {
    		updatepaymentdetails($insertedOrdId,false,null,$mygate_details,$PaymentDetails,null,true);
    	}
	}