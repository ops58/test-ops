<?php

	/**
	 * This file is check the paypal response.
	 *
	 * @author     Radixweb <team.radixweb@gmail.com>
	 * @copyright  Copyright (c) 2008, Radixweb
	 * @version    1.0
	 * @since      1.0
	 */
	require_once("../../lib/common.php");
	require_once("ccavenue_function.php");
	require_once(DIR_WS_PAYMENT."/payment.php");
	require_once(DIR_WS_MODEL."/OrderMaster.php");	

	/**
	 * Object Declaration
	 */
	$OrderMasterObj   		  = new OrderMaster();
	
	/*$fp = fopen(__DIR__."/payment.txt", 'a');
	$current ='';
	$current .='====================status---callback===========================';
	$current .=print_r($_REQUEST,true);
	$current .='====================status===========================';
	fwrite($fp, $current);
	fclose($fp);*/
	
	// Get Payment Method Details
    $paymentMethodId = $RRequest->request('pmid');
    $paymentMethodId = (isset($paymentMethodId) && $paymentMethodId!="")?$paymentMethodId:basename(__DIR__);
	$PaymentDetails = getPaymentMethodSettings($paymentMethodId);
	
	$res_working_key 	= 	$PaymentDetails["working_key"]; //put in the 32 bit working key in the quotes provided here
    $encResponse = $RRequest->post("encResp"); //This is the response sent by the CCAvenue Server
	$rcvdString			=	decrypt($encResponse,$res_working_key);		//Crypto Decryption used as per the specified working key.
	$order_status		=	"";
	$decryptValues		=	explode('&', $rcvdString);
	$dataSize=sizeof($decryptValues);
	
	for($i = 0; $i < $dataSize; $i++) 
	{
		$information=explode('=',$decryptValues[$i]);
		switch ($i){
			case 0:
				$res_order_id = $information[1];
				break;
			
			case 1:
				$transaction_id = $information[1];
				break;
		
			case 3:
				$order_status=$information[1];
				break;
			
			case 10:
				$res_amount = $information[1];
				break;
			
			case 26:
			    $custom_val = $information[1];
			    break;
		}
	}
	
	$mygate_details 	=   serialize($decryptValues);
	//If Response is true then add/Update the Entry else give the error message
	if($custom_val != 'Order') //Partial payment request response handling
	{
        $orderNumber = str_replace("PR_","",$res_order_id);
        
		if(strtoupper($order_status) === "SUCCESS")
				process_payment_request($sucess=true,false,$orderNumber,$transaction_id,serialize($decryptValues));
		else
				process_payment_request(false,$fail=true,$orderNumber,'',serialize($decryptValues));
	}
	else {
    	$OrderMasterObj->setWhere("AND order_id_type = :order_id_type", $res_order_id, 'string');
    	$actualo_id = $OrderMasterObj->getOrder();
    	
    	$insertedOrdId = $actualo_id[0]['orders_id'];
    	if(strtoupper($order_status) === "SUCCESS"){
    		$transaction_id = $transaction_id;
    		$remark = 'Success';
    		updatepaymentdetails($insertedOrdId,$success=true,$transaction_id,$mygate_details,$PaymentDetails,$remark);
    	} else {
    		updatepaymentdetails($insertedOrdId,false,null,$mygate_details,$PaymentDetails,null);
    	}
	}