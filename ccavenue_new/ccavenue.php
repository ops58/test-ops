<?php
	/**
	 * File Inclusion
	 */
	require_once("../../lib/common.php");
	require_once("ccavenue_function.php");
	require_once(DIR_WS_PAYMENT."/payment.php");
	require_once(DIR_WS_LIB.'cart_class.php');	
	
	//Get the Data 
	if($__Session->HasValue('_session_payment_request')) {
		$checkout_details = new Shopping_Cart_Data(true,true,true,true,false,true);
	} else {
		$checkout_details        = new Shopping_Cart_Data(true,true,true,true,false);
	}
	
	$amt = $checkout_details->payment_method_amount;	
    $paymentMethodId = $RRequest->request('payment_method_id');
    $payment_method_id = (isset($paymentMethodId) && $paymentMethodId!="")?$paymentMethodId:basename(__DIR__);
	$PaymentDetails = getPaymentMethodSettings($payment_method_id);
	
	//Set the variable
	if ($PaymentDetails['sel_payment_env'] == 0) {
	    $request_url   	=	"https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction"; //for test
	}else{
		$request_url   	=	"https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction";//for live
	}	
	
	if($__Session->HasValue('_session_payment_request')){
			$order_id		=  	"PR_".$RRequest->request('order_id');
			$custom_val = 'Direct Payment Request';
	} else {
			$order_id		=  	$RRequest->request('order_id');
			$ordertype=eval("return ".ORDER_ID_TYPE.";");
			$order_id = displayOrderNumber($order_id,$ordertype);
			$custom_val = 'Order';
	}
	$price			=  	number_format($amt, 2, '.', '');
	
	$merchant_Id	=  	$PaymentDetails["merchant_id"];
	$working_key 	=	$PaymentDetails["working_key"];
	$access_code 	=	$PaymentDetails["access_code"];
    $currency = $RRequest->request('ccode');
	
    $redirect_url	=	show_page_link("payment/ccavenue_new/ccavenue_response.php?pmid=".$payment_method_id);
    $redirect_url = update_query_string( $redirect_url );	
    $cancel_url		=	show_page_link("payment/ccavenue_new/ccavenue_response.php?pmid=".$payment_method_id);	
    $cancel_url = update_query_string( $cancel_url );	
	
    $Merchant_Param	=	'';
    
	
	/**
	 * Get the Shipping & Billing Details
	 */
	$billingcustomername = $checkout_details->billing_address['firstname'] . " " . $checkout_details->billing_address['lastname'];
	$billingstreetaddress = $checkout_details->billing_address['street_address'] . " " . $checkout_details->billing_address['suburb'];			
	$shippingcustomername = $checkout_details->shipping_address['firstname'] . " " . $checkout_details->shipping_address['lastname'];
	$shippingstreetaddress = $checkout_details->shipping_address['street_address'] . " " . $checkout_details->shipping_address['suburb'];
	$billingcustomername = str_replace(array('(',')','.'),array('','',''),$billingcustomername); //circleone store need
    $shippingcustomername = str_replace(array('(',')','.'),array('','',''),$shippingcustomername);//circleone store need
    
    function CheckValid($valid,$type = 'string') {
        $pattern = ($type != 'string') ? '/[^a-zA-Z0-9]/' : '/[^A-Za-z ]/';
        return  trim(preg_replace($pattern, '', $valid));
    }

    $reqParam = array();
    $reqParam['merchant_id'] = $merchant_Id;
    $reqParam['order_id'] = $order_id;
    $reqParam['amount'] = $price;
    $reqParam['currency'] = $currency;
    $reqParam['redirect_url'] = $redirect_url;
    $reqParam['cancel_url'] = $cancel_url;
    $reqParam['language'] = "EN";
    $reqParam['billing_name'] = CheckValid($billingcustomername);
    $reqParam['billing_address'] = $billingstreetaddress;
    $reqParam['billing_city'] = CheckValid($checkout_details->billing_address["city"]);
    $reqParam['billing_state'] = $checkout_details->billing_address["state"];
    $reqParam['billing_zip'] = CheckValid($checkout_details->billing_address["postcode"],'number');
    $reqParam['billing_country'] = $checkout_details->billing_address['countryname'];
    $reqParam['billing_tel'] = $checkout_details->billing_address['phone'];
    $reqParam['billing_email'] = $user_details['email'];
    $reqParam['delivery_name'] = CheckValid($shippingcustomername);
    $reqParam['delivery_address'] = $shippingstreetaddress;
    $reqParam['delivery_city'] = CheckValid($checkout_details->shipping_address['city']);
    $reqParam['delivery_state'] = $checkout_details->shipping_address['state'];
    $reqParam['delivery_zip'] = CheckValid($checkout_details->shipping_address['postcode'],'number');
    $reqParam['delivery_country'] = $checkout_details->shipping_address['countryname'];
    $reqParam['delivery_tel'] = $checkout_details->shipping_address['phone'];
    $reqParam['merchant_param1'] = $custom_val;
    $reqParam['merchant_param2'] = '';
    $reqParam['merchant_param3'] = '';
    $reqParam['merchant_param4'] = '';
    $reqParam['merchant_param5'] = '';
    $reqParam['promo_code'] = '';
    $reqParam['customer_identifier'] = '';
	
	if(isset($reqParam)  && !empty($reqParam)) {
		foreach ($reqParam as $key => $value){
			$merchant_data.=$key.'='.$value.'&';
		}
		$encrypted_data=encrypt($merchant_data,$working_key); // Method for encrypting the data.	
	}	
			
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head></head>
<body>
<form method="post" name="redirect" action="<?php echo $request_url;?>"> 
    <input type=hidden name=encRequest value="<?php echo $encrypted_data;?>">
    <input type=hidden name=access_code value="<?php echo $access_code;?>">
</form>
<script language='javascript'>document.redirect.submit();</script>
</body>
</html>