<?php
define_const('PAYMENT_COMMON_LBL_ACCESS_CODE', 'Access Code');
/*Help messages */
$help_messages1= array (
'HELP_PAYMENT_CCAVENUE_ACCESS_CODE' => 'Access Code is assigned to merchant.',
);
$help_messages =array_merge($help_messages,$help_messages1);

?>
<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">	
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_ENVIRONMENT; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['sel_payment_env'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['sel_payment_env'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   
		   ?>
		   <span><input type="radio" id="sel_payment_env" name="sel_payment_env" value="1" <?php echo $lchecked;?>><?php echo PAYMENT_LBL_LIVE_ENV; ?></span>
		   <span class="radio_distance"><input type="radio" id="sel_payment_env" name="sel_payment_env" value="0" <?php echo $tchecked;?>><?php echo PAYMENT_LBL_TESTING_ENV; ?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_CCAVENUE_ENVIRONMENT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo PAYMENT_COMMON_LBL_MERCHANT_ID; ?> : </td>
		<td><input type="text" name="merchant_id" id="merchant_id" value="<?php echo $PaymentDetails['merchant_id'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_CCAVENUE_MERCHANT_ID'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption" align="left"><?php echo PAYMENT_COMMON_LBL_WORKING_COPY; ?> : </td>
		<td align="left"><input type="text" name="working_key" id="working_key" value="<?php echo $PaymentDetails['working_key'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_CCAVENUE_WORKING_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption" align="left"><?php echo PAYMENT_COMMON_LBL_ACCESS_CODE; ?> : </td>
		<td align="left"><input type="text" name="access_code" id="access_code" value="<?php echo $PaymentDetails['access_code'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_CCAVENUE_ACCESS_CODE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
</table>