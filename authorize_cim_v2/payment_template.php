<?php 
/**
 * 	Authorize.Net CIM (Customer Information Manager) Payment.
 *  Revision 8 
 * */
$action = getRequestObj()->request('action');
if(!$delete_saved_card){
if($action == 'delete'){
	require_once("../../lib/common.php");
	require_once ("languages/" . DIR_CURRENT_LANGUAGE.'.php');
}else {
	require_once("lib/common.php");
	require_once(DIR_WS_PAYMENT."authorize_cim_v2/languages/" . DIR_CURRENT_LANGUAGE.'.php');
}
}
require_once(DIR_WS_PAYMENT."/payment.php");
require_once(DIR_WS_MODEL . "PaymentMethodMaster.php");
require_once(DIR_WS_LIB.'cart_class.php');
require_once(DIR_WS_PAYMENT.'authorize_lib/vendor/autoload.php');
require_once(DIR_WS_MODEL."/AddressMaster.php");

$UtilMasterObj    = new UtilMaster();
// Object Declaration
$ObjSitePaymentMethodMaster = new PaymentMethodMaster();
/* $checkout_details        	= new Shopping_Cart_Data(true,true,true,true,false);
$Payment_Checkout 			= $checkout_details->ship_pay_details; */
$AuthnetCIMObj				= new AuthorizeNetCIM;
$deleteMessage 				= AUTHNET_CIM_DELETE_CARD;
$address_param = (FILE_FILENAME_WITH_EXT == FILE_DIRECT_PAYMENT) ? 'payreq=1&prid='.initRequestValue('RequestId') : 'cart=1';
$userID = getAccessObj()->getUserID();
// Get Payment Method Details
$ObjSitePaymentMethodMaster->setSelect(array("payment_method_id","payment_details"))
                           ->setWhere("AND directory = :directory","authorize_cim_v2","string");
$DataDomainPayment = $ObjSitePaymentMethodMaster->getPaymentMethod();
if ($DataDomainPayment) {
	$paymentMethodID = $DataDomainPayment[0]['payment_method_id'];
	$PaymentDetails  = unserialize($DataDomainPayment[0]['payment_details']);
}

if($PaymentDetails['sel_payment_env'] == "1"){
	define('AUTHORIZENET_SANDBOX', false);
}else{
	define('AUTHORIZENET_SANDBOX', true);
}
	
if(getRequestObj()->request('action') == 'delete'){
	$uid = getRequestObj()->request('id');
	$result = "failure";
	
	$UtilMasterObj->setFrom('authorize_cim_child')
	              ->setJoin("LEFT JOIN authorize_cim_master ON authorize_cim_child.userid = authorize_cim_master.userid ")
	              ->setWhere("authorize_cim_child.auto_id  = :auto_id",$uid,"int");
	$data = $UtilMasterObj->exec_query();
	$data = $data[0];
	$customerProfileId = $data['customer_profile_id']; 
	$paymentProfileId = $data['payment_profile_id'];
	
	// Merchant account Login Credentials
	$apiLoginId		= $PaymentDetails['txt_api_login'];
	$transactionKey	= $PaymentDetails['txt_transaction_key'];
	define_const('AUTHORIZENET_API_LOGIN_ID', $apiLoginId);
	define_const('AUTHORIZENET_TRANSACTION_KEY', $transactionKey);
	$authNetCIMObj = new AuthorizeNetCIM;
	$response = $authNetCIMObj->deleteCustomerPaymentProfile($customerProfileId, $paymentProfileId);
	$errorCode	= $response->xml->messages->message->code;
	if($errorCode == 'I00001'){
		$delete_query = "DELETE FROM authorize_cim_child WHERE auto_id=".$uid;
		$UtilMasterObj->exec_query($delete_query);
		$result = "success";
	} else {
		// For debugging purpose
		$current = "--CustID:".$userID."|Error:".$errorCode."|CustomerProfileID:".$customerProfileId."|PaymentProfileId:".$paymentProfileId."|Page:checkout-payment_template.php|".date('Y-m-d H:i:s')."\n";
		$fp = fopen(DIR_WS_PAYMENT."authorize_cim_v2/payment.log", 'a');
		fwrite($fp, $current);
		fclose($fp);
	}
	/* if($delete_saved_card == true){
	    show_page_header(FILE_AUTHRIZE_SAVED_CARDS.'?delete='.$result,true);
	}
	 */
	echo json_encode(array("result"=>$result));
	exit();
}
global $__Session;
/* if($__Session->HasValue('_session_address_details')) {
    $checkout_details_arr = $__Session->GetValue('_session_address_details');
    $user_default_address = $checkout_details_arr['bill_address_id'];		// Shipping Address
} */
// Creating table in database (if not there) to store the profileID and its related paymentProfileId
$table_query ='CREATE TABLE IF NOT EXISTS `authorize_cim_master` (
			  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
			  `userid` int(11) NOT NULL,
			  `customer_profile_id` varchar(255) NOT NULL,
			  PRIMARY KEY (`auto_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';
$UtilMasterObj->setCustomQuery($table_query);
$UtilMasterObj->exec_query();
$table_query_2 = 'CREATE TABLE IF NOT EXISTS `authorize_cim_child` (
			  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
			  `userid` int(11) NOT NULL,
			  `user_card_number` varchar(255) NOT NULL,
			  `payment_profile_id` varchar(255) NOT NULL,
              `address_book_id` int(11) NOT NULL,
			  PRIMARY KEY (`auto_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';

$UtilMasterObj->setCustomQuery($table_query_2);
$UtilMasterObj->exec_query();

$UtilMasterObj->setFrom('authorize_cim_child')->setWhere("AND userid = :userid",$userID,"int");
$data_array = $UtilMasterObj->exec_query();

if($__Session->HasValue('_session_checkout_details')) {
    $checkout_details_arr 	= $__Session->GetValue('_session_checkout_details');
    $auth_cim_selectd   = $checkout_details_arr['post_data']['payment_type'];
}
if(empty($auth_cim_selectd)){
    $auth_cim_selectd = 'new';
}
$sitepaymentid=$data['sitepaymentid']; ?>
<div class="row">
<?php 
if($_GET['status']=='failure'/*  && $data['error_status_message']==true */) {
	$status_message = AUTHNET_CIM_ERROR_MESSAGE;
	if(!empty(getRequestObj()->request('message'))) {
		$status_message = getRequestObj()->request('message');
	}    
?>
	<div class="text-danger text-center col-sm-12"><?php echo $status_message; ?></div>
<?php } ?>
	<div class="col-12" >
		<div class="calc_loading d-none">
			<div class="loading-container-sm">
				<div class="loading"></div><div id="loading-text"><?php echo COMMON_WAIT;?></div>
			</div>
		</div>
		
		 
		 <div class="form-row mb-3">
			<div class="position-relative">
				<input type="hidden" name="payment_option" id="payuoc_payment_option" value="" />
				<input type="hidden" name="payment_vault_id" id="payment_vault_id" value="<?php echo $data_array[0]['auto_id']; ?>" />
				<?php
				if(!empty($data_array)){ 
    				foreach ($data_array as $key=>$value){  ?>
    					<div class="<?php echo $value['auto_id'];?> custom-control custom-radio pr-3 m-3">
    					
    					<?php $data_label = AUTHNET_CIM_PAY_WITH.' <strong>'.$value['user_card_number'].'</strong>&nbsp;&nbsp;'; ?>
    						<input type="radio" name="payment_type" id="existing_<?php echo $sitepaymentid."_".$value['auto_id']; ?>" class="<?php echo $value['auto_id'];?> existing custom-control-input" <?php if($value['auto_id'] == $auth_cim_selectd){echo 'checked="checked"';}?> value="<?php echo $value['auto_id']; ?>"  style="margin-left:20px;"/>
    						<label for="existing_<?php echo $sitepaymentid."_".$value['auto_id']; ?>" class="custom-control-label"><?php echo AUTHNET_CIM_PAY_WITH.' '.$value['user_card_number'];?><a href="javascript:void(0);" id="<?php echo $value['auto_id'];?>" data-add-id="<?php echo $value['address_book_id'];?>" class="remove ml-2" title="Remove this card"><i class="fa fa-trash"></i></a></label>
    					</div>
    				<?php }
				} ?>
				<div class="new custom-control custom-radio pr-3 m-3">
					<input type="radio" name="payment_type" id="new_<?php echo $sitepaymentid; ?>" class="new custom-control-input" value="new" style="margin-left:20px;" <?php if($auth_cim_selectd == 'new') echo 'checked';?>/>
					<label for="new_<?php echo $sitepaymentid; ?>" class="custom-control-label"><?php echo AUTHNET_CIM_PAY_NEW_DETAIL;?></label>
				</div>
 			</div>
		</div>
		 
		 
		 
		 <input type="hidden" name="payment_type_new" id="payment_type_new" />
		 
		 <div id="auth_form">	
		<div class="text-danger text-right mb-3 mr-3"><small><?php echo COMMON_REQUIRED_INFOMATION; ?></small></div>	
		<div class="form-row mb-3">
			<label for="firstName_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo AUTHNET_CIM_LABEL_FIRSTNAME;?></label>							
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<input type="text" name="auth_net_firstName" id="firstName_<?php echo $sitepaymentid; ?>" value="" maxlength="50" class=" form-control" placeholder="" data-rule-required="true" data-msg-required="<?php echo COMMON_FULLNAME_IS_REQUIRED; ?>" />        	        	
					<span class="text-danger" id="FNameError_<?php echo $sitepaymentid; ?>"></span>
					<span class="input-group-require text-danger pl-1"> * </span>
				</div>
			</div>
		</div>
		
		<div class="form-row mb-3">
			<label for="creditCardType_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo AUTHNET_CIM_LABEL_CARD_TYPE;?></label>
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<select class="select_width custom-select" name="auth_net_creditCardType" id="creditCardType_<?php echo $sitepaymentid; ?>" data-rule-required="true">
						<option value="visa" selected="selected" ><?php echo CREDITCARD_LABEL_VISACARD;?></option>
						<option value="mastercard" ><?php echo CREDITCARD_LABEL_MASTERCARD;?></option>
						<option value="discover" ><?php echo CREDITCARD_LABEL_DISCOVERCARD;?></option>
						<option value="amex" ><?php echo CREDITCARD_LABEL_AMERICANCARD;?></option>
					</select>										        	        	
					<span class="input-group-require text-danger pl-1"></span>
				</div>
			</div>
		</div>
		
		<div class="form-row mb-3">
			<label for="creditCardNumber_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo AUTHNET_CIM_LABEL_CARD_NUMBER;?></label>							
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<input type="text" name="auth_net_creditCardNumber" id="creditCardNumber_<?php echo $sitepaymentid; ?>" autocomplete="off" value="" maxlength="16" class=" form-control" placeholder="" data-rule-required="true" data-msg-required="<?php echo AUTHNET_CIM_CARD_NUMBER_IS_REQUIRED; ?>" data-msg-creditcardtypes="<?php echo AUTHNET_CIM_INVALID_CARD_NUMBER;?>" />        	        	
					<span class="text-danger" id="CNumberError_<?php echo $sitepaymentid; ?>"></span>
					<span class="input-group-require text-danger pl-1"> * </span>
				</div>
			</div>
		</div>
		<div class="form-row mb-3">
			<label for="expDateMonth_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo AUTHNET_CIM_LABEL_CARD_EXPIRED;?></label>
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<select class="select_width custom-select mr-2" name="auth_net_expDateMonth" id="expDateMonth_<?php echo $sitepaymentid; ?>">
						<option value="01" >01</option>
						<option value="02" >02</option>
						<option value="03" >03</option>
						<option value="04" >04</option>
						<option value="05" >05</option>
						<option value="06" >06</option>
						<option value="07" >07</option>
						<option value="08" >08</option>
						<option value="09" >09</option>
						<option value="10" >10</option>
						<option value="11" >11</option>
						<option value="12" >12</option>
					</select>
					<?php $current_year = date('Y'); ?>
					<select class="select_width custom-select" name="auth_net_expDateYear" id="expDateYear_<?php echo $sitepaymentid; ?>" data-msg-datecompare="<?php echo AUTHNET_CIM_INVALID_EXPIRY_DATE; ?>">
						<?php for($i=0;$i<10;$i++) { ?>
						<option value="<?php echo $current_year; ?>" ><?php echo $current_year; ?></option>
						<?php 	$current_year++; } ?>
					</select>
					<span class="text-danger" id="ExpireDateError_<?php echo $sitepaymentid; ?>"></span>
					<span class="input-group-require text-danger pl-1"></span>
				</div>	
			</div>
		</div>
		
		<div class="form-row mb-3">
			<label for="cvv2Number_<?php echo $sitepaymentid; ?>" class="control-label col-12 col-md-4"><?php echo AUTHNET_CIM_LABEL_CARD_VERIFY_NUMBER;?></label>							
			<div class="col-12 col-md-8">
				<div class="d-flex position-relative">
					<input type="text" name="auth_net_cvv2Number" id="cvv2Number_<?php echo $sitepaymentid; ?>" value="" maxlength="4" class=" form-control" placeholder="" data-rule-required="true" data-msg-required="<?php echo AUTHNET_CIM_VERIFICATION_NUMBER_IS_REQUIRED; ?>" data-rule-minlength="3" data-msg-minlength="<?php echo AUTHNET_CIM_INVALID_VERIFICATION_NUMBER; ?>" onkeypress="return NumericValidation(event);" />        	        	
					<span class="input-group-require text-danger pl-1"> * </span>
				</div>
			</div>
		</div>
		
		<div class="form-row mb-3">
	        <div class="d-flex col-sm-8 pull-right page-section-header">
                <div class="custom-control custom-checkbox">
    				<input type="checkbox" class="custom-control-input" id="save_this_card" name="save_this_card" />
    				<label class="custom-control-label" for="save_this_card">Do you want to save this card? </label>
				</div>
	        </div>
        </div>
		
	</div>
        </div>    
</div>
<script>
var userid = "<?php echo $userID; ?>";
var ADMIN_SHIP_BILL_ADDRESS_CHANGE = "<?php echo defined('ADMIN_SHIP_BILL_ADDRESS_CHANGE'); ?>";
var SITE_DIRECTORY = "<?php echo SITE_DIRECTORY; ?>";
var FILE_COUNTRY_STATES = "<?php echo FILE_COUNTRY_STATES; ?>";
var state_placeholder = "<?php echo COMMON_STATE; ?>";
var state_settings = "<?php echo $mandatory_settings['state_setting']; ?>";
var COMMON_STATE_IS_REQUIRED = "<?php echo COMMON_STATE_IS_REQUIRED; ?>";
</script>
<?php
$AUTH_CARD_DELETE_SUCCESS = AUTHNET_CIM_DELETE_CARD_SUCCESS;
$AUTH_CARD_DELETE_FAILURE = AUTHNET_CIM_DELETE_CARD_FAILURE;
?>
<?php
$JS = <<<JS
<script>
function show_hide_cim_payment(id,payId){
	if(id == 'new'){
	   $("#paymethod"+payId).find("#payment_option").val('');
	   $("#paymethod"+payId).find("#auth_form").show();
    }else{
		$("#paymethod"+payId).find("#payment_option").val('1');
		$("#paymethod"+payId).find("#auth_form").hide();
	}
	//$('#payment_type_new').val(id);
}
$('.remove').click(function(){
	if(confirm("$deleteMessage")){
	    var mid = $(this).attr('id');
	    $('.calc_loading').removeClass('d-none');
	    $.ajax({
			url : "/payment/authorize_cim_v2/payment_template.php?id="+mid+"&action=delete",
			dataType : "json",
			success : function(data){
				$('.calc_loading').addClass('d-none');
				if(data.result == "success"){
					$('.'+mid).remove();
					$('#payment_option').val(0);
					$("#new_$sitepaymentid").prop("checked", true);
					$('#auth_form').show();
					$.bootstrapGrowl("$AUTH_CARD_DELETE_SUCCESS",{type:'success', delay:'4000'});
				}else{
					$.bootstrapGrowl("$AUTH_CARD_DELETE_FAILURE",{type:'danger', delay:'4000'});
				}
			}
		});
	}
});
$("input[name='payment_type']").change(function(){
	if($(this).is(":checked")) {
		if($(this).val()=='new'){
			$("#auth_form").show();
		}else{
			$("#auth_form").hide();
		}
	}
});
//$("input[name='payment_type']").trigger('change');	

show_hide_cim_payment("$auth_cim_selectd","$sitepaymentid");
</script>
JS;
global $jscode;
$jscode .= $JS;
?>