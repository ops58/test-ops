<?php 
require_once(DIR_WS_PAYMENT."payment.php");
require_once(DIR_WS_LIB.'cart_class.php');
require_once (DIR_WS_PAYMENT.'/authorize_lib/vendor/autoload.php');

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

function authorize_cim_v2_process_refund($OrderDetails,$paymentdetails,$refund_data) {
    
    define("AUTHORIZENET_LOG_FILE", "phplog");
    $cardresponse = getTransactionDetails($OrderDetails['transactionid'],$paymentdetails);
    if(empty($cardresponse['error'])) {
        
        $Card4chars = substr($cardresponse['cardno'],4);
        $refund_response = refundTransaction($OrderDetails,$paymentdetails,$refund_data,$Card4chars);
        
        return $refund_response;
    } else {
        
        $refund_detail['orders_id'] = $OrderDetails['orders_id'];
        if(!empty($refund_data['orders_products_id']))
            $refund_detail['orders_products_id'] = $refund_data['orders_products_id'];
        $refund_detail['refund_amount'] = $refund_data['refund_amount'];
        $refund_detail['status'] = 'failure';
        $refund_detail['error'] = $cardresponse['error'];
        
        return $refund_detail;
    }
}

function getTransactionDetails($transactionId,$paymentdetails)
{
    /* Create a merchantAuthenticationType object with authentication details
     retrieved from the constants file */
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantAuthentication->setName($paymentdetails['txt_api_login']);
    $merchantAuthentication->setTransactionKey($paymentdetails['txt_transaction_key']);

    // Set the transaction's refId
    $refId = generateRandomNo(5);
    if ($paymentdetails['sel_payment_env'] == 1) {
        $post_url="https://api2.authorize.net";
    } else {
        $post_url="https://apitest.authorize.net";
    }
    
    $request = new AnetAPI\GetTransactionDetailsRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setTransId($transactionId);

    $controller = new AnetController\GetTransactionDetailsController($request);

    $response = $controller->executeWithApiResponse($post_url);

    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok"))
    {
        $card_response = array();
        $card_response['card_found'] = true;
        $card_response['cardno'] = $response->getTransaction()->getPayment()->getcreditCard()->getcardNumber();
    }
    else
    {
        $card_response = array();
        $card_response['card_found'] = false;
        $card_response['error'] = $response->getMessages()->getMessage();
    }
    return $card_response;
}

function refundTransaction($OrderDetails,$paymentdetails,$refund_data,$Card4chars)
{
    /* Create a merchantAuthenticationType object with authentication details
     retrieved from the constants file */
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantAuthentication->setName($paymentdetails['txt_api_login']);
    $merchantAuthentication->setTransactionKey($paymentdetails['txt_transaction_key']);

    // Set the transaction's refId
    $refId = generateRandomNo(5);

    // Create the payment data for a credit card
    $creditCard = new AnetAPI\CreditCardType();
    $creditCard->setCardNumber($Card4chars);
    $creditCard->setExpirationDate(XXXX);
    $paymentOne = new AnetAPI\PaymentType();
    $paymentOne->setCreditCard($creditCard);
    //create a transaction
    $transactionRequest = new AnetAPI\TransactionRequestType();
    $transactionRequest->setTransactionType( "refundTransaction");
    $transactionRequest->setAmount($refund_data['refund_amount']);
    $transactionRequest->setPayment($paymentOne);
    $transactionRequest->setRefTransId($OrderDetails['transactionid']);


    $request = new AnetAPI\CreateTransactionRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setTransactionRequest( $transactionRequest);
    $controller = new AnetController\CreateTransactionController($request);
    
    if ($paymentdetails['sel_payment_env'] == 1) {
        $post_url="https://api2.authorize.net";
    } else {
        $post_url="https://apitest.authorize.net";
    }
    
    $response = $controller->executeWithApiResponse($post_url);

    if ($response != null)
    {
        if($response->getMessages()->getResultCode() == "Ok")
        {
            $tresponse = $response->getTransactionResponse();

            if ($tresponse != null && $tresponse->getMessages() != null)
            {
                $refund_response1['response_code'] = $tresponse->getResponseCode();
                $refund_response1['transactionid'] = $tresponse->getTransId();
                $refund_response1['code'] = $tresponse->getMessages()[0]->getCode();
                $refund_response1['Description'] = $tresponse->getMessages()[0]->getDescription();
                $refund_response1['payment_method'] = $paymentdetails['payment_method_name'];
                
                $refund_detail['orders_id'] = $OrderDetails['orders_id'];
                if(!empty($refund_data['orders_products_id']))
                    $refund_detail['orders_products_id'] = $refund_data['orders_products_id'];
                $refund_detail['refund_amount'] = $refund_data['refund_amount'];
                $refund_detail['status'] = 'success';
                $refund_detail['refund_response'] = json_encode($refund_response1);
                $refund_detail['error'] = '';
            }
            else
            {
                if($tresponse->getErrors() != null)
                {
                    $refund_detail['orders_id'] = $OrderDetails['orders_id'];
                    if(!empty($refund_data['orders_products_id']))
                        $refund_detail['orders_products_id'] = $refund_data['orders_products_id'];
                    $refund_detail['refund_amount'] = $refund_data['refund_amount'];
                    $refund_detail['status'] = 'failure';
                    $refund_detail['error'] = $tresponse->getErrors()[0]->getErrorText();
                }
            }
        }
        else
        {
            $tresponse = $response->getTransactionResponse();
            if($tresponse != null && $tresponse->getErrors() != null)
            {
                $refund_detail['orders_id'] = $OrderDetails['orders_id'];
                if(!empty($refund_data['orders_products_id']))
                    $refund_detail['orders_products_id'] = $refund_data['orders_products_id'];
                $refund_detail['refund_amount'] = $refund_data['refund_amount'];
                $refund_detail['status'] = 'failure';
                $refund_detail['error'] = $tresponse->getErrors()[0]->getErrorText();
            }
            else
            {
                $refund_detail['orders_id'] = $OrderDetails['orders_id'];
                if(!empty($refund_data['orders_products_id']))
                    $refund_detail['orders_products_id'] = $refund_data['orders_products_id'];
                $refund_detail['refund_amount'] = $refund_data['refund_amount'];
                $refund_detail['status'] = 'failure';
                $refund_detail['error'] = $response->getMessages()->getMessage()[0]->getText();
            }
        }
    }
    else
    {
        $refund_detail['orders_id'] = $OrderDetails['orders_id'];
        if(!empty($refund_data['orders_products_id']))
            $refund_detail['orders_products_id'] = $refund_data['orders_products_id'];
        $refund_detail['refund_amount'] = $refund_data['refund_amount'];
        $refund_detail['status'] = 'failure';
        $refund_detail['error'] = "No response returned";
    }
    return $refund_detail;
}
?>
