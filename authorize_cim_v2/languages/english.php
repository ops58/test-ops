<?php 
define_const("AUTHORIZE_NET_CIM_LBL_CLIENT_ID","Client ID");
define_const("AUTHORIZE_NET_CIM_LBL_CLIENT_SECRET","Client Secret");
define_const('AUTHORIZE_API_LOGIN_ID','Login Id');
define_const('AUTHORIZE_TRANSACTION_KEY','Transaction Key');
define_const('AUTHORIZE_TRANSACTION_TYPE','Transaction Type');
define_const('AUTHORIZE_AUTH_CAPTURE','Authorize and Capture');
define_const('AUTHORIZE_AUTH_ONLY','Authorize');
define_const('AUTHNET_CIM_PAY_NEW_DETAIL',"Pay with new card");
define_const('AUTHNET_CIM_LABEL_FIRSTNAME','Name');
define_const('AUTHNET_CIM_LABEL_CARD_TYPE','Card Type');
define_const('AUTHNET_CIM_LABEL_CARD_NUMBER','Card Number');
define_const('AUTHNET_CIM_LABEL_CARD_EXPIRED','Expiration Date');
define_const('AUTHNET_CIM_LABEL_CARD_VERIFY_NUMBER','Verification Number');
define_const('AUTHNET_CIM_PAY_WITH','Pay with');
define_const('AUTHNET_CIM_DELETE_CARD_SUCCESS','Card has been deleted successfully');
define_const('AUTHNET_CIM_DELETE_CARD_FAILURE','Card Can not be deleted due to some error');
define_const('AUTHNET_CIM_ERROR_MESSAGE','Invalid Card Detail');
$help_messages1= array (
    'HELP_PAYMENT_AUTHORIZE_API_LOGIN_ID' => 'API Loin Id which is found under Account -> Api & Credentials',
    'HELP_PAYMENT_AUTHORIZE_TRANSACTION_KEY' => 'Transaction Key which is found under Account -> Api & Credentials',
    'HELP_TRANSACTION_TYPE'=>'Default value for this field is Authorize and Capture.<br>
    Authorize : The transaction will not be sent for settlement until the transaction is submitted for capture manually in the Merchant Interface.'
);
$help_messages =array_merge($help_messages,$help_messages1);
?>