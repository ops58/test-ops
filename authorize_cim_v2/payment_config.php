<?php
require(DIR_WS_PAYMENT."authorize_cim_v2/languages/" . DIR_CURRENT_LANGUAGE.'.php');
?>
<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">   
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_ENVIRONMENT; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['sel_payment_env'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['sel_payment_env'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   
		   ?>
		   <span><input type="radio" id="sel_payment_env" name="sel_payment_env" value="1" <?php echo $lchecked;?>><?php echo PAYMENT_LBL_LIVE_ENV; ?></span>
		   <span class="radio_distance"><input type="radio" id="sel_payment_env" name="sel_payment_env" value="0" <?php echo $tchecked;?>><?php echo PAYMENT_LBL_TESTING_ENV; ?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_PAYPAL_STANDARD_ENVIRONMENT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>		
	<tr>
		<td class="form_caption" align="left"><?php echo AUTHORIZE_API_LOGIN_ID; ?> : </td>
		<td><input type="text" name="txt_api_login" id="txt_client_id" value="<?php echo $PaymentDetails['txt_api_login'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_AUTHORIZE_API_LOGIN_ID'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo AUTHORIZE_TRANSACTION_KEY; ?> : </td>
		<td align="left"><input type="text" name="txt_transaction_key" id="txt_client_secret" value="<?php echo $PaymentDetails['txt_transaction_key'];?>" class="text_box_std" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_AUTHORIZE_TRANSACTION_KEY'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td align="left" class="form_caption"><?php echo AUTHORIZE_TRANSACTION_TYPE; ?> : </td>
		<td align="left">
			<select name="txt_transaction_type" id="txt_transaction_type" class="select_std">			
				<option value="<?php echo 'AuthCapture'; ?>" <?php if($PaymentDetails['txt_transaction_type'] == 'AuthCapture') { echo 'selected'; } ?>><?php echo AUTHORIZE_AUTH_CAPTURE; ?></option>
				<option value="<?php echo 'AuthOnly'; ?>" <?php if($PaymentDetails['txt_transaction_type'] == 'AuthOnly') { echo 'selected'; } ?>><?php echo AUTHORIZE_AUTH_ONLY; ?></option>
			</select>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_TRANSACTION_TYPE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
</table>
