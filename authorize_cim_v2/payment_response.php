<?php
require_once(DIR_WS_PAYMENT.'authorize_lib/vendor/autoload.php');
require_once(DIR_WS_MODEL . "UtilMaster.php");
require_once(DIR_WS_MODEL . "AddressMaster.php");
require_once 'autoload.php';
$UtilMasterObj    = new UtilMaster();
$ObjAddressMaster = new AddressMaster();
// Selecting Payment Environment.
if ($PaymentDetails['sel_payment_env'] == 1) {
	define_const('AUTHORIZENET_SANDBOX', false);
} else {
	define_const('AUTHORIZENET_SANDBOX', true);
}
// Merchant account Login Credentials 
$apiLoginId		= $PaymentDetails['txt_api_login'];
$transactionKey	= $PaymentDetails['txt_transaction_key'];

if($apiLoginId != '' && $transactionKey != ''){
	define_const('AUTHORIZENET_API_LOGIN_ID', $apiLoginId);
	define_const('AUTHORIZENET_TRANSACTION_KEY', $transactionKey);
}

$authNetCIMObj 		= new AuthorizeNetCIM;
$user_id  			= getAccessObj()->getUserID();
$sitename			= SITE_URL;
$userMail 			= getAccessObj()->getUserEmail();
$BillingAddressId   = $checkout_details->billing_address['address_book_id'];
$authPaymentMethod 	= (!empty($PaymentDetails['txt_transaction_type'])?$PaymentDetails['txt_transaction_type']:"AuthCapture"); //$PaymentDetails['pay_action'];
$payment_option_id 	= $checkout_details->ship_pay_details['payment_vault_id'];
$payment_option 	= $checkout_details->ship_pay_details['payment_option'];
//$payment_type	 	= $checkout_details->ship_pay_details['payment_type_new'];
$payment_type	 	= $checkout_details->ship_pay_details['payment_type'];
$cardNumber			= $checkout_details->ship_pay_details['auth_net_creditCardNumber'];
$cardCode			= $checkout_details->ship_pay_details['auth_net_cvv2Number'];
$expMonth 			= $checkout_details->ship_pay_details['auth_net_expDateMonth'];
$expYear 			= $checkout_details->ship_pay_details['auth_net_expDateYear'];
$save_this_card     = $checkout_details->ship_pay_details['save_this_card'];
$bill_address_auth  = $BillingAddressId;
$expirationDate 	= $expYear.'-'.$expMonth;
$card_action        = $checkout_details->ship_pay_details['card_action'];

/*
* Check whether customer profile ID exists in the system or not
* If not then set flag true to create one.
* Else use the existing profile fetched from the system.
*/
$UtilMasterObj->setFrom('authorize_cim_master')->setWhere("AND userid = :userid",$user_id,"int");
$userIdExists = $UtilMasterObj->exec_query();
$count = 0;
$createAuthProfile = true;
if ($userIdExists) {
    $createAuthProfile 	= false;
    $userData 			= $userIdExists[0];
    $customerProfileId 	= $userData['customer_profile_id'];
    
    $UtilMasterObj->setSelect(' COUNT(*) AS count')
                  ->setFrom('authorize_cim_child')
                  ->setWhere("AND userid = :userid",$user_id,"int");
    
    $authChild = $UtilMasterObj->exec_query();
    $authChild = $authChild[0];
    $count = $authChild['count'];
}


$description	= "$sitename -- Retail";
$customerType	= 'individual';
if(getAccessObj()->getUserType() == '2'){
	$description	= "$sitename -- Coroprate";
	$customerType	= 'business';
}

// Add customer profile
if($createAuthProfile){
	$customerProfile 					 = new AuthorizeNetCustomer;
	$customerProfile->description 		 = $description;
	$customerProfile->merchantCustomerId = (int)$user_id;
	$customerProfile->email 			 = $userMail;
	$customerProfileresponse 			 = $authNetCIMObj->createCustomerProfile($customerProfile);
	$customerProfileCode				 = $customerProfileresponse->xml->messages->message->code;
	$customerProfileId = "";
	if($customerProfileCode == 'I00001'){
	    $customerProfileId 					 = $customerProfileresponse->getCustomerProfileId();
		$UtilMasterObj->setCustomQuery("INSERT INTO authorize_cim_master(userid, customer_profile_id) VALUES (:userid, :customer_profile_id)",
		      array(':userid'=>$user_id,':customer_profile_id'=>$customerProfileId),array(':userid'=>'int',':customer_profile_id'=>'int'));
		$UtilMasterObj->exec_query();
	}
}
if($payment_type == "new" || ($payment_option == 0 && $card_action == "add_new_card")) { 
    $card_limit = true;
	if($count == 10){
		$status = 0;
		$response_details['payment_ack_failure_string'] = 'message=Maximum Number of cards are saved. Please delete any one card to save new.';
        $action = getRequestObj()->request('action');
		if(isset($action) && $action=='add') {
		    //show_page_header(FILE_AUTHRIZE_SAVED_CARDS.'?add=invalid&message=Maximum Number of cards are saved. Please delete any one card to save new.',true);
		}
		return $response_details['payment_ack_failure_string'];
	} else {
		// Add payment profile.
		$paymentProfile 									 = new AuthorizeNetPaymentProfile;
		$paymentProfile->customerType 		 				 = $customerType;
		$paymentProfile->payment->creditCard->cardNumber 	 = $cardNumber;
		$paymentProfile->payment->creditCard->expirationDate = $expirationDate;
		$paymentProfile->payment->creditCard->cardCode		 = $cardCode;
		
		//bbaaviationprintshoppe: Bind billing address details with payment profile id [start]
		$custBillingAddress = $checkout_details->billing_address;
		//update payment profile with billing address
		$paymentProfile->billTo->firstName		 = $custBillingAddress['firstname'];
		$paymentProfile->billTo->lastName		 = $custBillingAddress['lastname'];
		$paymentProfile->billTo->company		 = substr($custBillingAddress['companyname'], 0, 50);
		$paymentProfile->billTo->address		 = substr($custBillingAddress['street_address'], 0, 60);
		$paymentProfile->billTo->city		     = substr($custBillingAddress['city'], 0, 40);
		if((!empty($custBillingAddress['state']) && !empty($custBillingAddress['country'])) && empty($custBillingAddress['country'])) {
		    $customquery ="SELECT `country_states`.`state_name`, (SELECT `countries`.`countries_name` FROM `countries` WHERE `countries`.`countries_id` = ".$custBillingAddress['country'].") AS countries_name FROM `country_states` WHERE `country_states`.`countries_id` = ".$custBillingAddress['country']." AND `country_states`.`state_code` = '".$custBillingAddress['state']."'";
		    $country_state = $ObjAddressMaster->exec_query($customquery);
		    if(!empty($country_state)) {
		        $paymentProfile->billTo->state		     = substr($country_state[0]['state_name'], 0, 40);
		        $paymentProfile->billTo->country		 = substr($country_state[0]['countries_name'], 0, 60);
		    }
		} else if(!empty($custBillingAddress['state']) && !empty($custBillingAddress['countryname'])) {
		    $paymentProfile->billTo->state		     = substr($custBillingAddress['state'], 0, 40);
		    $paymentProfile->billTo->country		 = substr($custBillingAddress['countryname'], 0, 60);
		}
		$paymentProfile->billTo->zip		     = substr($custBillingAddress['postcode'], 0, 20);
		$paymentProfile->billTo->phoneNumber	 = (!empty($custBillingAddress['phone']))?$custBillingAddress['phone']:$custBillingAddress['phone_number'];
		//bbaaviationprintshoppe: Bind billing address details with payment profile id [end]
		
		$paymentProfileResponse = $authNetCIMObj->createCustomerPaymentProfile($customerProfileId, $paymentProfile);
		$paymentProfileId 	 = $paymentProfileResponse->getPaymentProfileId();
		$paymentResponseCode = $paymentProfileResponse->xml->messages->message->code;
	
		/* $validete_response = $authNetCIMObj->validateCustomerPaymentProfile($customerProfileId,$paymentProfileId,null,$cardCode);
		$cardValidationCode = $validete_response->xml->messages->message->code; */
	    if($paymentResponseCode =='I00001' && !empty($paymentProfileId)){
		    $acc_no = 'XXXX'.substr($cardNumber,-4);
		    
		    if(!empty($bill_address_auth)){
    		    $UtilMasterObj->setCustomQuery("INSERT INTO authorize_cim_child (userid, user_card_number, payment_profile_id, address_book_id) VALUES (:userid, :user_card_number, :payment_profile_id, :address_book_id)",
    		          array(':userid'=>$user_id, ':user_card_number'=>$acc_no,':payment_profile_id'=>$paymentProfileId,':address_book_id'=>$bill_address_auth),array(':userid'=>'int', ':user_card_number'=>'string',':payment_profile_id'=>'int',':address_book_id'=>'int'));
    		    $UtilMasterObj->exec_query();
		    }else{
		        $UtilMasterObj->setCustomQuery("INSERT INTO authorize_cim_child (userid, user_card_number, payment_profile_id) VALUES (:userid, :user_card_number, :payment_profile_id)",
		            array(':userid'=>$user_id, ':user_card_number'=>$acc_no,':payment_profile_id'=>$paymentProfileId),array(':userid'=>'int', ':user_card_number'=>'string',':payment_profile_id'=>'int'));
		        $UtilMasterObj->exec_query();
		    }
	    } else { 
    		if($paymentResponseCode =='E00039' && !empty($paymentProfileId)){
    		    $message = AUTHNET_CIM_DUPLICATE_CARD_MSG;
    		    $UtilMasterObj->setCustomQuery("SELECT COUNT(*) AS count FROM `authorize_cim_child` WHERE userid = :user_id AND payment_profile_id = :payment_profile_id",
    		        array(':user_id'=>$user_id, ':payment_profile_id'=>$paymentProfileId),array(':user_id'=>'int', ':payment_profile_id'=>'string'));
    		    $countArr = $UtilMasterObj->exec_query();
    		    $pay_count = $countArr[0]['count'];
    		     if($pay_count == 0){
    		        $acc_no = 'XXXX'.substr($cardNumber,-4);
    		        if(!empty($bill_address_auth)){
    		            $UtilMasterObj->setCustomQuery("INSERT INTO authorize_cim_child (userid, user_card_number, payment_profile_id, address_book_id) VALUES (:userid, :user_card_number, :payment_profile_id, :address_book_id)",
    		                array(':userid'=>$user_id, ':user_card_number'=>$acc_no,':payment_profile_id'=>$paymentProfileId,':address_book_id'=>$bill_address_auth),array(':userid'=>'int', ':user_card_number'=>'string',':payment_profile_id'=>'int',':address_book_id'=>'int'));
    		            $UtilMasterObj->exec_query();
    		        }else{
    		            $UtilMasterObj->setCustomQuery("INSERT INTO authorize_cim_child (userid, user_card_number, payment_profile_id) VALUES (:userid, :user_card_number, :payment_profile_id)",
    		                array(':userid'=>$user_id, ':user_card_number'=>$acc_no,':payment_profile_id'=>$paymentProfileId),array(':userid'=>'int', ':user_card_number'=>'string',':payment_profile_id'=>'int'));
    		            $UtilMasterObj->exec_query();
    		        }
    		    }
    		}
    		// Log error -- start
    		$current = "CustID: ".$user_id." \n";
    		if($createAuthProfile && $customerProfileCode!='I00001') {
    		    $error_message = $customerProfileresponse->xml->messages->message->text;
    		}
    		if($paymentResponseCode!='I00001') {
    		    $error_message = $paymentProfileResponse->xml->messages->message->text;
    		}
	    }
	}
} else {
    $UtilMasterObj->setSelect('payment_profile_id')
                  ->setFrom('authorize_cim_child')
                  ->setWhere("AND auto_id = :auto_id",$payment_type,"int");
	$paymentProfileArr 	= $UtilMasterObj->exec_query();
	$paymentProfileId 	= $paymentProfileArr[0]['payment_profile_id'];
	if(empty($paymentProfileArr[0]['address_book_id'])){
	    $UtilMasterObj    = new UtilMaster();
	    $UtilMasterObj->setCustomQuery("UPDATE authorize_cim_child SET address_book_id = :address_book_id WHERE auto_id = :auto_id",array($bill_address_auth,$payment_type),array("int","int"));
	    $UtilMasterObj->exec_query();
	}
}

$amount 								= $checkout_details->payment_method_amount;
$transaction 							= new AuthorizeNetTransaction;
$transaction->amount 					= $amount;
$transaction->customerProfileId 		= $customerProfileId;
$transaction->customerPaymentProfileId 	= $paymentProfileId;
if($payment_option != '1'){
	$transaction->cardCode 				= $cardCode;
}
$transaction->order->invoiceNumber      = $checkout_details->ship_pay_details['invoice_number'];
$transaction->order->purchaseOrderNumber= $checkout_details->ship_pay_details['ponumber'];
$transactionProfileResponse 		    = $authNetCIMObj->createCustomerProfileTransaction($authPaymentMethod, $transaction);

$transResponseCode	                    = $transactionProfileResponse->xml->messages->message->code;
$status = 0;
if($transResponseCode =='I00001' ) {
    $transactionResponse = $transactionProfileResponse->getTransactionResponse();
    $transactionId 		 = $transactionResponse->transaction_id;
    $accountNumber 		 = $transactionResponse->account_number;
    $status = 1;
    
    if($paymentResponseCode =='I00001') {
       if(!$save_this_card){
           $deletePaymentProfileResponse = $authNetCIMObj->deleteCustomerPaymentProfile($customerProfileId, $paymentProfileId);
           $UtilMasterObj->setCustomQuery("DELETE FROM `authorize_cim_child` WHERE payment_profile_id =:payment_profile_id",
               array(':payment_profile_id'=>$paymentProfileId),array(':payment_profile_id'=>'string'));
           $UtilMasterObj->exec_query();
       }
    }
} else {
    if($paymentResponseCode =='I00001') {
        $deletePaymentProfileResponse = $authNetCIMObj->deleteCustomerPaymentProfile($customerProfileId, $paymentProfileId);
        $UtilMasterObj->setCustomQuery("DELETE FROM `authorize_cim_child` WHERE payment_profile_id =:payment_profile_id",
               array(':payment_profile_id'=>$paymentProfileId),array(':payment_profile_id'=>'string'));
        $UtilMasterObj->exec_query();
    }
}
if($status == 0) {
    $error_message = empty($error_message)?$transactionProfileResponse->xml->messages->message->text:$error_message;
	$response_details['payment_ack_failure_string'] = 'message='.$error_message;
}

$statusMessage = $transactionResponse->response_reason_text;
$response_details['payment_ack_status'] = $status;

// Acknowledge message like Success, Failure, Successs with warning etc..... received from payment gateway
$response_details['ack_status_message'] = $statusMessage;

// Order Progress status id, set from admin side. if this is blank, it will take the default order status
$response_details["order_status"]	= $PaymentDetails[ELEMENT_ORDER_STATUS];

// Transaction Id return by gateway
$response_details['transaction_id'] = $transactionId;

// Selected currency code (3 digit) from admin side
$response_details['currency_code'] = $PaymentDetails['txt_currency_code'];

// Comment sent from payment gateways
$response_details["payment_comment"] = '';

// Serialized data. if other details need to add into database, received from payment gateway
$response_details["payment_response_details"] = serialize($transactionResponse);

// 1 => send Confirmation mail, 0 => do not send Confirmation mail
$response_details["mail_status"] = 1;

// This will be used to redirect to third party payment website i.e. Paypal Standard Method, CCAvenue 
$response_details["redirect_url"] = '';

$current_date =date('Y-m-d H:i:s');
$fp = fopen(DIR_WS_IMAGES."logs/payment_error5546.log", 'a');
$current ='';
$current .= "\n\n=================="."[".$current_date."]===============================\n\n";
$current .= "Payment Type: ".$payment_type."\n";
$current .= "Status:".$status."\n";
$current .=print_r($response_details,true);
$current .= "\n\n=========================================================================\n\n";
// Write the contents back to the file
fwrite($fp, $current);
fclose($fp);