<?php
$Customer_Payment_Details = array();

$Customer_Payment_Details[] = array(
	'Label' => PAYPAL_PRO_LABEL_CARD_HOLDER_NAME,
	'Value' => $CustomerPaymentDetails['auth_net_firstName'] . ' ' . $CustomerPaymentDetails['auth_net_lastName']
);

$Customer_Payment_Details[] = array(
	'Label' => PAYPAL_PRO_LABEL_CARD_TYPE,
	'Value' => $CustomerPaymentDetails['auth_net_creditCardType']
);

$Customer_Payment_Details[] = array(
	'Label' => PAYPAL_PRO_LABEL_CARD_NUMBER,
	'Value' => 'XXXXXXXXXXXX'.substr($CustomerPaymentDetails['auth_net_creditCardNumber'],12)
);

$Customer_Payment_Details[] = array(
	'Label' => PAYPAL_PRO_LABEL_CARD_EXPIRED,
	'Value' => $CustomerPaymentDetails['auth_net_expDateMonth'] . ',  ' . $CustomerPaymentDetails['auth_net_expDateYear']
);

$Customer_Payment_Details[] = array(
	'Label' => PAYPAL_PRO_LABEL_CARD_VERIFY_NUMBER,
	'Value' => $CustomerPaymentDetails['auth_net_cvv2Number']
);


?>