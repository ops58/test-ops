<?php

	/**
	 * This file is check the paypal response.
	 *
	 * @author     Radixweb <team.radixweb@gmail.com>
	 * @copyright  Copyright (c) 2008, Radixweb
	 * @version    1.0
	 * @since      1.0
	 */
	require_once("../../lib/common.php");
	require_once(DIR_WS_PAYMENT."/payment.php");

	$document_root = dirname(__FILE__);
	require_once($document_root.'/common.php');

	/** Variable **/
	$OrderNo = $RRequest->request('OrderNo');
	if($__Session->HasValue('_session_payment_request')) //Partial payment request  handling
	{			
		$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));
		
		$encrypt_password	= $PaymentDetails['txt_password'];
		//Get the Data
		$res_merchant_id	=	$RRequest->request('Merchant_Id');
		$res_order_id		=	$RRequest->request('OrderNo');
		$res_amount			=	$RRequest->request('Amount');
		$strCrypt			=	$RRequest->request("crypt");
		
		//$strDecoded=simpleXor(Base64Decode($strCrypt),$encrypt_password);
		//$payment_response_details = getToken($strDecoded);
		$payment_response_details=decrypt_details($strCrypt,$encrypt_password);
		$transaction_id = $payment_response_details['TxAuthNo'];
		
		if($RRequest->request('pact_res') === 'return' && $RRequest->request('add') === 'success')  
			process_payment_request($sucess=true,false,$OrderNo,$transaction_id,serialize($payment_response_details));
		else
			process_payment_request(false,$fail=true,$OrderNo,'',serialize($payment_response_details));
	}
	// Get Payment Method Details
	$PaymentDetails = getPaymentMethodSettings(basename(__DIR__));
	
	$encrypt_password	= $PaymentDetails['txt_password'];
	//Get the Data
	$res_merchant_id	=	$RRequest->request('Merchant_Id');
	$res_order_id		=	$RRequest->request('OrderNo');
	$res_amount			=	$RRequest->request('Amount');
	$strCrypt			=	$RRequest->request("crypt");
	
	$payment_response_details=decrypt_details($strCrypt,$encrypt_password);

	$payment_remarks = $payment_response_details['Status'];
	$payment_status_details = $payment_response_details['Status'] . ' - ' . $payment_response_details['StatusDetail'];
	$transaction_id = $payment_response_details['TxAuthNo'];
	$paypal_details = serialize($payment_response_details);

	//If Response is true then add/Update the Entry else give the error message
	$insertedOrdId = $RRequest->request('OrderNo');
	if($RRequest->request('pact_res') === 'return' && $RRequest->request('add') === 'success') {
	
		$transaction_id = $transaction_id;
		$remark = $payment_status_details;
		updatepaymentdetails($insertedOrdId,$success=true,$transaction_id,$paypal_details,$PaymentDetails,$remark);
	}
	else {
		updatepaymentdetails($insertedOrdId,false,null,$paypal_details,$PaymentDetails,null);
	}
