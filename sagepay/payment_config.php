<table width="100%" border="0" cellspacing="5" cellpadding="0"  align="left">	
	<tr>
		<td width="26%" align="left" class="form_caption"><?php echo PAYMENT_LBL_ENVIRONMENT; ?> : </td>
		<td align="left">
		   <?php
		   		if($PaymentDetails['sel_payment_env'] == "0"){
		   			$tchecked = "checked";
		   		} else {
		   			$tchecked = "";
		   		}
		   		if($PaymentDetails['sel_payment_env'] == "1"){
		   			$lchecked = "checked";
		   		} else {
		   			$lchecked = "";
		   		}
		   
		   ?>
		   <span><input type="radio" id="sel_payment_env" name="sel_payment_env" value="1" <?php echo $lchecked;?>><?php echo PAYMENT_LBL_LIVE_ENV; ?></span>
		   <span class="radio_distance"><input type="radio" id="sel_payment_env" name="sel_payment_env" value="0" <?php echo $tchecked;?>><?php echo PAYMENT_LBL_TESTING_ENV; ?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SAGEPAY_ENVIRONMENT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo SAGEPAY_LBL_VENDOR_NAME; ?> : </td>
		<td><input type="text" name="txt_vendor_name" id="txt_vendor_name" value="<?php echo $PaymentDetails['txt_vendor_name'];?>" class="text_box_std col-md-3" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SAGEPAY_VENDOR_NAME'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo SAGEPAY_LBL_PASSWORD; ?> : </td>
		<td><div class="input-group col-md-3"><input type="password" name="txt_password" id="txt_password" value="<?php echo $PaymentDetails['txt_password'];?>" class="text_box_std form-control" /><span class="input-group-addon password_toggle"><i class="pwd-icon fa fa-eye"></i></span></div></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SAGEPAY_PASSWORD'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo SAGEPAY_LBL_CURRENCY_CODE; ?> : </td>
		<td><input type="text" name="txt_currency_code" id="txt_currency_code" value="<?php echo $PaymentDetails['txt_currency_code'];?>" class="text_box_std col-md-3" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SAGEPAY_CURRENCY_CODE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td  align="left" class="form_caption"><?php echo PAYPAL_PRO_PAYMENT_ACTION;?> : </td>
		<td align="left">
		<?php
			if($PaymentDetails['pay_action'] == PAYPAL_PRO_PAYMENT_SALE){
				$Paycheckedsale  = "checked";
			} else {
				$Paycheckedsale  = "";
			}
			if($PaymentDetails['pay_action'] == PAYPAL_PRO_PAYMENT_ORDER){
				$Paycheckedorder  = "checked";
			} else {
				$Paycheckedorder = "";
			}
		    if($PaymentDetails['pay_action'] == PAYPAL_PRO_PAYMENT_AUTHORIZATION){
				$Paycheckedauth  = "checked";
			} else {
				$Paycheckedauth = "";
			}
		
			 if($PaymentDetails['pay_action'] == "PAYMENT"){
				$Paycheckedpayment  = "checked";
			} else {
				$Paycheckedpayment = "";
			}
		?>
		<span><input type="radio" id="pay_action" name="pay_action" value="<?php echo PAYPAL_PRO_PAYMENT_SALE;?>" <?php echo $Paycheckedsale;?>><?php echo PAYPAL_PRO_PAYMENT_SALE;?></span>
		<span class="radio_distance"><input type="radio" id="pay_action" name="pay_action" value="<?php echo PAYPAL_PRO_PAYMENT_ORDER;?>" <?php echo $Paycheckedorder;?>><?php echo PAYPAL_PRO_PAYMENT_ORDER;?></span>
		<span class="radio_distance"><input type="radio" id="pay_action" name="pay_action" value="<?php echo PAYPAL_PRO_PAYMENT_AUTHORIZATION;?>" <?php echo $Paycheckedauth;?>><?php echo PAYPAL_PRO_PAYMENT_AUTHORIZATION;?></span>
		<span class="radio_distance"><input type="radio" id="pay_action" name="pay_action" value="<?php echo "PAYMENT";?>" <?php echo $Paycheckedpayment;?>><?php echo "PAYMENT";?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SAGEPAY_PAYMENT_TYPE'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>	
	<tr>
		<td class="form_caption" align="left"><?php echo SAGEPAY_LBL_PARTNER_ID; ?> : </td>
		<td align="left"><input type="text" name="txt_partner_id" id="txt_partner_id" value="<?php echo $PaymentDetails['txt_partner_id'];?>" class="text_box_std col-md-3" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SAGEPAY_PARTNER_ID'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo SAGEPAY_LBL_EMAIL_ID; ?> : </td>
		<td><input type="text" name="txt_email_id" id="txt_email_id" value="<?php echo $PaymentDetails['txt_email_id'];?>" class="text_box_std col-md-3" /></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SAGEPAY_EMAIL_ID'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo SAGEPAY_LBL_CUSTOMER_EMAIL_OPTION; ?> : </td>
		<td><label><input type="radio" class="radiochk" name="rdo_cust_email" id="rdo_cust_email" value="1" <?php if($PaymentDetails['rdo_cust_email'] == '1') { echo ' checked="checked"'; } ?> />&nbsp;<?php echo SAGEPAY_LBL_CUSTOMER_EMAIL_OPTION_YES; ?></label>&nbsp;&nbsp;<label><input type="radio" class="radiochk" name="rdo_cust_email" id="rdo_cust_email" value="0" <?php if($PaymentDetails['rdo_cust_email'] == '0' || $PaymentDetails['rdo_cust_email'] =='') { echo ' checked="checked"'; } ?> />&nbsp;<?php echo SAGEPAY_LBL_CUSTOMER_EMAIL_OPTION_NO; ?></label></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SAGEPAY_SENT_MAIL'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo SAGEPAY_LBL_EMAIL_MESSAGE; ?> : </td>
		<td align="left"><textarea id="txt_mail_message" name="txt_mail_message" rows="10" cols="40" ><?php echo $PaymentDetails['txt_mail_message'];?></textarea></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SAGEPAY_EMAIL_CONTENT'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td class="form_caption" align="left"><?php echo SAGEPAY_LBL_DESCRIPTION; ?> : </td>
		<td><textarea name="txt_description" id="txt_description" rows="10" cols="40" ><?php echo $PaymentDetails['txt_description'];?></textarea></td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"><?php echo $help_messages['HELP_PAYMENT_SAGEPAY_DESCRIPTION'];?></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
	<tr>
		<td  align="left" class="form_caption"><?php echo 'Sage 50 Synchronization';?> : </td>
		<td align="left">
		<span><input type="radio" id="sage50_sync1" name="sage50_sync" value="1" <?php if($PaymentDetails['sage50_sync'] == '1') { echo ' checked="checked"'; } ?>>&nbsp;<?php echo SAGEPAY_LBL_CUSTOMER_EMAIL_OPTION_YES;?></span>
		<span class="radio_distance"><input type="radio" id="sage50_sync2" name="sage50_sync" value="0" <?php if($PaymentDetails['sage50_sync'] == '0' || $PaymentDetails['sage50_sync'] =='') { echo ' checked="checked"'; } ?>>&nbsp;<?php echo SAGEPAY_LBL_CUSTOMER_EMAIL_OPTION_NO;?></span>
		</td>
	</tr>
	<tr>											
		<td><?php echo draw_separator($sep_width,$sep_height);?></td>
		<td class="help_msg"></td>
	</tr>
	<tr>											
		<td colspan="2"><?php echo draw_separator($sep_width,$sep_height);?></td>
	</tr>
</table>