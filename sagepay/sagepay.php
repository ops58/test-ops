<?php
require_once("../../lib/common.php");
require_once(DIR_WS_PAYMENT."/payment.php");
require_once(DIR_WS_LIB.'cart_class.php');
require_once(DIR_WS_MODEL."PaymentDetailsMaster.php");

$paymentMethodID = $RRequest->request('payment_method_id');
$payment_method_id = (isset($paymentMethodID) && $paymentMethodID!="")?$paymentMethodID:basename(__DIR__);
$PaymentDetails = getPaymentMethodSettings($payment_method_id);
$PaymentDetailsMasterObj= new PaymentDetailsMaster();
//Get the Data 
$checkout_details        = new Shopping_Cart_Data(true,true,true,true,false);
$cart_details_arr           = $checkout_details->cart_details;

if ($PaymentDetails['sel_payment_env'] == 1) {
	$post_url   = "https://live.sagepay.com/gateway/service/vspform-register.vsp";  //for live
} else {
	$post_url   = "https://test.sagepay.com/gateway/service/vspform-register.vsp";//for testing	
}

$document_root = dirname(__FILE__);
require_once($document_root.'/common.php');

$strVendorName = $PaymentDetails['txt_vendor_name'];
$orderid		= $RRequest->request('order_id');
$userID = getAccessObj()->getUserID();
$strVendorTxCode = $strVendorName.$orderid.$userID.date('dmy');

$price			= number_format($RRequest->request('total'), 2, '.', '');

$paypal_email 	= $PaymentDetails['txt_business_email']; //user email address
$item_name		= $PaymentDetails['txt_item_name'];
$currency_code	= $PaymentDetails['txt_currency_code'];
$encrypt_password	= $PaymentDetails['txt_password'];


$sagepay_data = array();
$sagepay_data['VendorTxCode']=$strVendorTxCode;
if (!empty($PaymentDetails['txt_partner_id'])) {
	$sagepay_data['ReferrerID']=$PaymentDetails['txt_partner_id'];
}

$flag_email_option = 0; // Disable Mail
if (!empty($PaymentDetails['txt_email_id'])) {
	$sagepay_data['VendorEMail']=$PaymentDetails['txt_email_id'];
	$flag_email_option = 1; // Only vendor Mail
}
if (!empty($PaymentDetails['rdo_cust_email']) && $PaymentDetails['rdo_cust_email'] == '1') {
	$sagepay_data['CustomerEMail']=$PaymentDetails['txt_email_id'];
	$flag_email_option = 2;
}

$strTransactionType = $PaymentDetails['pay_action'];
if ($strTransactionType!=="AUTHENTICATE") {
	$sagepay_data['ApplyAVSCV2'] = "0";
}

$tax = unserialize($checkout_details->city_tax_amount);
//echo '<pre>';print_r($tax);echo '</pre>';exit;
// this code is related to sage 50 accounting software
$shipping_method = explode("_", $checkout_details->shipping_type)[0];
$shipping_method_name = get_shipping_method_name($shipping_method);
$total_tax = 0;
if(!empty($tax)){
    foreach ($tax as $taxkey => $taxvalue){
        $total_tax += $taxvalue['tax_amount'];
    }
}
$shipping_amount = $checkout_details->shipping_amount;

$count_total_products = count($cart_details_arr)+2;
$basket_string = '';
$basket_string .= $count_total_products;
foreach ($cart_details_arr as $key => $cart_details_values) {
    $product_sku_d = '';
    $product_sku = getProductSetting($field="", $cart_details_values['product_id'], 'PRODUCT_SKU_NUMBER',"var");
    if($product_sku != '') {
        $product_sku_d ="[$product_sku[PRODUCT_SKU_NUMBER]]";
    }
    $key = $key + 1;
    $basket_string .= ':'.$product_sku_d.$cart_details_values['product_title'].':'.$cart_details_values['quantity'].':'.$cart_details_values['product_sub_price']/$cart_details_values['quantity'].'::'.$cart_details_values['product_sub_price'].':'.$cart_details_values['product_sub_price'].$last_colon;
}
$basket_string .= ":TAX/VAT:::::$total_tax";
$basket_string .= ":SelectedShippingMethod-$shipping_method_name:1:0:0:0:$shipping_amount";
//end sage 50 accounting software
$sagepay_data['Amount'] = $RRequest->request('total');
$sagepay_data['Currency'] = $PaymentDetails['txt_currency_code'];
$sagepay_data['Description'] = $PaymentDetails['txt_description'];
$sagepay_data['SuccessURL'] = show_page_link("payment/sagepay/response.php?pact_res=return&add=success&OrderNo=".$orderid,true);
$sagepay_data['FailureURL'] = show_page_link("payment/sagepay/response.php?pact_res=cancel&add=invalid&OrderNo=".$orderid,true);
$companyname = ($checkout_details->billing_address['companyname'] != '')?$checkout_details->billing_address['companyname']:$checkout_details->customer_address['firstname'].' '.$checkout_details->customer_address['lastname'];
$sagepay_data['CustomerName'] = $companyname;
$sagepay_data['CustomerEMail'] = getAccessObj()->getUserEmail();
$sagepay_data['SendEMail'] = $flag_email_option;
$sagepay_data['eMailMessage'] = $PaymentDetails['txt_mail_message'];
$sagepay_data['BillingFirstnames'] = $checkout_details->billing_address['firstname'];
$sagepay_data['BillingSurname'] = $checkout_details->billing_address['lastname'];
$sagepay_data['BillingAddress1'] = $checkout_details->billing_address['street_address'];
$sagepay_data['BillingCity'] = $checkout_details->billing_address['city'];
$sagepay_data['BillingPostCode'] = $checkout_details->billing_address['postcode'];
$sagepay_data['BillingCountry'] = $checkout_details->billing_address['countrycode2'];
if($checkout_details->billing_address['countrycode2'] == 'US') {
	$sagepay_data['BillingState'] = $checkout_details->billing_address['statecode'];
}
//in case of local pickup method we have company name insted of company name
$sagepay_data['DeliveryFirstnames'] = ($checkout_details->shipping_method_id == -1)?substr($checkout_details->shipping_address['companyname'],0,20):substr($checkout_details->shipping_address['firstname'],0,20);
if($shipping_method == '-1') {
    $sagepay_data['DeliverySurname'] = $checkout_details->shipping_address['companyname'];
} else {
    $sagepay_data['DeliverySurname'] = $checkout_details->shipping_address['lastname'];
}
$sagepay_data['DeliveryAddress1'] = $checkout_details->shipping_address['street_address'];
$sagepay_data['DeliveryCity'] = $checkout_details->shipping_address['city'];
$sagepay_data['DeliveryPostCode'] = $checkout_details->shipping_address['postcode'];
$sagepay_data['DeliveryCountry'] = $checkout_details->shipping_address['countrycode2'];

if($checkout_details->shipping_address['countrycode2'] == 'US') {
	$sagepay_data['DeliveryState'] = $checkout_details->shipping_address['statecode'];
}

if($PaymentDetails['sage50_sync'] == '1') {
    if($__Session->HasValue('_session_payment_request')){
        $sagepay_data['Basket'] = '';
    } else {
        $sagepay_data['Basket'] = $basket_string;
    }
}
$sagepay_data['AllowGiftAid'] = '0';
$sagepay_data['Apply3DSecure'] = '0';
$userid = $userID;
$PaymentDetailsMasterObj->setSelect("orders.orders_id");
$PaymentDetailsMasterObj->setJoin("LEFT JOIN orders ON orders.orders_id = payment_details.orders_id");
$PaymentDetailsMasterObj->setWhere("orders.user_id = :user_id AND payment_details.payment_method = :payment_method", array(':user_id'=>$userID,':payment_method'=>$payment_method_id), array(':user_id'=>'int',':payment_method'=>'int'));
$check_repeat_user = $PaymentDetailsMasterObj->getPaymentDetails();
if(empty($check_repeat_user)) {
    $precust = '0';
} else {
    $precust = '1';
}

$phone = $checkout_details->billing_address['phone'];
if($PaymentDetails['sage50_sync'] == '1') {
    if($phone != '' && strlen($phone) >= 10) {
        $xml_phone = <<<HTML
            <customerWorkPhone>$phone</customerWorkPhone>
HTML;
        }
        $sagepay_data['CustomerXML'] = <<<HTML
        <customer>
        $xml_phone
            <previousCust>$precust</previousCust>
        	<customerId>$userid</customerId>
        </customer>
HTML;
}
/* if($_SERVER['REMOTE_ADDR'] == '180.211.112.242') {
    echo '<pre>';print_r($sagepay_data);echo '</pre>';
    echo '<pre>';print_r($checkout_details);echo '</pre>';exit;
} */

$str_crypt_data = encrypt_details($sagepay_data, $encrypt_password);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></title>
	<link href="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_CSS;?>style.css" rel="stylesheet" type="text/css" media="screen" />
<style type="text/css" >
#contentContainer {
	background:none;
}
.div_img_loader {
	color:#df6c1b;
	float:left;
	font-size:72px;
	text-align:left;
	width:25%;
	margin:-12px 0px 0px 0px;
}
.btn_pay_style{
	background:#df6c1b;
	color:#FFFFFF;
	font-size:16px;
	height:48px;
	padding:3px 5px 3px 5px;
}
.wait_img_loading {
	height:300px;
}
.wait_img_loading .img_load {
	float:none;
	padding:125px 0 10px 0;
	text-align:center;
	line-height:28px;
	font-size:15px;	
	/*font-weight:bold;*/
}
.wait_img_loading .img_load .load_message {
	text-align:right;
	line-height:28px;
	font-size:34px;
	font-weight:bold;
	color:#df6c1b;
	width:66%;
	float:left;
}
.wait_img_loading .img_load .click_message {
	float:left;
	width:100%;
	text-align:center;
	line-height:28px;
	font-size:16px;
}
</style>
<script type="text/javascript">
<!-- Begin
/* This script and many more are available free online at
The JavaScript Source!! http://javascript.internet.com
Created by: Abraham Joffe :: http://www.abrahamjoffe.com.au/ */

var startTime=new Date();
var strDisplay = '.';
var repeat = 5;
var start = 0;
function currentTime(){
  var a=Math.floor((new Date()-startTime)/100)/10;
  if(start != repeat ) {
  	document.getElementById("div_repeat_info").innerHTML = document.getElementById("div_repeat_info").innerHTML + strDisplay;
  	start = start + 1;
  } else {
  	document.getElementById("div_repeat_info").innerHTML = strDisplay;
  	start = 0;
  }
}

window.onload=function(){
  clearTimeout(loopTime);
  strDisplay = document.getElementById("div_repeat_info").innerHTML
}

// End -->
</script>
</head>
<body>
<!-- ************************************************************************************* -->
<!-- This form is all that is required to submit the payment information to the system -->
<form action="<?php echo $post_url ?>" method="POST" id="SagePayForm" name="SagePayForm"> 
<div id="container">
	
	
	<div id="header">
		<div id="headerleft">
		<!-- Header Left -->
			<div class="logo">
				<?php /*<span><a href="<?php echo show_page_link(FILE_INDEX);?>"><img src="<? echo DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES; ?>logo.gif" /></a></span>*/ ?>
				<span><a href="<?php echo show_page_link(FILE_INDEX);?>"><img src="<? echo SITE_LOGO_PATH; ?>" /></a></span>
			</div>
		</div>
	</div>
	<!-- main contian End -->
	<div id="contentContainer" class="wait_img_loading">
		<div class="img_load">
			<div class="load_message"><?php echo LABEL_PAYMENT_SITE_REDIRECTION; ?></div><div id="div_repeat_info" class="div_img_loader">.</div>
			<script type="text/javascript">
  			var loopTime=setInterval("currentTime()",500);
			</script>
			<div class="click_message"><?php echo LABEL_PAGE_LOAD_PROBLEM ;?><br /><br />
			<?php if(file_exists(DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES.'img_click_to_pay.gif')) { ?>
				<input type="image" class="btn_pay_style" name="btnpaysubmit" src="<?php echo DIR_HTTP_SITE_CURRENT_TEMPLATE_IMAGES; ?>img_click_to_pay.gif" title="Click Here To Pay" value="<?php echo LEBEL_CLICK_HERE_TO_PAY;?>" alt="Click Here To Pay">
			<?php } else { ?>
				<input type="submit" class="btn_pay_style" name="btnpaysubmit" title="Click Here To Pay" value="<?php echo LEBEL_CLICK_HERE_TO_PAY;?>" >
			<?php } ?>
			</div>
		</div>
		<input type="hidden" name="navigate" value="" />
		<input type="hidden" name="VPSProtocol" value="<?php echo SAGEPAY_PROTOCOL ?>">
		<input type="hidden" name="TxType" value="<?php echo $strTransactionType ?>">
		<input type="hidden" name="Vendor" value="<?php echo $strVendorName ?>">
		<input type="hidden" name="Crypt" value="<?php echo $str_crypt_data ?>">
	</div>
	</div>
</div>
</form>
<!-- ************************************************************************************* -->
<!-- Main End -->
</body>
</html>
<script language="javascript">
document.SagePayForm.submit();
</script>